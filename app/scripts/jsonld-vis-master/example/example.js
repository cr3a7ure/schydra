
import d3 from 'd3';
require('..');

var exampleGraph = {
    "@context": {
        "vocab": "http://vps362714.ovh.net:8085/docs.jsonld",
        "@base": "http://vps362714.ovh.net:8085",
        "hydra": "http://www.w3.org/ns/hydra/core#",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "xmls": "http://www.w3.org/2001/XMLSchema#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "schema": "http://schema.org/",
        "domain": {
            "@id": "rdfs:domain",
            "@type": "@id"
        },
        "range": {
            "@id": "rdfs:range",
            "@type": "@id"
        },
        "subClassOf": {
            "@id": "rdfs:subClassOf",
            "@type": "@id"
        },
        "expects": {
            "@id": "hydra:expects",
            "@type": "@id"
        },
        "returns": {
            "@id": "hydra:returns",
            "@type": "@id"
        },
        "object": {
            "@id": "schema:object",
            "@type": "@id"
        },
        "result": {
            "@id": "schema:result",
            "@type": "@id"
        },
        "target": {
            "@id": "schema:target",
            "@type": "@id"
        },
        "query": {
            "@id": "schema:query"
        },
        "property": {
            "@id": "hydra:property"
        },
        "variable": {
            "@id": "hydra:variable"
        },
        "required": {
            "@id": "hydra:required"
        }
    },
    "@id": "/docs.jsonld",
    "hydra:title": "API Platform's demo",
    "hydra:description": "This is a demo application of the [API Platform](https://api-platform.com) framework.\n[Its source code](https://github.com/api-platform/demo) includes various examples, check it out!\n",
    "hydra:entrypoint": "/",
    "hydra:supportedClass": [
        {
            "@id": "vocab:#TouristAttraction",
            "@type": [
                "hydra:Class",
                "http://schema.org/TouristAttraction"
            ],
            "jsonld_context": "test",
            "rdfs:label": "TouristAttraction",
            "hydra:title": "TouristAttraction",
            "hydra:supportedProperty": [
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#TouristAttraction/name",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/name"
                        ],
                        "rdfs:label": "name",
                        "domain": "http://schema.org/TouristAttraction",
                        "range": "xmls:string"
                    },
                    "hydra:title": "name",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "The name of the item"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#TouristAttraction/description",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/description"
                        ],
                        "rdfs:label": "description",
                        "domain": "http://schema.org/TouristAttraction",
                        "range": "xmls:string"
                    },
                    "hydra:title": "description",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "A description of the item"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#TouristAttraction/url",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/url"
                        ],
                        "rdfs:label": "url",
                        "domain": "http://schema.org/TouristAttraction",
                        "range": "xmls:string"
                    },
                    "hydra:title": "url",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "URL of the item"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#TouristAttraction/hasMap",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/hasMap"
                        ],
                        "rdfs:label": "hasMap",
                        "domain": "http://schema.org/TouristAttraction",
                        "range": "xmls:string"
                    },
                    "hydra:title": "hasMap",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "A URL to a map of the place"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#TouristAttraction/geo",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/geo"
                        ],
                        "rdfs:label": "geo",
                        "domain": "http://schema.org/TouristAttraction",
                        "range": "http://schema.org/GeoCoordinates"
                    },
                    "hydra:title": "geo",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "The geo coordinates of the place"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#TouristAttraction/image",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/image"
                        ],
                        "rdfs:label": "image",
                        "domain": "http://schema.org/TouristAttraction",
                        "range": "xmls:string"
                    },
                    "hydra:title": "image",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "An image of the item. This can be a \\[\\[URL\\]\\] or a fully described \\[\\[ImageObject\\]\\]"
                }
            ],
            "hydra:supportedOperation": [
                {
                    "@type": [
                        "hydra:operation",
                        "schema:Action"
                    ],
                    "hydra:method": "GET",
                    "hydra:title": "Retrieves TouristAttraction resource.",
                    "rdfs:label": "Retrieves TouristAttraction resource.",
                    "returns": "http://schema.org/TouristAttraction"
                },
                {
                    "@type": [
                        "hydra:operation",
                        "schema:UpdateAction"
                    ],
                    "expects": "http://schema.org/TouristAttraction",
                    "hydra:method": "PUT",
                    "hydra:title": "Replaces the TouristAttraction resource.",
                    "rdfs:label": "Replaces the TouristAttraction resource.",
                    "returns": "http://schema.org/TouristAttraction"
                },
                {
                    "@type": [
                        "hydra:operation",
                        "schema:DeleteAction"
                    ],
                    "hydra:method": "DELETE",
                    "hydra:title": "Deletes the TouristAttraction resource.",
                    "rdfs:label": "Deletes the TouristAttraction resource.",
                    "returns": "owl:Nothing"
                }
            ]
        },
        {
            "@id": "vocab:#PostalAddress",
            "@type": [
                "hydra:Class",
                "http://schema.org/PostalAddress"
            ],
            "jsonld_context": "test",
            "rdfs:label": "PostalAddress",
            "hydra:title": "PostalAddress",
            "hydra:supportedProperty": [
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#PostalAddress/addressCountry",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/addressCountry"
                        ],
                        "rdfs:label": "addressCountry",
                        "domain": "http://schema.org/PostalAddress",
                        "range": "xmls:string"
                    },
                    "hydra:title": "addressCountry",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "The country. For example, USA. You can also provide the two-letter \\[ISO 3166-1 alpha-2 country code\\](http://en.wikipedia.org/wiki/ISO\\_3166-1)"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#PostalAddress/addressLocality",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/addressLocality"
                        ],
                        "rdfs:label": "addressLocality",
                        "domain": "http://schema.org/PostalAddress",
                        "range": "xmls:string"
                    },
                    "hydra:title": "addressLocality",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "The locality. For example, Mountain View"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#PostalAddress/addressRegion",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/addressRegion"
                        ],
                        "rdfs:label": "addressRegion",
                        "domain": "http://schema.org/PostalAddress",
                        "range": "xmls:string"
                    },
                    "hydra:title": "addressRegion",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "The region. For example, CA"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#PostalAddress/postalCode",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/postalCode"
                        ],
                        "rdfs:label": "postalCode",
                        "domain": "http://schema.org/PostalAddress",
                        "range": "xmls:string"
                    },
                    "hydra:title": "postalCode",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "The postal code. For example, 94043"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#PostalAddress/streetAddress",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/streetAddress"
                        ],
                        "rdfs:label": "streetAddress",
                        "domain": "http://schema.org/PostalAddress",
                        "range": "xmls:string"
                    },
                    "hydra:title": "streetAddress",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "The street address. For example, 1600 Amphitheatre Pkwy"
                }
            ],
            "hydra:supportedOperation": [
                {
                    "@type": [
                        "hydra:operation",
                        "schema:Action"
                    ],
                    "hydra:method": "GET",
                    "hydra:title": "Retrieves PostalAddress resource.",
                    "rdfs:label": "Retrieves PostalAddress resource.",
                    "returns": "http://schema.org/PostalAddress"
                },
                {
                    "@type": [
                        "hydra:operation",
                        "schema:UpdateAction"
                    ],
                    "expects": "http://schema.org/PostalAddress",
                    "hydra:method": "PUT",
                    "hydra:title": "Replaces the PostalAddress resource.",
                    "rdfs:label": "Replaces the PostalAddress resource.",
                    "returns": "http://schema.org/PostalAddress"
                },
                {
                    "@type": [
                        "hydra:operation",
                        "schema:DeleteAction"
                    ],
                    "hydra:method": "DELETE",
                    "hydra:title": "Deletes the PostalAddress resource.",
                    "rdfs:label": "Deletes the PostalAddress resource.",
                    "returns": "owl:Nothing"
                }
            ]
        },
        {
            "@id": "vocab:#GeoCoordinates",
            "@type": [
                "hydra:Class",
                "http://schema.org/GeoCoordinates"
            ],
            "jsonld_context": "test",
            "rdfs:label": "GeoCoordinates",
            "hydra:title": "GeoCoordinates",
            "hydra:supportedProperty": [
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#GeoCoordinates/latitude",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/latitude"
                        ],
                        "rdfs:label": "latitude",
                        "domain": "http://schema.org/GeoCoordinates",
                        "range": "xmls:decimal"
                    },
                    "hydra:title": "latitude",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "The latitude of a location. For example ```37.42242``` (\\[WGS 84\\](https://en.wikipedia.org/wiki/World\\_Geodetic\\_System))"
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#GeoCoordinates/longitude",
                        "@type": [
                            "rdf:Property",
                            "http://schema.org/longitude"
                        ],
                        "rdfs:label": "longitude",
                        "domain": "http://schema.org/GeoCoordinates",
                        "range": "xmls:decimal"
                    },
                    "hydra:title": "longitude",
                    "hydra:required": false,
                    "hydra:readable": true,
                    "hydra:writable": true,
                    "hydra:description": "The longitude of a location. For example ```-122.08585``` (\\[WGS 84\\](https://en.wikipedia.org/wiki/World\\_Geodetic\\_System))"
                }
            ],
            "hydra:supportedOperation": [
                {
                    "@type": [
                        "hydra:operation",
                        "schema:Action"
                    ],
                    "hydra:method": "GET",
                    "hydra:title": "Retrieves GeoCoordinates resource.",
                    "rdfs:label": "Retrieves GeoCoordinates resource.",
                    "returns": "http://schema.org/GeoCoordinates"
                },
                {
                    "@type": [
                        "hydra:operation",
                        "schema:UpdateAction"
                    ],
                    "expects": "http://schema.org/GeoCoordinates",
                    "hydra:method": "PUT",
                    "hydra:title": "Replaces the GeoCoordinates resource.",
                    "rdfs:label": "Replaces the GeoCoordinates resource.",
                    "returns": "http://schema.org/GeoCoordinates"
                },
                {
                    "@type": [
                        "hydra:operation",
                        "schema:DeleteAction"
                    ],
                    "hydra:method": "DELETE",
                    "hydra:title": "Deletes the GeoCoordinates resource.",
                    "rdfs:label": "Deletes the GeoCoordinates resource.",
                    "returns": "owl:Nothing"
                }
            ]
        },
        {
            "@id": "vocab:#Entrypoint",
            "@type": "hydra:Class",
            "hydra:title": "The API entrypoint",
            "hydra:supportedProperty": [
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "/tourist_attractions",
                        "@type": "hydra:Link",
                        "domain": "vocab:#Entrypoint",
                        "rdfs:label": "The collection of TouristAttraction resources",
                        "range": {
                            "@type": "hydra:PartialCollectionView",
                            "hydra:member": {
                                "@type": "vocab:#TouristAttraction"
                            },
                            "hydra:search": {
                                "@type": "hydra:IriTemplate",
                                "hydra:template": "/tourist_attractions{?geo.latitude,geo.longitude}",
                                "hydra:variableRepresentation": "BasicRepresentation",
                                "hydra:mapping": [
                                    {
                                        "@type": "hydra:IriTemplateMapping",
                                        "variable": "geo.latitude",
                                        "property": "geo.latitude",
                                        "required": false
                                    },
                                    {
                                        "@type": "hydra:IriTemplateMapping",
                                        "variable": "geo.longitude",
                                        "property": "geo.longitude",
                                        "required": false
                                    }
                                ]
                            }
                        },
                        "hydra:supportedOperation": [
                            {
                                "@type": [
                                    "hydra:operation",
                                    "schema:searchAction"
                                ],
                                "hydra:method": "GET",
                                "hydra:title": "Retrieves the collection of TouristAttraction resources.",
                                "object": "vocab:#TouristAttraction",
                                "query": {
                                    "@type": "vocab:#GeoCoordinates"
                                },
                                "rdfs:label": "Retrieves the collection of TouristAttraction resources.",
                                "result": "vocab:#TouristAttraction",
                                "returns": "hydra:PartialCollectionView",
                                "target": "/tourist_attractions"
                            },
                            {
                                "@type": [
                                    "hydra:operation",
                                    "schema:AddAction"
                                ],
                                "expects": "http://schema.org/TouristAttraction",
                                "hydra:method": "POST",
                                "hydra:title": "Creates a TouristAttraction resource.",
                                "rdfs:label": "Creates a TouristAttraction resource.",
                                "returns": "http://schema.org/TouristAttraction"
                            }
                        ]
                    },
                    "hydra:title": "The collection of TouristAttraction resources",
                    "hydra:readable": true,
                    "hydra:writable": false
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "/postal_addresses",
                        "@type": "hydra:Link",
                        "domain": "vocab:#Entrypoint",
                        "rdfs:label": "The collection of PostalAddress resources",
                        "range": {
                            "@type": "hydra:PartialCollectionView",
                            "hydra:member": {
                                "@type": "vocab:#PostalAddress"
                            },
                            "hydra:search": null
                        },
                        "hydra:supportedOperation": [
                            {
                                "@type": [
                                    "hydra:operation",
                                    "schema:Action"
                                ],
                                "hydra:method": "GET",
                                "hydra:title": "Retrieves the collection of PostalAddress resources.",
                                "rdfs:label": "Retrieves the collection of PostalAddress resources.",
                                "returns": "hydra:PartialCollectionView"
                            },
                            {
                                "@type": [
                                    "hydra:operation",
                                    "schema:AddAction"
                                ],
                                "expects": "http://schema.org/PostalAddress",
                                "hydra:method": "POST",
                                "hydra:title": "Creates a PostalAddress resource.",
                                "rdfs:label": "Creates a PostalAddress resource.",
                                "returns": "http://schema.org/PostalAddress"
                            }
                        ]
                    },
                    "hydra:title": "The collection of PostalAddress resources",
                    "hydra:readable": true,
                    "hydra:writable": false
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "/geo_coordinates",
                        "@type": "hydra:Link",
                        "domain": "vocab:#Entrypoint",
                        "rdfs:label": "The collection of GeoCoordinates resources",
                        "range": {
                            "@type": "hydra:PartialCollectionView",
                            "hydra:member": {
                                "@type": "vocab:#GeoCoordinates"
                            },
                            "hydra:search": null
                        },
                        "hydra:supportedOperation": [
                            {
                                "@type": [
                                    "hydra:operation",
                                    "schema:Action"
                                ],
                                "hydra:method": "GET",
                                "hydra:title": "Retrieves the collection of GeoCoordinates resources.",
                                "rdfs:label": "Retrieves the collection of GeoCoordinates resources.",
                                "returns": "hydra:PartialCollectionView"
                            },
                            {
                                "@type": [
                                    "hydra:operation",
                                    "schema:AddAction"
                                ],
                                "expects": "http://schema.org/GeoCoordinates",
                                "hydra:method": "POST",
                                "hydra:title": "Creates a GeoCoordinates resource.",
                                "rdfs:label": "Creates a GeoCoordinates resource.",
                                "returns": "http://schema.org/GeoCoordinates"
                            }
                        ]
                    },
                    "hydra:title": "The collection of GeoCoordinates resources",
                    "hydra:readable": true,
                    "hydra:writable": false
                }
            ],
            "hydra:supportedOperation": {
                "@type": "hydra:Operation",
                "hydra:method": "GET",
                "rdfs:label": "The API entrypoint.",
                "returns": "#EntryPoint"
            }
        },
        {
            "@id": "vocab:#ConstraintViolation",
            "@type": "hydra:Class",
            "hydra:title": "A constraint violation",
            "hydra:supportedProperty": [
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#ConstraintViolation/propertyPath",
                        "@type": "rdf:Property",
                        "rdfs:label": "propertyPath",
                        "domain": "vocab:#ConstraintViolation",
                        "range": "xmls:string"
                    },
                    "hydra:title": "propertyPath",
                    "hydra:description": "The property path of the violation",
                    "hydra:readable": true,
                    "hydra:writable": false
                },
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#ConstraintViolation/message",
                        "@type": "rdf:Property",
                        "rdfs:label": "message",
                        "domain": "vocab:#ConstraintViolation",
                        "range": "xmls:string"
                    },
                    "hydra:title": "message",
                    "hydra:description": "The message associated with the violation",
                    "hydra:readable": true,
                    "hydra:writable": false
                }
            ]
        },
        {
            "@id": "vocab:#ConstraintViolationList",
            "@type": "hydra:Class",
            "subClassOf": "hydra:Error",
            "hydra:title": "A constraint violation list",
            "hydra:supportedProperty": [
                {
                    "@type": "hydra:SupportedProperty",
                    "hydra:property": {
                        "@id": "vocab:#ConstraintViolationList/violations",
                        "@type": "rdf:Property",
                        "rdfs:label": "violations",
                        "domain": "vocab:#ConstraintViolationList",
                        "range": "vocab:#ConstraintViolation"
                    },
                    "hydra:title": "violations",
                    "hydra:description": "The violations",
                    "hydra:readable": true,
                    "hydra:writable": false
                }
            ]
        }
    ]
};

// d3.json('example.json', (err, data) => {
//   if (err) return console.warn(err);
  d3.jsonldVis(exampleGraph, '#graph', { w: 800, h: 600, maxLabelWidth: 250 });
// });
