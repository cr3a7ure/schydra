// app/scripts/app.js
'use strict';

/**
 * @ngdoc overview
 * @name blogApp
 * @description
 * # blogApp
 *
 * Main module of the application.
 */

var blogApp = angular.module('blogApp',["angucomplete-alt"]);

blogApp.run(function() {
        // JSON-LD @id support
        // Restangular.setRestangularFields({
        //     id: '@id'
        // });
        // Restangular.setSelfLinkAbsoluteUrl(false);
    });

// blogApp.directive('newScope', function($parse){
//   return {
//     scope:true,
//     compile: function (tElm,tAttrs){
//       var fn = $parse(tAttrs.newScope);
      
//       return function(scope, elm, attrs){
//         var property = fn(scope.$parent);
//         property = angular.isObject(property) ? property : {};
//         angular.extend(scope, property);
//       };
//     }
//   };
// });