// app/scripts/controllers/schemaClient.js
'use strict';
// Client for our testing
// Automated API discovery based on schemantics and vocabularies

// blogApp.factory('schydraAngular', ['$scope','Restangular','$http', '$q', function ($scope, Restangular, $http, $q) {
blogApp.factory('schydraAngular', ['$log','$http', '$q', function ($log,$http, $q) {

  var schydraAngular = {};
  var hydraDocument = '';
  var log = 'Initiated\n';
  var error = '';
  var RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
  var RDFS = $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
  var FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
  var XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
  var SCHEMA = $rdf.Namespace("http://schema.org/");
  var HYDRA = $rdf.Namespace("http://www.w3.org/ns/hydra/core#");

/*

 */
String.prototype.toClassNameSimple = function() {
  var str = this;
  str = str.split('/').pop();
  return str;
}

/*

 */
String.prototype.toClassNameGraph = function() {
  var str = this;
  str = str.split('#').pop();
  return str;
}

/**
 * [This is a test function to get the UIRs from schema classes
 * We should use https://github.com/tc39/proposal-intl-plural-rules
 * but for now is ok]
 * @return {[type]} [description]
 */
String.prototype.toUri = function() {
    var str = this
    var checkEnd = new RegExp(/[s]$/g)
    str = str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1_').toLowerCase();
    if (checkEnd.test(str)) {
        str = str + 'es';
        return str;
    } else {
        str = str + 's';
        return str;
    }
}

String.prototype.toClassName = function() {
  var str = this
  console.log(this);
  // var checkEnd = new RegExp(/[s]$/g)
  str = str.split('/').pop();
  return str.replace('>','');
}

String.prototype.getBase = function() {
  var str = this;
  var parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
  var parts = parse_url.exec( str );
  console.log('Url BASE',parts);
  var result = parts[1]+':'+parts[2]+parts[3];
  result = parts[4] ? result+':'+parts[4] : result;
  return result;
};

String.prototype.getBaseSimple = function() {
  var str = this;
  var parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
  var parts = parse_url.exec( str );
  console.log('Url BASE',parts);
  var result = parts[1]+':'+parts[2]+parts[3];
  result = parts[4] ? result+':'+parts[4] : result;
  return result;
};

String.prototype.splitURI = function() {
  var str = this;
  var parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
  var parts = parse_url.exec( str );
  return parts;
};

function isSchema(property,schema) {
    var schemaUri = SCHEMA(property);
    var tempNum = schema.whether(schemaUri,null,null);
    tempNum = tempNum + schema.whether(null,null,schemaUri);
    if (tempNum===0){
        return false;
    } else {
        return true;
    }
}

// var isContext = new RegExp('^@');
  function isContext(str) {
    var reg = new RegExp('^@');
    return reg.test(str);
  }

/*
 Split empty properties
 Check the value field
 */
function isEmpty(property) {
  if (property===undefined||property===""||property===null) {
    // console.log('property value:',property);
    return true;
  } else {
    return false;
  }
}

/**
 * [isComplete description]
 * @param  {[Object]}  classObj [description]
 * @return {Boolean}          [description]
 */
  function isComplete(classObj) {
    // console.log(classObj);
    return Object.keys(classObj).every(function(prop,k){
      return (isEmpty(classObj[prop]));
    });
  }

  /**
 * [isEmptyClass description]
 * @param  {[Object]}  classObj [description]
 * @return {Boolean}          [description]
 */
  function isEmptyClass(classObj) {
    return Object.keys(classObj).some(function(prop,k){
      if (!(isContext(prop))) {
        if(!(isEmpty(classObj[prop]))) {
          return false;
        }
      }
    });
  }

var datatypeList = ["xmls:dateTime","xmls:integer",'date','datetime','boolean','number','integer','float','text','string','time'];

function isDatatype(property,datatypeList) {
    if (datatypeList.indexOf(property.type)>-1){
        return true;
    } else {
        return false;
    }
}

/*
  Return depth of object hierarchy
 */
function depthOf(object) {
  var level = 1;
  var key;
  for(key in object) {
    if (!object.hasOwnProperty(key)) {continue;}
    if(typeof object[key] === 'object') {
      var depth = depthOf(object[key]) + 1;
      level = Math.max(depth, level);
    }
  }
  return level;
}

/**
 * [createObjects description]
 * @param  {[type]} rawJSON [description]
 * @param  {[type]} graph   [description]
 * @param  {[type]} other   [description]
 * @return {[type]}         [description]
 */
function createObjects(rawJSON,graph,other) {
  var objectArray = {};
  // console.log(rawJSON);
  Object.keys(rawJSON).forEach(function(cl,i){
    if(cl!=='@context') {
      var classID = rawJSON[cl]['@type'];
      objectArray[classID] = function hydraObject() {
        var that = this;
        this['@type'] = classID;
        this['@id'] = '0';
        this['value'] = {};
        this['label'] = 'class label';
        this['actions'] = '';
        this['graph'] = graph;
        this['get'] = function(property,value) {
          if(typeof that.value[property] === "object") {
            if((value === null)||(typeof value === "undefined")||(value === 'value')) {
              if (that.value[property].range==='xmls:dateTime') {
                var date = that.value[property].value;
                var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
                // return dateString;
                return that.value[property].value;
              } else {
                return that.value[property].value;
              }
            } else if(value == 'range') {
              return that.value[property].range;
            } else if (value == 'label')
            return that.value[property].label;
          } else {
            console.log('This class does not contain property:', property);
            return 0;
          }
        };
        this['set'] = function(property,data,value) {
          if(typeof that.value[property] === "object") {
            if((value == null)||(typeof value === "undefined")||(value === 'value')) {
              if (that.value[property].range ==='xmls:dateTime') {
                that.value[property].value = new Date(data);
              } else {
                that.value[property].value = data;
              }
              return that.value[property].value;
            } else if(value === 'range') {
              that.value[property].range = data;
              return that.value[property].range;
            } else if (value === 'label')
            that.value[property].label = data;
            return that.value[property].label;
          } else {
            // console.log('This class does not contain property:', property);
            return 0;
          }
        };
        var actions = {};
        var prop = {};

      Object.keys(rawJSON[cl]).forEach(function(property,k){
      if(property ==='potentialAction') {
        Object.keys(rawJSON[cl]['potentialAction']).forEach(function(action,j){
          var actionType = rawJSON[cl]['potentialAction']['@type'];
          if(actionType ==='searchAction') {
            actions['testAction'] = function() {
              return that.label;
            };
           actions['searchAction'] = function(graph,input,type, action,fetchall) {
            input = input ? input : that['value'];
            type = that['@type'];
            graph = that['graph'];
            action = action ? action : actionType;
            fetchall = (typeof fetchall === 'undefined' || fetchall === null) ? fetchall : true;
            // var result = searchAction(graph,input,type,'');
            return retrieveAction(graph,input,type,actionType,true);
          };//endof function
         } else if(actionType ==='deleteAction') {
          console.log('test');
         }//end of action selection

        });// end of potentian action iteration
      } else if((property !== '@type')&&(property !== '@id')&&(property !== 'potentialAction')) {
        prop[property] = {};
        if(typeof rawJSON[cl][property] === "object") {
          prop[property]['range'] = rawJSON[cl][property]['@type'];
          // console.log('TYPEEE',rawJSON[cl][property]['@type']);
        } else {
          prop[property]['range'] = 'Property range';
        }
        if ( prop[property]['range'] === 'xmls:dateTime') {
            prop[property].get = function() {
              return prop[property].value.toDateString();
          };
          prop[property].set = function(data) {
            prop[property]['value'] = new Date(data);
            return prop[property].value.toJSON();
          };
        } else {
          prop[property].value = '';
          // prop[property].get = function() {
          //   return prop[property]['value'];
          // }
          // prop[property].set = function(data) {
          //   prop[property]['value'] = data;
          //   return true;
          // }
        }
        prop[property].label = 'Property Label';
      }
    });
      this.value = prop;
      this.actions = actions;
    }; //end of definition
  }
});//end of eachclass
  return objectArray;
}//end of createObject

/**
 * [Parse Hydrajsonld as RDF graph and return a promise]
 * @param  {[type]} hydraDoc    [description]
 * @param  {[type]} hydraGraph  [description]
 * @param  {[type]} hydraServer [description]
 * @return {[type]}             [description]
 */
var hydraParsedPromise = function(hydraDoc,hydraGraph,hydraServer) {
  var previousGraph = hydraGraph.statements.length;
    return new Promise(function(resolve, reject) {
        $rdf.parse(hydraDoc, hydraGraph, hydraServer,'application/ld+json',function(error,kb){
          if ((error===null)&&(kb.statements.length>previousGraph)) {
            console.log('kb-length',kb.statements.length);
            log += 'Added ' + kb.statements.length + ' statements to default graph\n';
            resolve(kb);
          } else {
            // reject(error);
          }
        });
    });
};

function parseHydraObject(object,asd) {
  var props = graph.each(null,HYDRA('supportedProperty'),null);
  //retrievePropertyLiterals(node,graph)
  //retrieveHydraProperties(classNode,graph)
}

function parseHydra(graph) {
  console.log('parseHydra-GRAPH',graph);
  var classNodeArray = graph.each(null,RDF('type'),HYDRA('Class'));
  // var collectionNodeArray = graph.each(null,RDF('type'),HYDRA('Link'));
  var collectionNodeArray = retrieveCollectionNodes(graph);//graph.each(null,RDF('type'),HYDRA('PartialCollectionView'));
  var classLiterals = {};
  var collectionLiterals = {};
  var propNodes = [];
  var methodNodes = [];
  classNodeArray.forEach(function(classNode,i){
    var classPropsLiterals = {};
    var classMethodsLiterals = {};
    classLiterals[classNodeArray[i].value] = {};
    classLiterals[classNodeArray[i].value]['literals'] = retrieveClassLiterals(classNodeArray[i],graph);
    [propNodes,methodNodes] = retrieveClass(graph,classNodeArray[i]);

    propNodes.forEach(function(prop,k){
      var currentProp = retrievePropertyLiterals(prop,graph);
      classPropsLiterals[currentProp['label']] = currentProp;
    });
    classLiterals[classNodeArray[i].value]['properties'] = classPropsLiterals;

    methodNodes.forEach(function(prop,k){
      var currentMethod = retrieveOperationLiterals(methodNodes[k],graph);
      classMethodsLiterals[currentMethod['method']] = currentMethod;
    });
    classLiterals[classNodeArray[i].value]['operations'] = classMethodsLiterals;
  });
  collectionNodeArray.forEach(function(collectionNode,i){
    var collectionActionsLiterals = {};
    var collectionMethodsLiterals = {};
    var collectionNamedNode = graph.the(null,RDFS('range'),collectionNode);
    var pred =  graph.the(null,HYDRA('property'),collectionNode);
    console.log(pred);
    var currentProp = retrieveHydraPropertyLiterals(collectionNamedNode,graph);
    collectionLiterals[collectionNamedNode.value] = {};
    collectionLiterals[collectionNamedNode.value]['literals'] = currentProp;
    [propNodes,methodNodes] = retrieveClass(graph,collectionNamedNode);
    // collectionNodeArray[currentProp['label']] = currentProp;
    methodNodes.forEach(function(prop,k){
      var currentMethod = retrieveActionLiterals(methodNodes[k],graph);
      var actionName = currentMethod['potentialAction'].toClassNameSimple();
      var methodName = currentMethod['method'].toClassNameSimple();
      collectionActionsLiterals[actionName] = currentMethod;
      collectionMethodsLiterals[methodName] = currentMethod;
    });
    collectionLiterals[collectionNamedNode.value]['actions'] = collectionActionsLiterals;
    collectionLiterals[collectionNamedNode.value]['operations'] = collectionMethodsLiterals;
  });
  console.log(classLiterals);
  console.log(collectionLiterals);
  return [classLiterals,collectionLiterals];
}

function assignDuplicates(graph) {
  return true;
}

function clusterClasses(nodeArray,graph) {
  var clusters = {};
  nodeArray.forEach(function(classNode,i){
    var types = graph.each(classNode,RDF('type'),null);
    var counter = 0;
    var classTypes = [];
    types.forEach(function(cl,i){
      if (types[i].value!==HYDRA('Class').value) {
        classTypes[counter] = types[i].value;
        counter++;
      }
    });
    classTypes.forEach(function(type,k){
      if (clusters.hasOwnProperty(type)) {
        clusters[type].push(classNode);
      } else {
        clusters[type] = [];
        clusters[type].push(classNode);
      }
    });
  });
  return clusters;
}

function assignProperties(propertiesLiterals) {
  var prop = {};
  Object.keys(propertiesLiterals).forEach(function(property,k){
    var obj = propertiesLiterals[property] ;
    prop[property] = {};
    if((property !== '@type')&&(property !== '@id')&&(property !== 'potentialAction')) {
      prop[property] = obj;
      prop[property]['value'] = {};
      obj['types'].forEach(function(value,i){
        if (value === HYDRA('Link').value) {
          // prop[property]['types'] = 
        }
      })
      prop[property].label = 'Property Label';
    }
  });
  return prop;
}

function assignActions(actionLiterals,hydraObject,action,referenceObject,instance) {
  console.log('Assigning Actions');
  console.log('actionLiterals',actionLiterals);
  if (!(instance)) {
    instance = false;
  }
  var meth = actionLiterals['method'];
  var data = {};
  Object.keys(hydraObject.value).forEach(function(property,i){
    data[property] = hydraObject.value[property].value;
  });
  console.log('payload of HTTP request: ',data);
  // var url = 'http://0.0.0.0:8091/software_applications';
  var url;
  if ((typeof hydraObject['@id'] === 'undefined')||(hydraObject['@id'] === '')) {
    if ((actionLiterals['target'] === 'undefined')||(actionLiterals['target'] === '')) {
      console.log('No URL given, check @id field of hydraClass or hydraCollection. Or Action target property is missing.');
      return false;
    } else {
      url = actionLiterals['target'];
    }
  } else {
    url = hydraObject['@id'];
  }
  var options;
  var defaultOptions = {
    method: meth,
    headers: {'Content-Type': 'application/ld+json'},
    url: url
  };
  //switch based on action name, if nothing special proceed based on http method
  switch(action) {
    case 'schemasearch':
      console.log('Creating search Action');
      return null;
      break;
    case 'schemaAdd':
      break;
    case 'schema':
      break;
    case 'schemaTest':
      break;
  }
  if(instance) {
    switch(meth) {
      case 'DELETE':
          return $http(defaultOptions);
      break;
      case 'GET':
        return hydraResponsePromise(defaultOptions,referenceObject);
      break;
      case 'PUT':
          defaultOptions.data = data;
          return $http(defaultOptions);
      break;
      case 'POST':
          defaultOptions.data = data;
          return $http(defaultOptions);
      break;
    }
  } else {
    switch(meth) {
      case 'GET':
        return hydraResponsePromise(defaultOptions,referenceObject);
        break;
      case 'DELETE':
        return $http(defaultOptions);
        break;
      case 'PUT':
          defaultOptions.data = data;
          return $http(defaultOptions);
        break;
        case 'POST':
          defaultOptions.data = data;
          return $http(defaultOptions);
        break;
    }
  }
}

// var actionType = rawJSON[cl]['potentialAction']['@type'];
//           if(actionType ==='searchAction') {
//             actions['testAction'] = function() {
//               return that.label;
//             };
//            actions['searchAction'] = function(graph,input,type, action,fetchall) {
//             input = input ? input : that['value'];
//             type = that['@type'];
//             graph = that['graph'];
//             action = action ? action : actionType;
//             fetchall = (typeof fetchall === 'undefined' || fetchall === null) ? fetchall : true;
//             // var result = searchAction(graph,input,type,'');
//             return retrieveAction(graph,input,type,actionType,true);
//           };//endof function
//          } else if(actionType ==='deleteAction') {
//           console.log('test');
//          }//end of action selection

//         });// end of potentian action iteration

var parseHydraObj = function(members,referenceObj,hydraObject) {
  members = Array.isArray(members) ? members : [members];
  var currentType = members[0]['@type'].split('/').pop();
  var tempObj = new referenceObj[currentType];
  var responseObjects = [];
  // console.log(members[0]);
  // console.log(members);
  var returnObj;
  if((hydraObject === null)||(typeof hydraObject === "undefined")||(hydraObject === '')) {
    if (referenceObj) {
      returnObj = new referenceObj[currentType];
    } else {
      return null;
    }
  } else {
    returnObj = hydraObject;
  }

  if (members[0]['@type']!==tempObj['type']) {
    console.log('Wrong reference Class type, collection contains instances of '+members[0]['@type']+' reference object is of type '+ referenceObj['type']);
  } else {
    members.forEach(function(memberObj,k){
      var classType = memberObj['@type'].split('/').pop();
      var returnObj;
      if((hydraObject === null)||(typeof hydraObject === "undefined")||(hydraObject === '')) {
        if (referenceObj[currentType]) {
          returnObj = new referenceObj[currentType];
        } else {
          return null;
        }
      } else {
        returnObj = hydraObject;
      }
      // console.log('EXW obj:',memberObj);
      returnObj['@id'] = memberObj['@id'];
      Object.keys(memberObj).forEach(function(prop){
        if (!(isContext(prop))) {
           // returnObj.value[prop] = {};
          if (( typeof memberObj[prop] === "object")&&( memberObj[prop])) {
            // console.log('Exw nested', prop);
            returnObj.set(prop,parseHydraObj(memberObj[prop],referenceObj));
          } else {
            returnObj.set(prop,memberObj[prop]);
          }
        }
      });
      responseObjects.push(returnObj);
    });
  }
  return responseObjects.length===1 ? responseObjects[0] : responseObjects;
}

var hydraResponsePromise = function(options,referenceObject) {
  return new Promise(function(resolve,reject) {
    $http(options)
    .then(function(response){
      // pasre response
      console.log('Raw hydra response, before saving',response);
      var parsed = parseHydraObj(response.data['hydra:member'][0],referenceObject);
      resolve({response: response, parsedObjects: parsed});
    },
    function(error) { reject(error); } );
  });
};

function assignMethods(methodLiterals,hydraObject,meth,referenceObject,instance) {
 if (!(instance)) {
    instance = false;
  }
  console.log('Assigning Methods');
  // console.log('meth',meth);
  // console.log(hydraObject);
  // console.log('methodLiterals',methodLiterals);
  var data = {};
  Object.keys(hydraObject.value).forEach(function(property,i){
    data[property] = hydraObject.value[property].value;
  });
  console.log('payload of HTTP request: ',data);
  // var url = 'http://0.0.0.0:8091/software_applications';
  var url;
  if (typeof hydraObject['@id'] === 'undefined') {
    console.log('No URL given, check @id field of hydraClass or hydraCollection.');
    return false;
    url = '';
  } else {
    url = hydraObject['@id'];
  }
  var options;
  var defaultOptions = {
    method: meth,
    headers: {'Content-Type': 'application/ld+json'},
    url: url
  };
  if(instance) {
    switch(meth) {
      case 'DELETE':
          return $http(defaultOptions);
      break;
      case 'GET':
        return hydraResponsePromise(defaultOptions,referenceObject);
      break;
      case 'PUT':
          defaultOptions.data = data;
          return $http(defaultOptions);
      break;
      case 'POST':
          defaultOptions.data = data;
          return $http(defaultOptions);
      break;
    }
  } else {
    switch(meth) {
      case 'GET':
        return hydraResponsePromise(defaultOptions,referenceObject);
        break;
      case 'DELETE':
        return $http(defaultOptions);
        break;
      case 'PUT':
          defaultOptions.data = data;
          return $http(defaultOptions);
        break;
        case 'POST':
          defaultOptions.data = data;
          return $http(defaultOptions);
        break;
    }
  }
}

function commonNode(classArray) {
  return classArray[0];
}

function createProtos(graph,keepAll) {
  console.log('Creating protoype Objects - createProtos');
  var hydraProtoClass = {};
  var hydraProtoCollection = {};
  var hydraProtoClassList = {};
  var classNodeArray = graph.each(null,RDF('type'),HYDRA('Class'));
  var clusterClassArray = clusterClasses(classNodeArray,graph);
  var classLiterals = {};
  var collectionLiterals = {};
  var propNodes = [];
  var methodNodes = [];
  console.log('clusterClassArray',clusterClassArray);
  Object.keys(clusterClassArray).forEach(function(classArray,l){
    // smash common node classes
    var current = clusterClassArray[classArray];
    var selectedClassNode = [];
    if (current.length === 1) {
      //unique class
      selectedClassNode.push(current[0]);
    } else {
      // we have current class type served by many APIs
      if(keepAll) {
        // select only one class
        selectedClassNode.push(commonNode(current));
      } else {
        //create multiple objects for same class
        // selectedClassNode = selectedClassNode.concat(assignDuplicates(current,graph));
      }
    }
    //Parse selected Classes and create objects
    selectedClassNode.forEach(function(classNode,i){
      var classPropsLiterals = {};
      var classMethodsLiterals = {};
      var classActionsLiterals = {};
      classLiterals[classNode.value] = {};
      classLiterals[classNode.value]['literals'] = retrieveClassLiterals(classNode,graph);
      [propNodes,methodNodes] = retrieveClass(graph,classNode);

      propNodes.forEach(function(prop,k){
        var currentProp = retrievePropertyLiterals(prop,graph);
        classPropsLiterals[currentProp['label']] = currentProp;
      });
      classLiterals[classNode.value]['properties'] = classPropsLiterals;

      methodNodes.forEach(function(prop,k){
        var currentMethod = retrieveOperationLiterals(methodNodes[k],graph);
        classMethodsLiterals[currentMethod['method']] = currentMethod;
      });
      methodNodes.forEach(function(prop,k){
        var currentAction = retrieveActionLiterals(methodNodes[k],graph);
        classActionsLiterals[currentAction['potentialAction']] = currentAction;
      });

      classLiterals[classNode.value]['operations'] = classMethodsLiterals;
      classLiterals[classNode.value]['actions'] = classActionsLiterals;

      hydraProtoClass[classNode.value.toClassNameGraph()] = function hydraClass() {
        var that = this;
        this['@id'] = '';
        this['type'] = classLiterals[classNode.value]['literals']['classTypes'][0];
        this['value'] = assignProperties(classPropsLiterals);
        this['label'] = classLiterals[classNode.value]['literals']['label'];
        this['graph'] = graph;
        this['get'] = function(property,spec) {
          var out;
          if(typeof that.value[property] === "object") {
            if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
              if ((that.value[property].range===XSD('dateTime').value)||(that.value[property].range==='xmls:dateTime')) {
                var date = that.value[property].value;
                var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
                // return dateString;
                return that.value[property].value;
              } else if (that.value[property].range===XSD('date')) {
                var date = that.value[property].value;
                var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
                // return dateString;
                return that.value[property].value;
              } else {
                out = that.value[property].value ? that.value[property].value : false;
                return out;
              }
            } else if( typeof spec === 'string') {
                out = that.value[property].spec ? that.value[property].spec : false;
                return out;
            } else {
              console.log('This class does not contain property:', property);
              return false;
            }
          } else {
            console.log('This class does not contain property:', property);
            return false;
          }
        };
        this['set'] = function(property,data,spec) {
          var saved;
          if(typeof that.value[property] === "object") {
            if((spec == null)||(typeof spec === "undefined")||(spec === 'value')) {
              if ((that.value[property].range===XSD('dateTime').value)||(that.value[property].range==='xmls:dateTime')) {
                that.value[property].value = new Date(data);
              } else {
                that.value[property].value = data;
              }
              return that.value[property].value;
            } else if (typeof spec === 'string') {
                saved = that.value[property].spec ? that.value[property].spec : false;
                that.value[property].spec = data;
                return saved;
            } else
              console.log('This class does not contain property:', property);
              return false;
          } else {
            // console.log('This class does not contain property:', property);
            return 0;
          }
        };
        this['actions'] = {};
        this['methods'] = {};
        Object.keys(classLiterals[classNode.value]['operations']).forEach(function(meth,i){
            that['methods'][meth.toLowerCase()] = function() {
              return assignMethods(classLiterals[classNode.value]['operations'][meth],that,meth,'',true);
            }
        });
        Object.keys(classLiterals[classNode.value]['actions']).forEach(function(actionName,i){
          // console.log('classLiterals[classNode.value]',classLiterals[classNode.value]['actions'][actionName]);
            that['actions'][actionName] = function() {
              return assignActions(classLiterals[classNode.value]['actions'][actionName],that,actionName,'',true);
            }
        });
      }
    })
  });
  // var collectionNodeArray = graph.each(null,RDF('type'),HYDRA('Link'));
  var collectionNodeArray = retrieveCollectionNodes(graph);//graph.each(null,RDF('type'),HYDRA('PartialCollectionView'));
  var collectionLiterals = {};
  var propNodes = [];
  var methodNodes = [];
  collectionNodeArray.forEach(function(collectionNode,i){
    var collectionActionsLiterals = {};
    var collectionMethodsLiterals = {};
    var collectionNamedNode = graph.the(null,RDFS('range'),collectionNode);
    // console.log('collectionNamedNode',collectionNamedNode);
    var pred =  graph.the(null,HYDRA('property'),collectionNode);
    // console.log(pred);
    var currentProp = retrieveHydraPropertyLiterals(collectionNamedNode,graph);
    collectionLiterals[collectionNamedNode.value] = {};
    collectionLiterals[collectionNamedNode.value]['literals'] = currentProp;
    [propNodes,methodNodes] = retrieveClass(graph,collectionNamedNode);
    // collectionNodeArray[currentProp['label']] = currentProp;
    methodNodes.forEach(function(prop,k){
      var currentMethod = retrieveActionLiterals(methodNodes[k],graph);
      var actionName = currentMethod['potentialAction'].toClassNameSimple();
      var methodName = currentMethod['method'].toClassNameSimple();
      collectionActionsLiterals[actionName] = currentMethod;
      collectionMethodsLiterals[methodName] = currentMethod;
    });
    collectionLiterals[collectionNamedNode.value]['actions'] = collectionActionsLiterals;
    collectionLiterals[collectionNamedNode.value]['operations'] = collectionMethodsLiterals;
    var collectionClassType = collectionLiterals[collectionNamedNode.value]['literals']['range'];
    // create Collection Array Objects
    hydraProtoCollection[collectionClassType.toClassNameGraph()] = function hydraCollection(classRef) {
      var that = this;
      this['@id'] = collectionLiterals[collectionNamedNode.value]['literals']['id'];
      this['member'] = collectionLiterals[collectionNamedNode.value]['literals']['member'];
      this['label'] = collectionLiterals[collectionNamedNode.value]['literals']['label'];
      this['value'] = [];
      this['obj'] = hydraProtoClass;//classRef;
      this['graph'] = graph;
      this['get'] = function(property,spec) {
        var out;
        if(typeof that.value[property] === "object") {
          if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
            if ((that.value[property].range===XSD('dateTime').value)||(that.value[property].range==='xmls:dateTime')) {
              var date = that.value[property].value;
              var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
              // return dateString;
              return that.value[property].value;
            } else if (that.value[property].range===XSD('date')) {
              var date = that.value[property].value;
              var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
              // return dateString;
              return that.value[property].value;
            } else {
              out = that.value[property].value ? that.value[property].value : false;
              return out;
            }
          } else if( typeof spec === 'string') {
              out = that.value[property].spec ? that.value[property].spec : false;
              return out;
          } else {
            console.log('This class does not contain property:', property);
            return false;
          }
        } else {
          console.log('This class does not contain property:', property);
          return false;
        }
      };
      this['set'] = function(property,data,spec) {
        var saved;
        if(typeof that.value[property] === "object") {
          if((spec == null)||(typeof spec === "undefined")||(spec === 'value')) {
            if ((that.value[property].range===XSD('dateTime').value)||(that.value[property].range==='xmls:dateTime')) {
              that.value[property].value = new Date(data);
            } else {
              that.value[property].value = data;
            }
            return that.value[property].value;
          } else if (typeof spec === 'string') {
              saved = that.value[property].spec ? that.value[property].spec : false;
              that.value[property].spec = data;
              return saved;
          } else
            console.log('This class does not contain property:', property);
            return false;
        } else {
          // console.log('This class does not contain property:', property);
          return 0;
        }
      };
      this['actions'] = {};
      this['methods'] = {};
      Object.keys(collectionLiterals[collectionNamedNode.value]['operations']).forEach(function(meth,i){
        that['methods'][meth.toLowerCase()] = function() {
          return assignMethods(collectionLiterals[collectionNamedNode.value]['operations'][meth],that,meth,that['obj'],false);
        }
      });
      Object.keys(collectionLiterals[collectionNamedNode.value]['actions']).forEach(function(actionName,i){
        console.log('collectionLiterals[collectionNamedNode.value]',collectionLiterals[collectionNamedNode.value]['actions'][actionName]);
        that['actions'][actionName] = function() {
          return assignActions(collectionLiterals[collectionNamedNode.value]['actions'][actionName],that,actionName,that['obj'],false);
        }
      });
    }

  });

  // console.log('hydraProtoClass',hydraProtoClass);
  // console.log(classLiterals);
  // console.log(collectionLiterals);
  return [classLiterals,collectionLiterals,hydraProtoClass,hydraProtoCollection];
}

// function bindMethod(classInstance,collectionInstance,method,protocol) {
//   var item = classInstance ? true : false; // choose whether we have item or collection
//   var id = item ? classInstance.id : collectionInstance.id;
//   // var url = collectionInstance ? collectionInstance.id : null;
//   var data = classInstance ? classInstance.data : null;
//   var out = {};
//   var defaultOptions = {
//     method: method,
//     headers: {'Content-Type': 'application/ld+json'},
//     url: id
//   };

//   if(item) {
//     switch(method) {
//       case 'DELETE','GET':
//         out[method] = function() {
//           return $http(defaultOptions);
//         }
//       break;
//       case 'PUT':
//         out[method] = function() {
//           defaultOptions.data = data;
//           return $http(defaultOptions);
//         }
//       break;
//     }
//   } else {
//     switch(method) {
//       case 'GET':
//         out[method] = function() {
//           return $http(defaultOptions);
//         }
//         break;
//       case 'PUT', 'POST':
//         out[method] = function() {
//           defaultOptions.data = data;
//           return $http(defaultOptions);
//         }
//         break;
//       }
//   }
//   return out[method];
// }
/*
    Retrieve the properties a schema Class may contain
    input: class name only
    output: array of NameNodes
 */
function retrieveHydraProperties(classNode,graph) {
    var props = graph.each(classNode,HYDRA('supportedProperty'),null);
    var methods = graph.each(classNode,HYDRA('supportedOperation'),null);

    props.forEach(function(prop,k){
      // console.log(props[k]);
      var test = graph.each(props[k],HYDRA('property'),null);
      console.log('test',test);

      console.log(graph.each(props[k],HYDRA('title'),null));
      console.log('Test2:',graph.each(test[0],RDF('type'),null));

      test.forEach(function(bl,l){
        var test2 = graph.each(test[l],RDF('type'),null);
        console.log('RDF(type)',test2);
      })
      console.log(test);
    });
    console.log('subClassOf PostalAddress',props);
    return props;
}

/**
 * [retrieveClass description]
 * @return {[Array of Blank nodes]} [description]
 */
  function retrieveClass(graph,classNode) {
    var props = graph.each(classNode,HYDRA('supportedProperty'),null);
    var methods = graph.each(classNode,HYDRA('supportedOperation'),null);
    // test = {};
    // test['@type'] = 'Airport';
    // searchAction(graph,test,null,null);
    return [props,methods];
  }

/**
 * [retrieveCollectioNodes description]
 * @param  {[type]} graph [description]
 * @return {[type]}       [description]
 */
  function retrieveCollectionNodes(graph) {
    var collectionNodes = [];
    var subnodes = graph.each(null,RDF('type'),HYDRA('PartialCollectionView'));
   subnodes = subnodes.concat(graph.each(null,RDF('type'),HYDRA('Collection')));
    console.log('subnodes',subnodes);
    // subnodes.forEach(function(a,i){
    //   collectionNodes.push(graph.each(null,null,subnodes[i])[0]);
    //   // var temp = graph.each(null,null,subnodes[i]);
    //   // console.log('nonono',temp);
    // });
    // console.log('CollectionNodes:',collectionNodes);
    return subnodes;
  }

function retrieveOperationLiterals(node,graph) {
  var props = {};
  props.method = graph.each(node,HYDRA('method'),null)[0] ? graph.each(node,HYDRA('method'),null)[0].value : null;
  props.title = graph.each(node,HYDRA('title'),null)[0] ? graph.each(node,HYDRA('title'),null)[0].value : null;
  props.label = graph.the(node,RDFS('label'),null) ? graph.the(node,RDFS('label'),null).value : null;
  props.returns = graph.each(node,HYDRA('returns'),null)[0] ? graph.each(node,HYDRA('returns'),null)[0].value : null;
  props.expects = graph.each(node,HYDRA('expects'),null)[0] ? graph.each(node,HYDRA('expects'),null)[0].value : null;
  return props;
}

function retrieveHydraPropertyLiterals(node,graph) {
  var props = {};
  props.id = node ? node.value : null ;
  props.label = graph.each(node,RDFS('label'),null)[0] ? graph.each(node,RDFS('label'),null)[0].value : null;
  props.domain = graph.each(node,RDFS('domain'),null)[0] ? graph.each(node,RDFS('domain'),null)[0].value : null;
  var rangeBlankNode = graph.each(node,RDFS('range'),null)[0] ? graph.each(node,RDFS('range'),null)[0] : null;
  props.range = graph.each(node,RDFS('range'),null)[0] ? graph.each(node,RDFS('range'),null)[0].value : null;
  var memberBlanknode = graph.the(rangeBlankNode,HYDRA('member'),null) ? graph.the(rangeBlankNode,HYDRA('member'),null) : null;
  var memberType;
  if (memberBlanknode) {
    memberType = graph.each(memberBlanknode,RDF('type'),null) ? graph.each(memberBlanknode,RDF('type'),null)[0] : null;
    // console.log('memberType',memberType);
    var classTypeArray = graph.each(memberType,RDF('type'),null) ? graph.each(memberType,RDF('type'),null) : null;
    var counter = 0;
    var classTypes = [];
    classTypeArray.forEach(function(cl,i){
      if (classTypeArray[i].value!==HYDRA('Class').value) {
        classTypes[counter] = classTypeArray[i].value;
        counter++;
      }
    })
    props.member = classTypes[0];
    props.range = memberType.value;
  }

  var propTypes = graph.each(node,RDF('type'),null);
  var counter = 0;
  props.types = [];
  propTypes.forEach(function(cl,i){
    if (propTypes[i].value!==RDF('Property').value) {
      props.types[counter] = propTypes[i].value;
      counter++;
    }
  });
  return props;
}

function retrieveActionLiterals(node,graph) {
  var props = {};
  props.method = graph.each(node,HYDRA('method'),null)[0] ? graph.each(node,HYDRA('method'),null)[0].value : null;
  props.title = graph.each(node,HYDRA('title'),null)[0] ? graph.each(node,HYDRA('title'),null)[0].value : null;
  props.label = graph.the(node,RDFS('label'),null) ? graph.the(node,RDFS('label'),null).value : null;
  props.returns = graph.each(node,HYDRA('returns'),null)[0] ? graph.each(node,HYDRA('returns'),null)[0].value : null;
  props.expects = graph.each(node,HYDRA('expects'),null)[0] ? graph.each(node,HYDRA('expects'),null)[0].value : null;
  props.target = graph.each(node,SCHEMA('target'),null)[0] ? graph.each(node,SCHEMA('target'),null)[0].value : null;
  props.result = graph.each(node,SCHEMA('result'),null)[0] ? graph.each(node,SCHEMA('result'),null)[0].value : null;
  props.query = graph.each(node,SCHEMA('query'),null)[0] ? graph.each(node,SCHEMA('query'),null)[0].value : null;
  props.object = graph.each(node,SCHEMA('object'),null)[0] ? graph.each(node,SCHEMA('object'),null)[0].value : null;
  var propTypes = graph.each(node,RDF('type'),null);
  var counter = 0;
  // props.types = [];
  props.potentialAction = '';
  propTypes.forEach(function(cl,i){
    if (propTypes[i].value!==HYDRA('operation').value) {
      props.potentialAction = propTypes[i].value;
      var actionURI = props.potentialAction.splitURI();
      var actionName = actionURI[3].split('.')[0] + actionURI[5].replace(/Action/i,"");
      // console.log('actionName',actionName);
      props.potentialAction = actionName ;
      counter++;
    }
  });
  return props;
}

function retrievePropertyLiterals(node,graph) {
  var props = {};
  // searchOp['method'] = graph.each(node,HYDRA('method'),null)[0] ? graph.each(node,HYDRA('method'),null)[0].value : null;

  props.title = graph.each(node,HYDRA('title'),null)[0] ? graph.each(node,HYDRA('title'),null)[0].value : null;
  props.description = graph.each(node,HYDRA('description'),null)[0] ? graph.each(node,HYDRA('description'),null)[0].value : null;
  props.required = graph.each(node,RDFS('required'),null)[0] ? graph.each(node,RDFS('required'),null)[0].value : null;
  props.readable = graph.each(node,HYDRA('readable'),null)[0] ? graph.each(node,HYDRA('readable'),null)[0].value : null;
  props.writable = graph.each(node,HYDRA('writable'),null)[0] ? graph.each(node,HYDRA('writable'),null)[0].value : null;
  var hydraProps = graph.each(node,HYDRA('property'),null);
  var propses = graph.each(hydraProps[0],null,null);
  // console.log('hydraProps',propses);
  var out = Object.assign({},props,retrieveHydraPropertyLiterals(hydraProps[0],graph));
  return out;
}

function retrieveClassLiterals(node,graph) {
  var props = {};
  // searchOp['method'] = graph.each(node,HYDRA('method'),null)[0] ? graph.each(node,HYDRA('method'),null)[0].value : null;
  props.title = graph.each(node,HYDRA('title'),null)[0] ? graph.each(node,HYDRA('title'),null)[0].value : null;
  props.label = graph.the(node,RDFS('label'),null) ? graph.the(node,RDFS('label'),null).value : null;
  var types = graph.each(node,RDF('type'),null);
  var counter = 0;
  props.classTypes = [];
  types.forEach(function(cl,i){
    if (types[i].value!==HYDRA('Class').value) {
      props.classTypes[counter] = types[i].value;
      counter++;
    }
  })
  return props;
}

function retrieveSearchLiterals(node,graph) {
  var props = {};
  props.property = graph.each(node,HYDRA('property'),null)[0] ? graph.each(node,HYDRA('property'),null)[0].value : null;
  props.variable = graph.each(node,HYDRA('variable'),null)[0] ? graph.each(node,HYDRA('variable'),null)[0].value : null;
  props.required = graph.each(node,RDFS('required'),null)[0] ? graph.the(node,HYDRA('required'),null)[0].value : false;
  return props;
}

var schemaSearchAction = function(graph,data,actionOperationProps,actionType,fetchAll,options){
// var schemaSearchAction = function(actionLiterals,hydraObject,action,referenceObject,instance,options)(graph,data,actionOperationProps,actionType,fetchAll){
  console.log('Action initiated');
  console.log('Action Data Object',data);
  console.log('Action Class Type',classType);
  console.log('Action Graph',graph);
  console.log('actionOperations',actionOperations);

  var actionLiterals,hydraObject,action,referenceObject,instance,options;
  var actionOperations = graph.each(null,RDF('type'),SCHEMA(actionType)); //returns all nodes with selected action
  var actionNodes = []; //gather the nodes of search
  var baseUrl = options.getBase(); //get base server url
  var actionTargets = [];
  // var actionOperationProps = [];
  var operationSubjects = [];// gnwsto
  var subjectTypes = [];
  var urlTemplate = [];
  var searchMaps = [];
  var promiseArray = [];
  var finalPromise = $q.defer();

  actionOperations.forEach(function(opNode,i){
    var actionObj = graph.each(actionOperations[i],SCHEMA('object'),null);
    if (actionObj[0].value.toClassName()===classType) {
      var actionTarget = graph.each(actionOperations[i],SCHEMA('target'),null);
      var operationSubject = graph.each(null,HYDRA('supportedOperation'),actionOperations[i]);
      var subjectType = graph.each(operationSubject[0],RDF('type'),null);
      actionNodes.push(actionObj[0]);
      baseUrl.push(actionObj[0].value.getBase());
      actionTargets.push(actionTarget[0]);
      operationSubjects.push(operationSubject[0]);
      subjectTypes.push(subjectType[0]);
      actionOperationProps.push(retrieveOperationLiterals(actionOperations[i],graph));
    }
  });

  operationSubjects.forEach(function(node,i){
    // console.log('subjectTypes[i].value',subjectTypes[i].value);
    if (subjectTypes[i].value===HYDRA('Class').value) {
      //We got actions on class instances
    } else {
      //We suppose we have Collections
      var rangeObj = graph.each(operationSubjects[i],RDFS('range'),null);
      var rangeType = graph.each(rangeObj[0],RDF('type'),null);
      // console.log('rangeType[0].value',rangeType[0].value);
      if ((rangeType[0].value===HYDRA('PartialCollectionView').value)||(rangeType[0].value===HYDRA('Collection').value)) {
        var hydraSearchNode = graph.each(rangeObj[0],HYDRA('search'),null);
        var hydraRepresenation = graph.each(hydraSearchNode[0],HYDRA('variableRepresentation'),null);
        var hydraTemplate = graph.each(hydraSearchNode[0],HYDRA('template'),null);
        var hydraMapping = graph.each(hydraSearchNode[0],HYDRA('mapping'),null);
        urlTemplate.push(urltemplate.parse(hydraTemplate[0].value));
        var currentMapping = [];
        hydraMapping.forEach(function(map,j){
          currentMapping[j] = retrieveSearchLiterals(hydraMapping[j],graph);
        });
        searchMaps.push(currentMapping);
        console.log('Search URI template',hydraTemplate[0].value);
      }
    }// searchAction on Collection
  });
  if (!(fetchAll)) {
    urlTemplate = urlTemplate.slice(0,1);
    searchMaps = searchMaps.slice(0,1);
  }
  urlTemplate.forEach(function(tmpl,i){
    var url = generateSearchURL(urlTemplate[i],searchMaps[i],data);
    var options = {
      method: actionOperationProps[i].method,
      url: baseUrl[i]+url,
      headers: {
        'Content-Type': 'application/ld+json'
      },
      data: data
    };
    var deferred = $q.defer();
    promiseArray.push(deferred.promise);
    $http(options)
    .then(function(response){
        log += 'Server: ' + baseUrl[i] + '\n';
        log += 'Search request ' + url + '\n';
        var collectionData = response.data['hydra:member'][0];
        if (Array.isArray(collectionData)) {
          collectionData.forEach(function(member,k){
            collectionData[k]['@id'] = baseUrl[i] + collectionData[k]['@id'];
          });
        }
        deferred.resolve(response.data['hydra:member']);
        console.log('search response:',response);
      })
      .catch(function(error){
        deferred.resolve(error);
        console.log(error);
      });
  });

  $q.all(promiseArray)
  .then(function(resolved) {
    console.log("ALL INITIAL PROMISES RESOLVED");
    console.log('resolved',resolved);
    var temp = [];//Array.isArray()
    var resolvedData = [];
    if (Array.isArray(resolved)) {
      resolved.forEach(function(prom,i){
        if (Array.isArray(resolved[i])){
          if (resolved[i].length>0) {
            resolvedData = resolvedData.concat(resolved[i][0]);
          }
        }
      });
    }
    console.log('resolvedData',resolvedData);
    finalPromise.resolve(resolvedData);
    })
  .catch(function(resolved){
    $log.debug('One or more promises failed.');
    console.log('resolved',resolved);
    var temp = [];
    if (typeof resolved === "object") {

    } else {
      temp = resolved;
    }
    var resolvedData = [];
    temp.forEach(function(prom,i){
      if ((temp[i].length!==0)){
        resolvedData = resolvedData.concat(temp[i][0]);
      }
    });
    console.log('resolvedData',resolvedData);
    finalPromise.resolve(resolvedData);
    // finalPromise.resolve(resolved);
    // deferred.reject();
  });
  return finalPromise.promise;

}


var retrieveAction = function(graph,data,classType,actionType,fetchAll){
  console.log('Action initiated');
  console.log('Action Data Object',data);
  console.log('Action Class Type',classType);
  console.log('Action Graph',graph);
  var actionOperations = graph.each(null,RDF('type'),SCHEMA(actionType)); //returns all nodes with selected action
  console.log('actionOperations',actionOperations);
  var actionNodes = []; //gather the nodes of search
  var baseUrl = []; //get base server url
  var actionTargets = [];
  var actionOperationProps = [];
  var operationSubjects = [];
  var subjectTypes = [];
  var urlTemplate = [];
  var searchMaps = [];
  var promiseArray = [];
  var finalPromise = $q.defer();

  actionOperations.forEach(function(opNode,i){
    var actionObj = graph.each(actionOperations[i],SCHEMA('object'),null);
    if (actionObj[0].value.toClassName()===classType) {
      var actionTarget = graph.each(actionOperations[i],SCHEMA('target'),null);
      var operationSubject = graph.each(null,HYDRA('supportedOperation'),actionOperations[i]);
      var subjectType = graph.each(operationSubject[0],RDF('type'),null);
      actionNodes.push(actionObj[0]);
      baseUrl.push(actionObj[0].value.getBase());
      actionTargets.push(actionTarget[0]);
      operationSubjects.push(operationSubject[0]);
      subjectTypes.push(subjectType[0]);
      actionOperationProps.push(retrieveOperationLiterals(actionOperations[i],graph));
    }
  });
  //Special treatment for searchAction, we bind the Hydra mappings
  if (actionType==='searchAction') {
    // console.log('operationSubjects',operationSubjects)
    operationSubjects.forEach(function(node,i){
      // console.log('subjectTypes[i].value',subjectTypes[i].value);
      if (subjectTypes[i].value===HYDRA('Class').value) {
        //We got actions on class instances
      } else {
        //We suppose we have Collections
        var rangeObj = graph.each(operationSubjects[i],RDFS('range'),null);
        var rangeType = graph.each(rangeObj[0],RDF('type'),null);
        // console.log('rangeType[0].value',rangeType[0].value);
        if ((rangeType[0].value===HYDRA('PartialCollectionView').value)||(rangeType[0].value===HYDRA('Collection').value)) {
          var hydraSearchNode = graph.each(rangeObj[0],HYDRA('search'),null);
          var hydraRepresenation = graph.each(hydraSearchNode[0],HYDRA('variableRepresentation'),null);
          var hydraTemplate = graph.each(hydraSearchNode[0],HYDRA('template'),null);
          var hydraMapping = graph.each(hydraSearchNode[0],HYDRA('mapping'),null);
          urlTemplate.push(urltemplate.parse(hydraTemplate[0].value));
          var currentMapping = [];
          hydraMapping.forEach(function(map,j){
            currentMapping[j] = retrieveSearchLiterals(hydraMapping[j],graph);
          });
          searchMaps.push(currentMapping);
          console.log('Search URI template',hydraTemplate[0].value);
        }
      }// searchAction on Collection
    });
    if (!(fetchAll)) {
      urlTemplate = urlTemplate.slice(0,1);
      searchMaps = searchMaps.slice(0,1);
    }
    urlTemplate.forEach(function(tmpl,i){
      var url = generateSearchURL(urlTemplate[i],searchMaps[i],data);
      var options = {
        method: actionOperationProps[i].method,
        url: baseUrl[i]+url,
        headers: {
          'Content-Type': 'application/ld+json'
        },
        data: data
      };
      var deferred = $q.defer();
      promiseArray.push(deferred.promise);
      $http(options)
      .then(function(response){
          log += 'Server: ' + baseUrl[i] + '\n';
          log += 'Search request ' + url + '\n';
          var collectionData = response.data['hydra:member'][0];
          if (Array.isArray(collectionData)) {
            collectionData.forEach(function(member,k){
              collectionData[k]['@id'] = baseUrl[i] + collectionData[k]['@id'];
            });
          }
          deferred.resolve(response.data['hydra:member']);
          console.log('search response:',response);
        })
        .catch(function(error){
          deferred.resolve(error);
          console.log(error);
        });
    });
  } else {
    // not searchAction
  } // actionType
  $q.all(promiseArray)
  .then(function(resolved) {
    console.log("ALL INITIAL PROMISES RESOLVED");
    console.log('resolved',resolved);
    var temp = [];//Array.isArray()
    var resolvedData = [];
    if (Array.isArray(resolved)) {
      resolved.forEach(function(prom,i){
        if (Array.isArray(resolved[i])){
          if (resolved[i].length>0) {
            resolvedData = resolvedData.concat(resolved[i][0]);
          }
        }
      });
    }
    console.log('resolvedData',resolvedData);
    finalPromise.resolve(resolvedData);
    })
  .catch(function(resolved){
    $log.debug('One or more promises failed.');
    console.log('resolved',resolved);
    var temp = [];
    if (typeof resolved === "object") {

    } else {
      temp = resolved;
    }
    var resolvedData = [];
    temp.forEach(function(prom,i){
      if ((temp[i].length!==0)){
        resolvedData = resolvedData.concat(temp[i][0]);
      }
    });
    console.log('resolvedData',resolvedData);
    finalPromise.resolve(resolvedData);
    // finalPromise.resolve(resolved);
    // deferred.reject();
  });
  return finalPromise.promise;
};

var searchAction = function(graph,data,classType,collection){

};

/**
 * [js_traverse description]
 * @param  {[type]} o        [description]
 * @param  {[type]} property [description]
 * @return {[type]}          [description]
 */
function js_traverse(o,property) {
  var type = typeof o;
  var out = {};
  if (type === "object") {
      for (var key in o) {
          if (key===property) {
            return o;
          } else {
            out = js_traverse(o[key],property);
            if (typeof out === "object") {
            if (out.hasOwnProperty(property)) {return out;}
          }
          }
        }
  } else {
    return false;
  }
}

function nestedProperty(o,chain) {
  var child = chain[0];
  var temp;
  // console.log('child',child);
  // console.log(o);
  if ((o.hasOwnProperty(child))) {
    if ((typeof o[child].value === "object")) {
      if (chain.length>1) {
        var next = chain.slice(1);
        temp = nestedProperty(o[child].value,next);
        if(temp){
          return temp;
        } else {
          return false;
        }
      } else {
        return o[child];
      }
    } else {
      return o[child];
    }
  } else if(o.hasOwnProperty('value')){
    if ((typeof o.value === "object")) {
      if (chain.length>=1) {
        // var next = chain.slice(1);
        temp = nestedProperty(o.value,chain);
        if(temp){
          return temp;
        } else {
          return false;
        }
      } else {
        return o[child];
      }
    } else {
      return o[child];
    }
  } else {
    return false;
  }
}

/**
 * [searchDataBinds description]
 * @param  {[type]} mapObj [description]
 * @param  {[type]} data   [description]
 * @return {[type]}        [description]
 */
function searchDataBinds(mapObj,data) {
  console.log('dataBinds',data);
  console.log('mapObj',mapObj);
  mapObj.forEach(function(map,i){
    var chainProps = mapObj[i]['variable'].split(".");
    console.log('chainProps',chainProps);
    var propNode = SCHEMA(chainProps[chainProps.length-1]);
    var property = (chainProps[chainProps.length-1]);
    console.log(propNode);
    console.log('datatat',data);
    // console.log('DepthOfObject',depthOf(data));
    // console.log('classTier',classTier(data,0));
    // mapObj[i]['value'] = data['departureAirport']['value'];//assign value for search
    var values = nestedProperty(data,chainProps);
    // console.log('temp',temp);
    // var values = js_traverse(data,property);
    console.log('VALUESSS',values);
    if(values) {
      console.log('property',property);
      if (values.value instanceof Date) {
        mapObj[i]['value'] = values.value.toDateString();
      } else {
        mapObj[i]['value'] = values.value;//assign value for search
      }
    } else {
      mapObj[i]['value'] = '';
      console.log('propertyNOTSearch',property);
          //Should to some resaerch let for later*
    }
  });
  return mapObj;
}
/**
 * [generateSearchURL description]
 * @param  {[type]} urlTempl [description]
 * @param  {[type]} mapObj   [description]
 * @param  {[type]} data     [description]
 * @return {[type]}          [description]
 */
  function generateSearchURL(urlTempl,mapObj,data) {

    var readyMap = searchDataBinds(mapObj,data);
    console.log('readyMap',readyMap);
    var parameters = {};
    readyMap.forEach(function(parameter,i){
      if(readyMap[i].value) {
        var varName = readyMap[i]['variable'];
        parameters[varName] = readyMap[i].value;
      } else {
        console.log('Not searchable property: ', readyMap[i]['variable']);
        // We have a value that maybe is custom,
        // it has it's own name:value, and does not
        // meet the selected class
      }
    });
    console.log('urlTempl',urlTempl);
    console.log('parameters',parameters);
    var out = urlTempl.expand(parameters) ? urlTempl.expand(parameters) : null;
    console.log(out);
    return out;//srting
  }

  var parseInput = function(input,graph) {
      var deferred = $q.defer();
      var data = {};
      deferred.notify('About to parse data.');
      hydraParsedPromise(JSON.stringify(input),graph,'').then(function(graph) {
        console.log('graph with input',graph);
        Object.keys(input).forEach(function(cl,i){
          if(!(isContext(cl))) {
            data[cl] = input[cl];
          }
        });
        deferred.resolve(data);
        return data;
      });
      return deferred.promise;
  };

/**
 * [resolveAPI description]
 * @param  {[type]} graph [description]
 * @return {[type]}       [description]
 */
  var resolveAPI = function(graph) {
    var arrao = createProtos(graph,true);
    console.log('createProtos!',arrao);
    var testClass = new arrao[2]['Flight'];
    console.log('testClass!',testClass);
    var testCollection = new arrao[3]['TouristAttraction'](testClass);
    testCollection.set('@id','http://vps362714.ovh.net:8091/software_applications');
    console.log('testCollection!',testCollection);
    testClass['@id'] = 'http://vps362714.ovh.net:8091/software_applications';
    // testCollection['@id'] = 'http://vps362714.ovh.net:8091/software_applications';
    var testColOp = testCollection.methods.get();
    var testColAction = testCollection.actions.schemasearch();
    console.log('testColAction',testColAction);
    testColOp.then(function(resp){
      console.log('Final response',resp);
    });
      // var lool = testClass.methods.get();
      // // var lool2 = testClass.methods.delete();
      // console.log('lool',lool);
      // lool.then(function(resp){
      //   console.log(resp);
      // });
      // console.log('TEST OBJ',test);
      // console.log('RESOLVED!!!:',graph.statements.length);
    // var objects = createObjects(data,graph,'');
    var deferred = $q.defer();
    deferred.resolve(true);
    return deferred.promise;
  };

var fetchHydra = function(data,graph,req) {
  return new Promise(function(resolve,reject) {
    $http(req)
    .then(function(response){ resolve({hydra:response.data,graph:graph}); },
    function(error) { reject(error); } );
  });
  };

$log.debug( "Error logging failed" );
schydraAngular.log = log;
schydraAngular.error = error;
schydraAngular['init'] = function(rawJSON,resolverServer,graph){
  var deferred = $q.defer();
  var req = {
    method: 'PUT',
    url: resolverServer,
    headers: {
    'Content-Type': 'application/ld+json'
    },
    data: rawJSON
  };
    parseInput(rawJSON,graph)
    .then(function(){ fetchHydra(rawJSON,graph,req)
    .then(function(obj) { hydraDocument = obj.hydra; hydraParsedPromise(JSON.stringify(obj.hydra),obj.graph,'')
    .then(function(graph){ resolveAPI(graph)
    .then(function(resolved){  console.log('resolved',resolved);
      deferred.resolve(createObjects(rawJSON,graph,'other'));  },
      function(error) { console.log(error); } );
    },
      function(error) { console.log(error); } );
    },
      function(error) { console.log(error); } );
    },
      function(error) { console.log(error); } );

  console.log('Init Called');
  return deferred.promise;
};

// Retrieve Literals from hydra response
schydraAngular['getLiterals'] = function(graph) {
  return parseHydra(graph);
}
// Retrieve JSON-LD format
schydraAngular['getJsonld'] = function() {
  return hydraDocument;
}

  return schydraAngular;
}]);//END of anjular Controller console.log('Raw Json Answer:',JSON.stringify(obj.hydra));
