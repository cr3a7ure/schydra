// GEneral helper functions
//####################################


//RDF specific functions
//#############################

function visualiseGraph(hydraGraph,g) {

    var subj,obj,subjC,objC;
    var n=0;

    function addNode(rdfNode) {
        var node = {};
        if (rdfNode.__proto__.classOrder==6) {
            node ={
                id: rdfNode.value,
                label: rdfNode.value,
                x: Math.random()*10+5,
                y: Math.random()*1+1,
                size: Math.random(),
                color: '#00008B'
            };
        } else if(rdfNode.__proto__.classOrder==1){
            node ={
                id: rdfNode.value,
                label: rdfNode.value,
                x: Math.random()*2+10,
                y: Math.random()*2+5,
                size: Math.random(),
                color: '#319b39'
            };
        } else {
            var temp3 = rdfNode.uri
            temp3 = rdfNode.uri.split('/')
            node ={
                id: rdfNode.uri,
                label: temp3.pop(),
                x: Math.random()*3+5,
                y: Math.random()*3+5,
                size: Math.random(),
                color: '#ff0000'
            };
        }
        return node;
    }
    // Visualise Hydra Graph
    hydraGraph.statements.forEach(function(st){
        // console.log('statement')
        var subj,obj,subjC,objC;
        var bnodeRegex = new RegExp('b\d_b\d{0,6}')
        g.nodes.push(addNode(st.subject));
        g.nodes.push(addNode(st.object));

        var ccc = '#bfbfbf'
        var edgeLabel = (st.predicate.uri || st.predicate.value)
        var templabel = edgeLabel.split('#')
        edgeLabel = templabel[1];
        // if (bnodeRegex.test()) {
        //     ccc = '#000'
        // }
        g.edges.push({
            id: n,
            label: edgeLabel,
            source: (st.subject.value || st.subject.uri),
            target: (st.object.value || st.object.uri),
            size: 5,
            type: "arrow",
            color: ccc
        });
        n++;
    })//end of statement iterations

    var nodeNames  = [];
    g.nodes.forEach(function(node,index){
        nodeNames[index] = node.id;
    })

    // console.log(nodeNames)
    var uniq = nodeNames
    .map((name) => {
      return {count: 1, name: name}
    })
    .reduce((a, b) => {
      a[b.name] = (a[b.name] || 0) + b.count
      return a
    }, {})

    console.log('Number of unique URIs: '+Object.keys(uniq).length)

    g.nodes.forEach(function(node,index){
        Object.keys(uniq).forEach(function(uname,uindex){
            if ((node.id == uname)&&(uniq[uname]>1)) {
                console.log('sigma graph node match')
                uniq[uname] = uniq[uname] - 1;
                g.nodes[index] = undefined;
            }
        })
    })
    var temp = g.nodes.filter(Boolean);
    g.nodes = temp;
    delete temp;
    // console.log(g.nodes)
    sigmaGraph = new sigma({
        graph: g,
        renderer: {
            container: 'graph-container',
            type: 'canvas'
        },
        settings: {
            edgeLabelSize: 'proportional',
            scalingMode: 'outside',
            // drawEdgeLabels: true,
            edgeLabelThreshold: 0,
            edgeLabelSize: 8,
            defaultEdgeLabelSize: 8,
            labelThreshold: 3,
            labelSizeRatio: 5,
            defaultNodeBorderColor: '#F9F694',
            edgeLabelSizePowRatio: 10,
            font: "arial",
            minNodeSize: 5,
            minEdgeSize: 1,
            edgeHoverSizeRatio: 15,
            minArrowSize: 5
            // fontStyle: "bold"
        }
    });
    sigmaGraph.refresh();
}


function mergeBlankNodes(hydraGraph){

    var bbnodesNames = [];
    var bbnodes = [];
    hydraGraph.statements.forEach(function(st){
        var s = st.subject,
        p = st.predicate,
        o = st.object,
        w = st.why;
        // console.log(st.object.__proto__.classOrder)
        if (st.object.__proto__.classOrder==6) {
            var nameIndex = bbnodesNames.indexOf(st.object.value);
            // console.log('objectIndex: '+nameIndex)
            if (nameIndex==(-1)) {
                // first meet!
                bbnodes.push(st.object)
                bbnodesNames.push(st.object.value)
            } else {
                o = bbnodes[nameIndex];
            }
        }
        if (st.subject.__proto__.classOrder==6) {
            var nameIndex = bbnodesNames.indexOf(st.subject.value);
            // console.log('subjectIndex: '+nameIndex)
            if (nameIndex==(-1)) {
                // first meet!
                bbnodes.push(st.subject)
                bbnodesNames.push(st.subject.value)
            } else {
                s = bbnodes[nameIndex];
            }
        }
        if (st.predicate.__proto__.classOrder==6) {
            var nameIndex = bbnodesNames.indexOf(st.predicate.value);
            if (nameIndex==(-1)) {
                // first meet!
                bbnodes.push(st.predicate)
                bbnodesNames.push(st.predicate.value)
            } else {
                p = bbnodes[nameIndex];
            }
        }
        hydraGraph.removeStatement(st)
        hydraGraph.add(s,p,o,w)
    }); // forEAch statement END
        // console.log(bbnodesNames)

    return hydraGraph
}

var testPromise = function(sparQ,KBout) {
    return new Promise(function(resolve, reject) {
      var queryFromSPQ = $rdf.SPARQLToQuery(sparQ,true,KBout)
        setTimeout(function(){
          if (queryFromSPQ.pat.statements.length>0) {
            console.log("mhkos")
                resolve(queryFromSPQ)
            } else {
                reject(Error('No Statement parsed! Time out!'))
            }
        },2000);
    });
}
/*
    Retrieve the properties a schema Class may contain
    input: class name only
    output: array of NameNodes
 */
function retrieveProperties(className,schema) {
    var props = schema.each(null,SCHEMA('domainIncludes'),SCHEMA(className));
    console.log('subClassOf PostalAddress',props);
    return props;
}
/*
    Retrieve the properties a schema Class may contain
    input: class name only
    output: array of NameNodes
 */
function retrieveHydraProperties(className,schema) {
    var props = schema.each(SCHEMA(className),HYDRA('supportedProperty'),null);
    console.log('subClassOf PostalAddress',props);
    return props;
}

/*
    Retrieves the properties that are of type ClassName
 */
function retrieveInstances(className,schema) {
    var props = schema.each(null,SCHEMA('rangeIncludes'),SCHEMA(className));
    console.log(props);
    return props;
}

/*
    Retrieves Classes containing the propName
 */
function retrieveContainerClass(propName,schema) {
    var classes = schema.each(SCHEMA(propName),SCHEMA('domainIncludes'),null);
    console.log(classes);
    return classes;
}

/*
    Retrieves property's used types
 */
function retrieveProrertyTypes(propName,schema) {
    var classes = schema.each(SCHEMA(propName),SCHEMA('rangeIncludes'),null);
    console.log(classes);
    return classes;
}

function rdfExp(KBout) {

var onResult = function(result) {
        //console.log("TEST RESULT:");
        var str = "\tTEST Result: ";
        console.log(result)
        for (var v in result) {
            str += "   "+v+'->'+result[v];
        }
        console.log(str);
    };

    var onDone = function() {
        console.log("\tTEST DONE -- final callback")
    }
    // $rdf.log.info = function(str) {console.log("info:  "+str)};
    // $rdf.log.debug = function(str) {console.log("debug: "+str)};

// - load remote RDF document
// var kb3 = $rdf.graph();
// var fetch = $rdf.fetcher(kb3, 1000);
// fetch.nowOrWhenFetched(schemaOrgServer, undefined, function(ok) {
//     var sparqlQuery = 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> \
//                        SELECT ?person ?name \
//                        WHERE { \
//                          <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person . \
//                          OPTIONAL { ?person foaf:name ?name . } \
//                        }';
//     var query = $rdf.SPARQLToQuery(sparqlQuery, false, kb3);

//     console.log($rdf.queryToSPARQL(query)); // prints an empty OPTIONAL clause

//     kb3.query(query, function(result) {
//         // doesn't delivery any result
//         console.log("result",result)
//     }, fetch);
// });

var sparQ = 'SELECT \ ' +
           'WHERE { \
              ?who <http://schema.org/domainIncludes> <http://schema.org/arrivalAirport> . } ';
  var q3 = new $rdf.Query('test2',2);
  var testo = $rdf.graph();
  console.log("RDFFF",q2)
  var q2 = new $rdf.Query('test',3);
q2.pat.add($rdf.variable('who'),SCHEMA('domainIncludes'),SCHEMA('Airport'))
var test;
var queryFromSPQ = $rdf.SPARQLToQuery(sparQ,true,KBout)
q3 = queryFromSPQ;

console.log("SPARQL from query:\n",$rdf.queryToSPARQL(q2));

var fetch2 = $rdf.fetcher(KBout, 1);
testPromise(sparQ,KBout).then(function(result){
      KBout.query(result, onResult, null, onDone);
      // console.log()
});
console.log("SPARQL to $RDF query",queryFromSPQ);
    KBout.query(queryFromSPQ, onResult, undefined, onDone);

   var kb = $rdf.graph();
    var x = kb.sym('#foo');
    var foaf = $rdf.Namespace('http://xmlns.com/foaf/0.1/');
    kb.add(x, foaf('type'), foaf('Person'));

    // Make a query:

    // var q = new $rdf.Query('test', 3);

    // var who = $rdf.variable('who');
    // var email = $rdf.variable('email');
    // var name = $rdf.variable('name');

    // q.pat.add(who,  foaf('type'), foaf('Person'));

    // var opt1 = $rdf.graph();
    // opt1.add(who,  foaf('mbox'), email);
    // q.pat.optional.push(opt1);
    // var opt2 = $rdf.graph();
    // opt2.add(who,  foaf('name'), name);
    // q.pat.optional.push(opt2);

    // console.log('\nTest1:');
    // kb.query(q, onResult, undefined, onDone);

    // kb.add(x, foaf('name'), "Fred");

    // console.log('\nTest 2:');
    // kb.query(q, onResult, undefined, onDone);

    // kb.add(x, foaf('mbox'), kb.sym('mailto:fred@example.com'));

    // console.log('\nTest 3: kb='+kb);
    // kb.query(q, onResult, undefined, onDone);
}