

var data = {
   "var1": {
    "@id": '/var1',
    "@type": "GeoCoordinates",
    'addressLocality': "Chios",
    'addressRegion' : "",
    "id": 0,
    "addressCountry": ""
  },
  // "postalAddress2" : {
  //   "@type": "PostalAddress",
  //   "id": 0,
  //   "addressCountry": ""
  // },
  // "test2" : {
  //   "@type": "Event",
  //   "name": "Example Band goes to San Francisco",
  //   "startDate" : "2013-09-14T21:30",
  //   "url" : "http://example.com/tourdates.html",
  //   "location" : {
  //     "@type" : "Place",
  //     "sameAs" : "http://www.hi-dive.com",
  //     "name" : "The Hi-Dive",
  //     "address" : "7 S. Broadway, Denver, CO 80209"
  //   }
  // },
  // "manas": {
  //   "@type": "Airport",
  //   "GeoCoordinates": {
  //       "longitude": '13.254',
  //       "latitude": '70.123'
  //   }
  // },
  "test1": {
    "@type": "test",
    "longitude": '13.254',
    "latitude": '70.123'
  }
};
// TEST INPUT DATA
var testData = {
  dataClass1: {
    departureTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    dataClass22: {
      type: "testClass",
      value: "2013-03-01T16:15:09+01:00"
    },
    arrivalTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    PostalAddress: {
      level2Property: {
        type: "Airport",
        value: "Thessaloniki"
      },
      Airport: {
        level3Property: {
          type: "Airport",
          value: "Thessaloniki"
        },
        address: {
          type: "#TestPropClass4",
          value: "Thessaloniki"
        }
      }
    },
    0:{
        type: "#TestPropClass4",
        value: "Thessaloniki"
    }
  },
    departureAirport: {
        type: "Airport",
        value: "Thessaloniki"
    },
    arrivalAirport: {
        type: "Airport",
        value: "Chios"
    },
    PostalAddress: {
        type: "addressLocality",
        value: ''
    },
    addressLocality: {
        type: "string",
        value: "Chios"
    },
    adultNumber: {
        type: "xmls:integer",
        value: 1
    }
    // offer:{
    //     type: "#Offer",
    //     value: null
    // }
};

// TEST INPUT DATA
var testdataType   = {
    departureTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    arrivalTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    departureAirport: {
        type: "#Airport",
        value: { 
          GeoCoordinates: {
            longitude: '13.254',
            latitude: '70.123'
        }
      }
    },
    arrivalAirport: {
        type: "#Airport",
        value: "Chios"
    },
    // PostalAddress: {
    //     type: "addressLocality",
    //     value: ''
    // },
    addressLocality: {
        type: "string",
        value: "Chios"
    },
    adultNumber: {
        type: "xmls:integer",
        value: 1
    }
    // offer:{
    //     type: "#Offer",
    //     value: null
    // }
};



var RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
var RDFS = $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
var FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
var XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
var SCHEMA = $rdf.Namespace("http://schema.org/");
var HYDRA = $rdf.Namespace("http://www.w3.org/ns/hydra/core#");

var rdfs = {
    subClassOf: $rdf.sym('rdfs:subClassOf'),
    subPropertyOf: $rdf.sym('rdfs:subPropertyOf'),
    label: $rdf.sym('rdfs:label'),
    comment: $rdf.sym('rdfs:comment'),
    test2: $rdf.sym('rdfs:subPropertyOf'),
    test34: $rdf.sym('rdfs:subPropertyOf')
};

// Utility functions
// ###################

/**
 * Capitalize first char, for schema.org compatibility
 * @return {[type]} [description]
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

/**
 * [This is a test function to get the UIRs from schema classes
 * We should use https://github.com/tc39/proposal-intl-plural-rules
 * but for now is ok]
 * @return {[type]} [description]
 */
String.prototype.toUri = function() {
    var str = this
    var checkEnd = new RegExp(/[s]$/g)
    str = str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1_').toLowerCase();
    if (checkEnd.test(str)) {
        str = str + 'es';
        return str;
    } else {
        str = str + 's';
        return str;
    }
}

String.prototype.toClassName = function() {
  var str = this
  // var checkEnd = new RegExp(/[s]$/g)
  str = str.split('/').pop();
  return str.replace('>','');
}

/*
 Split empty properties
 Check the value field
 */
function isEmpty(property) {
  if (property==undefined||property==""||property==null) {
      return true
  } else {
      return false
  }
}

var datatypeList = ["xmls:dateTime","xmls:integer",'date','datetime','boolean','number','integer','float','text','string','time'];

function isDatatype(property,datatypeList) {
    if (datatypeList.indexOf(property.type)>-1){
        return true
    } else {
        return false
    }
}

function isSchema(property,schema) {
    var schemaUri = SCHEMA(property)
    var tempNum = schema.whether(schemaUri,null,null);
    tempNum = tempNum + schema.whether(null,null,schemaUri);
    if (tempNum==0){
        return false
    } else {
        return true
    }
}

/*
  Return depth of object hierarchy
 */
function depthOf(object) {
  var level = 1;
  var key;
  for(key in object) {
    if (!object.hasOwnProperty(key)) continue;
    if(typeof object[key] == 'object'){
      var depth = depthOf(object[key]) + 1;
      level = Math.max(depth, level);
    }
  }
  return level;
}

/**
 * Parse the DOM and retrieve the schema properties
 * @return {[type]} [description]
 */
function dataParser() {}


/*
  Check data integrity
  Check the containing data against the described datatype
 */
function isDataOK(property,datatypeList) {
  var datatypeProp = property["type"];
  // if (property.value==1) {}
  return true;
}