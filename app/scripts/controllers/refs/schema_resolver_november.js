

var RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
var RDFS = $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
var FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
var XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
var SCHEMA = $rdf.Namespace("http://schema.org/");
var HYDRA = $rdf.Namespace("http://www.w3.org/ns/hydra/core#");

var rdfs = {
    subClassOf: $rdf.sym('rdfs:subClassOf'),
    subPropertyOf: $rdf.sym('rdfs:subPropertyOf'),
    label: $rdf.sym('rdfs:label'),
    comment: $rdf.sym('rdfs:comment'),
    test2: $rdf.sym('rdfs:subPropertyOf'),
    test34: $rdf.sym('rdfs:subPropertyOf')
};

/**
 * Capitalize first char, for schema.org compatibility
 * @return {[type]} [description]
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

/**
 * [This is a test function to get the UIRs from schema classes
 * We should use https://github.com/tc39/proposal-intl-plural-rules
 * but for now is ok]
 * @return {[type]} [description]
 */
String.prototype.toUri = function() {
    var str = this
    var checkEnd = new RegExp(/[s]$/g)
    str = str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1_').toLowerCase();
    if (checkEnd.test(str)) {
        str = str + 'es';
        return str;
    } else {
        str = str + 's';
        return str;
    }
}


//Najularrrr
blogApp.controller('ReasonerCtrl', ['$scope','Restangular','$http', function ($scope, Restangular, $http) {


/**
 * Parse schema.org RDFa as RDF graph and return promise
 * @param  {[type]} schemaDoc       [description]
 * @param  {[type]} schemaGraph     [description]
 * @param  {[type]} schemaOrgServer [description]
 * @return {[type]}                 [description]
 */
var schemaParsedPromise = function(schemaDoc,schemaGraph,schemaOrgServer) {
    return new Promise(function(resolve, reject) {
        $rdf.parse(schemaDoc, schemaGraph, schemaOrgServer,'text/html')
        setTimeout(function(){
            if (schemaGraph.statements.length>0) {
                resolve(schemaGraph)
            } else {
                reject(Error('No Statement parsed! Time out!'))
            }
        },1000);
    });
}


/**
 * Fetch Hydra servers' entrypoints and create an entity for them
 * @param  {[type]} entrypointList [description]
 * @return {[type]}                [description]
 */
var fetchHydraApis = function(entrypointList) {
    return new Promise(function(resolve, reject) {
        var entryClasses = {}
        var isContext = new RegExp('^@');
          entrypointList.forEach(function(entrypoint){
            $http.get(entrypoint).then(function(response) {
                entryClasses[entrypoint] = {}
                var hydraApiClasses = (response.data);
                Object.keys(hydraApiClasses).forEach(function(api){
                      if (!isContext.test(api)) {
                        entryClasses[entrypoint][api.capitalize()] = {}
                        entryClasses[entrypoint][api.capitalize()]['link'] = hydraApiClasses[api]
                        entryClasses[entrypoint][api.capitalize()]['rank'] = 0;
                      }
                      // console.log(entryClasses)
                })
            })
        })
        setTimeout(function(){
            if (Object.keys(entryClasses).length==entrypointList.length) {
                resolve(entryClasses)
                console.log("All good")
            }
             else {
                resolve(entryClasses)
                console.log('Fetching from servers timed out, some may not be included')
            }
        },2000);
    });
}

/**
 * [Parse Hydrajsonld as RDF graph and return a promise]
 * @param  {[type]} hydraDoc    [description]
 * @param  {[type]} hydraGraph  [description]
 * @param  {[type]} hydraServer [description]
 * @return {[type]}             [description]
 */
var hydraParsedPromise = function(hydraDoc,hydraGraph,hydraServer) {
    return new Promise(function(resolve, reject) {
        $rdf.parse(hydraDoc, hydraGraph, hydraServer,'application/ld+json')
        setTimeout(function(){
            if (hydraGraph.statements.length>0) {
                resolve(hydraGraph)
            } else {
                reject(Error('No Statement parsed! Time out!'))
            }
        },5000);
    });
}

/**
 * Parse the DOM and retrieve the schema properties
 * @return {[type]} [description]
 */
function dataParser() {}

function returnContainedClasses(data,schema) {

    console.log("data input ",data)
    var returnClasses = {}
    var temp = []
    //Return doaminIncludes for properties
    Object.keys(data).forEach(function(dataClass){
        if (dataClass=="") {
            /*
            If we have empty class, add the domainIncludes classes
            should we check for the number of matches? per class?
             */
            temp.push(dataClass) // save CLASS
        } else {
          // console.log(dataClass)
            temp.push(dataClass) // save CLASS
        }
        Object.keys(data[dataClass]).forEach(function(prop){
            // data[dataClass][prop] rangeIncludes
            // add rangeIncludes structured data to classes
            // temp.push(dataClass.uri) // save CLASS
        })
        //for every class go check props and find structured data CLASSES
        // gia ka8e CLASS pou einai adeia-den exei class, trava thn domain class
        //
    })
    console.log("returnContainedClass",temp)
    return temp
    // return returnClasses
}

/*
We assign a rank to a server based on what we have/want
 */

function rankServer(arrayOfClasses,server) {
  var temp = 0;
  Object.keys(server).forEach(function(cl){
    if ((arrayOfClasses.indexOf(cl)>-1)) {
      temp++
    }
  })
  // An den periexei kamia klash, tote mhn t baleis mesa
  if (temp==0) {
    return null
  } else {
    server['rank'] = temp
    return server
  }
}

/*
This is called on the servers Entrypoints
We could assign this procedure while fetching,
bu this allows as to have prefetched our servers,
or cached!
 */
/**
 * [prepareServers description]
 * @param  {[type]} servers [description]
 * @param  {[type]} data    [description]
 * @param  {[type]} schema  [description]
 * @return {[type]}         [description]
 */
function prepareServers(servers,arrayOfClasses,schema) {

    Object.keys(servers).forEach(function(server){
      var temp = rankServer(arrayOfClasses,servers[server])
      if (temp!=null) {
        servers[server] = temp
      }
    })
  return servers
}


/*
 Sort servers, based on their rank
 */
function sortRankedServers(rankServerArray) {

}

var prepareServersPromise = function(serversEntrypointsFetched) {
    return new Promise(function(resolve, reject) {
      serversEntrypointsFetched.then(function(serversArray){
        Object.keys(serversArray).forEach(function(server){
            // console.log(server)
            var numMatchedClasses = 0;
            Object.keys(serversArray[server]).forEach(function(serviceClass){
                var temp = serversArray[server][serviceClass]
                // console.log('temp',temp)
                serversArray[server][serviceClass] = {}
                serversArray[server][serviceClass]['link'] = temp
                // serversArray[server][serviceClass]['matched'] = false
                Object.keys(domainClasses).forEach(function(classDomain){
                    // console.log(domainClasses[classDomain].uri.split('/').pop())
                    if (serviceClass==domainClasses[classDomain].uri.split('/').pop()) {
                        serversArray[server][serviceClass]['matched'] = false
                        numMatchedClasses++
                    }
                })
                serversArray[server]['rank'] = numMatchedClasses
            })
        })
    })
        setTimeout(function(){
            if (hydraGraph.statements.length>0) {
                resolve(hydraGraph)
            } else {
                reject(Error('No Statement parsed! Time out!'))
            }
        },5000);
    });
}


/*
Create new array per Class rather than per server
for each class, we select its server and rank
 */
function arrangePerClass(serversArray,arrayOfClasses) {
  console.log("serversArray",serversArray)
  console.log(arrayOfClasses)
  var ObjPerClass = {};
  arrayOfClasses.forEach(function(cl){
    ObjPerClass[cl] = {}
    Object.keys(serversArray).forEach(function(svr){
      ObjPerClass[cl]['servers'] = {}
      Object.keys(serversArray[svr]).forEach(function(svrCl){
        // console.log("svrCl",svrCl + " " + timeTest)
        if(cl==svrCl) {
          ObjPerClass[cl]['servers'][svr] = {}
          ObjPerClass[cl]['servers'][svr]['rank'] = serversArray[svr]["rank"];
        }
      })
    })
  })
  console.log("ObjPerClass",ObjPerClass)
  return ObjPerClass
}
/*
Get the ranked servers and start fetching data/resolving
 */

/*
Check input, to servers' classes
Create the likelyhood table,
 */
     //http://localhost:8000/contexts/Flight
     //http://localhost:8000/postal_addresses
function rankClasses(serversArray,arrayOfClassesIn,dataEdited) {


  var arrayOfClasses = arrangePerClass(serversArray,arrayOfClassesIn)
  console.log("arrayOfClassesApoRankClasses",arrayOfClasses)
  Object.keys(arrayOfClasses).forEach(function(cl){
    arrayOfClasses[cl]['filled'] = 0
    // arrayOfClasses.cl['matched'] = 0
    arrayOfClasses[cl]['empty'] = 0
    // arrayOfClasses.cl['searchable'] = 0

    var inputProps = dataEdited[cl]["properties"];
    console.log("inputProps",inputProps)
    arrayOfClasses.cl.properties.forEach(function(property){
      inputProps[property] = {}
              // Check for empty property
      if (!inputProps[property]) {
        inputProps[property]['filled'] = true;
        arrayOfClasses.cl['filled']++
      } else {
        inputProps[property]['filled'] = false;
        arrayOfClasses.cl['empty']++
      }
    })//END of properties iterator
    arrayOfClasses.cl.servers.forEach(function(server){
      inputProps['done'] = 0;
      inputProps['searchable'] = 0;
      inputProps['matched'] = 0;

      // for every Class and every Server
      // We need to fetch search vars and properties
      var classApiLink = server + arrayOfClasses.cl.servers[server].link;
      var classContextLink = server + '/contexts/' + cl
      $http.get(classApiLink).then(function(response) {
        var api = JSON.parse(response.data);
        api["hydra:search"]["hydra:mapping"].forEach(function(variable){
          var propName = api["hydra:search"]["hydra:mapping"][variable]['property'];
          //check if property is searchable
          if (inputProps.hasOwnProperty(propName)) {
            inputProps[propName]['searchable'] = true
            inputProps['searchable']++
          } else {
            inputProps[propName]['searchable'] = false
          }
        })
        inputProps['done']++;
      }) //END of search get
      $http.get(classContextLink).then(function(response) {
        var context = JSON.parse(response.data);
        //get below @base
        context['@context'].forEach(function(propName){
          if (inputProps.hasOwnProperty(propName)) {
            inputProps[propName]['matched'] = true
            inputProps['matched']++
          } else {
            inputProps[propName]['matched'] = false
          }

          var propName = context['@context'][prop]
          if (typeof propName === 'object') {
          }

        })
        inputProps['done']++;
      })// END of context get
      // Exoun pragmatopoih8ei kai ta 2 get!!!
      if (inputProps['done']==2) {
        var svrRanks = arrayOfClasses.cl.servers[server];
        var propSum = (arrayOfClasses.cl['empty']+arrayOfClasses.cl['filled'])
        svrRanks = {}
        svrRanks['matchedToAll'] = inputProps['matched']/propSum;
        svrRanks['searchableToFilled'] = inputProps['searchable']/arrayOfClasses.cl['filled']
        svrRanks['emptyAndMatched'] = 0;
        inputProps.forEach(function(prop){
          if((inputProps.prop['matched'])&& (!inputProps.prop['filled'])) {
            svrRanks['emptyAndMatched']++
          }
        })
        svrRanks['emptyMatchedToEmpty'] = svrRanks['emptyAndMatched']/arrayOfClasses.cl['empty'];
      }

    })//END of servers iterator
  })
  return 0;
}
/*
Match classes based on the ranked class table
 */
function matchClasses(arrrangedPerClassSearch,arrayOfClasses) {
  var svrRanks = arrayOfClasses.cl.servers[server];

}

function fixData(data) {
  var dataOut = {}
  Object.keys(data).forEach(function(cl) {
    dataOut[cl] = {}
    dataOut[cl]['properties'] = {}
    dataOut[cl]["properties"] = data[cl]
  })
  return dataOut
}

/*
Return the results from matching
 */
function returnData() {

}

function resolveApis(servers,data,schema) {
  Object.keys(servers).forEach(function(svrName){
    console.log(svrName)
  })
  var arrayOfClasses = returnContainedClasses(data,schema)
  var dataEdited = fixData(data);
  var serversArray = prepareServers(servers,arrayOfClasses,schema) //exoume etoimo ranked tous servers
  var arrrangedPerClassSearch = rankClasses(serversArray,arrayOfClasses,dataEdited)
  var matchedClasses = matchClasses(arrrangedPerClassSearch,arrayOfClasses)
  var out = returnData(matchedClasses)
     //Gia ka8e entrypoint pou tairiazei,

}

/*
Constants and arguments
 */
var entrypointList = ['http://localhost:8000','http://localhost:8080'];
var hydraObject;
var hydraGraph = $rdf.graph();
var hydraDoc = {};
var hydraServerUri = 'http://localhost:8000/';
var hydraServer = 'http://localhost:8000/apidoc';
var hydraPromise = jsonld.documentLoader(hydraServer);
var schemaKB = $rdf.graph();
// var schemaOrgServer = 'https://raw.githubusercontent.com/schemaorg/schemaorg/sdo-gozer/data/schema.rdfa';
var schemaOrgServer = 'http://localhost:9000/schema_3_2.rdfa';
var serversList = fetchHydraApis(entrypointList);
console.log('Hydra servers Entypoins',serversList);

// TEST INPUT DATA
var testdataType = {
    departureTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    arrivalTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    departureAirport: {
        type: "#Airport",
        value: "Thessaloniki"
    },
    arrivalAirport: {
        type: "#Airport",
        value: "Chios"
    },
    PostalAddress: {
        type: "addressLocality",
        value: ''
    },
    addressLocality: {
        type: "string",
        value: "Chios"
    },
    adultNumber: {
        type: "xmls:integer",
        value: 1
    }
    // offer:{
    //     type: "#Offer",
    //     value: null
    // }
};


    // $scope variables
    $scope.freeTextInput = "free text input test";
    $scope.$watch("freeTextInput",function (input){    });
    $scope.modelReasoner = "test";

    var timeStartFetch = performance.now();
    $http.get(schemaOrgServer).then(function(response) {
        // var schemaDoc = response.data
        schemaParsedPromise(response.data,schemaKB,schemaOrgServer).then(function(schemaGraph){
            var timeReadySchema = performance.now();
            console.log('Time took to fetch & parse schemaorg: ' + (timeReadySchema-timeStartFetch) + ' ms')
            console.log('KB from SCHEMA',schemaKB);
            serversList.then(function(entrypointArray){
              // console.log("entrypointArray",entrypointArray["http://localhost:8000"])
              resolveApis(entrypointArray,testdataType,schemaKB);
            })
       }); // END of schemaOrg Parse!
    }, //END of schemaOrg Fetching
    function errorCallback(response) {
        alert('Failed to fetch schema.org');
    });// SCHEMA fetch to rdf graph

}]); // Controller END