

var RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
var RDFS = $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
var FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
var XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
var SCHEMA = $rdf.Namespace("http://schema.org/");
var HYDRA = $rdf.Namespace("http://www.w3.org/ns/hydra/core#");

var rdfs = {
    subClassOf: $rdf.sym('rdfs:subClassOf'),
    subPropertyOf: $rdf.sym('rdfs:subPropertyOf'),
    label: $rdf.sym('rdfs:label'),
    comment: $rdf.sym('rdfs:comment'),
    test2: $rdf.sym('rdfs:subPropertyOf'),
    test34: $rdf.sym('rdfs:subPropertyOf')
};

/**
 * Capitalize first char, for schema.org compatibility
 * @return {[type]} [description]
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

/**
 * [This is a test function to get the UIRs from schema classes
 * We should use https://github.com/tc39/proposal-intl-plural-rules
 * but for now is ok]
 * @return {[type]} [description]
 */
String.prototype.toUri = function() {
    var str = this
    var checkEnd = new RegExp(/[s]$/g)
    str = str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1_').toLowerCase();
    if (checkEnd.test(str)) {
        str = str + 'es';
        return str;
    } else {
        str = str + 's';
        return str;
    }
}

String.prototype.toClassName = function() {
  var str = this
  // var checkEnd = new RegExp(/[s]$/g)
  str = str.split('/').pop()
  return str
}

//Najularrrr
blogApp.controller('ReasonerCtrl', ['$scope','Restangular','$http', function ($scope, Restangular, $http) {


/**
 * Parse schema.org RDFa as RDF graph and return promise
 * @param  {[type]} schemaDoc       [description]
 * @param  {[type]} schemaGraph     [description]
 * @param  {[type]} schemaOrgServer [description]
 * @return {[type]}                 [description]
 */
var schemaParsedPromise = function(schemaDoc,schemaGraph,schemaOrgServer) {
    return new Promise(function(resolve, reject) {
        $rdf.parse(schemaDoc, schemaGraph, schemaOrgServer,'text/html')
        setTimeout(function(){
            if (schemaGraph.statements.length>0) {
                resolve(schemaGraph)
            } else {
                reject(Error('No Statement parsed! Time out!'))
            }
        },1000);
    });
}


/**
 * Fetch Hydra servers' entrypoints and create an entity for them
 * @param  {[type]} entrypointList [description]
 * @return {[type]}                [description]
 */
var fetchHydraApis = function(entrypointList) {
    return new Promise(function(resolve, reject) {
        var entryClasses = {}
        var isContext = new RegExp('^@');
          entrypointList.forEach(function(entrypoint){
            $http.get(entrypoint).then(function(response) {
                entryClasses[entrypoint] = {}
                var hydraApiClasses = (response.data);
                Object.keys(hydraApiClasses).forEach(function(api){
                      if (!isContext.test(api)) {
                        entryClasses[entrypoint][api.capitalize()] = {}
                        entryClasses[entrypoint][api.capitalize()]['link'] = hydraApiClasses[api]
                        entryClasses[entrypoint][api.capitalize()]['rank'] = 0;
                      }
                      // console.log(entryClasses)
                })
            })
        })
        setTimeout(function(){
            if (Object.keys(entryClasses).length==entrypointList.length) {
                resolve(entryClasses)
                console.log("All good")
            }
             else {
                resolve(entryClasses)
                console.log('Fetching from servers timed out, some may not be included')
            }
        },2000);
    });
}

/**
 * [Parse Hydrajsonld as RDF graph and return a promise]
 * @param  {[type]} hydraDoc    [description]
 * @param  {[type]} hydraGraph  [description]
 * @param  {[type]} hydraServer [description]
 * @return {[type]}             [description]
 */
var hydraParsedPromise = function(hydraDoc,hydraGraph,hydraServer) {
    return new Promise(function(resolve, reject) {
        $rdf.parse(hydraDoc, hydraGraph, hydraServer,'application/ld+json')
        setTimeout(function(){
            if (hydraGraph.statements.length>0) {
                resolve(hydraGraph)
            } else {
                reject(Error('No Statement parsed! Time out!'))
            }
        },5000);
    });
}


/*
 Split empty properties
 Check the value field
 */
function isEmpty(property) {
    if (isNaN(property.value)){
        return true
    } else {
        return false
    }
}

var datatypeList = ["xmls:dateTime","xmls:integer",'date','datetime','boolean','number','integer','float','text','string','time'];

function isDatatype(property,datatypeList) {
    if (datatypeList.indexOf(property.type)>-1){
        return true
    } else {
        return false
    }
}

function isSchema(property,schema) {
    var schemaUri = SCHEMA(property)
    var tempNum = schema.whether(schemaUri,null,null);
    tempNum = tempNum + schema.whether(null,null,schemaUri);
    if (tempNum==0){
        return false
    } else {
        return true
    }
}

/**
 * Parse the DOM and retrieve the schema properties
 * @return {[type]} [description]
 */
function dataParser() {}


/*
Check the domainIncludes relation to specify if we can, the superClass
of the properties we have. This can help us determine the Class
we need for API discovery
@propList: Array of properties
@schema: The KB we check against
@return: the array of classes sorted descending
array[0] = {
    matched: 1
    uri: "http://schema.org/Flight"
    value: 4
}
From the given list of properties we try to figure out it's domain
and if we can match it with a schema.org model
*/
function domainInc(propList,schema) {
    var commons = [];
    var domains = {};
    var arrayClass = [];
    //gia ka8e property mazeuw ta domains pou to periexoun!
    // gia ka8e domain pou anhkei ena property
    // gia ka8e property diko mas kai tou domain aukshse tn counter
    // Ara exoume enan pinaka me tis koines emfaniseis
    propList.forEach(function(property) {
        domains[property] = schema.each(SCHEMA(property),SCHEMA('domainIncludes'),null);
        domains[property].forEach(function(node){
            // console.log(node)
            if (isNaN(commons[node['uri']])) {
              commons[node['uri']] = 1
          } else {
            commons[node['uri']]++
        }
    });
    });
    // console.log(domains);
    // console.log('commons')
    // console.log(Object.keys(commons)[0])
    for (i=0;i<Object.keys(commons).length;i++) {
        arrayClass.push({
            uri: Object.keys(commons)[i],
            value: commons[Object.keys(commons)[i]]
        });
        arrayClass.sort(function (a, b) {
          if (a.value > b.value) {
            return -1;
        }
        if (a.value < b.value) {
            return 1;
        }
      // a must be equal to b
      return 0;
  });
    }
    // console.log(arrayClass)
    return arrayClass
};


/*
Return contained classes from our data.
1. We may have already classes with properties,
  requested or given.
2. We may have only properties, that need to be matched
  against schema.org Classes so that we can query server

 */
/**
 * [returnContainedClasses description]
 * @param  {[type]} data   [description]
 * @param  {[type]} schema [description]
 * @return {[type]}        [description]
 */
function returnContainedClasses(data,schema) {

//TEMP SCHEMA RELS

    console.log("data input ",data)
    var returnClasses = {}
    var containedClasses = []
    var orphanProps = [];
    var classList = [];
    // For every data Object, we do not know if it's a 
    // property or class
    //Return doaminIncludes for properties
    Object.keys(data).forEach(function(dataObj){
      // if (data[dataObj].hasOwnProperty("value")) {
      if (isDatatype(data[dataObj],datatypeList)) {
        //we have property 0 level
        console.log("0-level property:", dataObj)
        orphanProps.push(dataObj)
          /*
          If we have empty class, add the domainIncludes classes
          should we check for the number of matches? per class?
           */
      } else {
        //We suppose we have Class
        console.log("Input class with properties",dataObj)
          containedClasses.push(dataObj) // save CLASS
          classList.push(dataObj)
      }
      Object.keys(data[dataObj]).forEach(function(prop){
          // data[dataClass][prop] rangeIncludes
          // add rangeIncludes structured data to classes
          // containedClasses.push(dataClass.uri) // save CLASS
      })
      //for every class go check props and find structured data CLASSES
      // gia ka8e CLASS pou einai adeia-den exei class, trava thn domain class
      //
    }) //endOfIteration
    // GO find matching classes for properties
    var guessedClasses = domainInc(orphanProps,schema)
    guessedClasses.forEach(function(cl,i){
      guessedClasses[i].uri = guessedClasses[i].uri.toClassName()
      classList.push(guessedClasses[i].uri.toClassName());
    })
    console.log("guessedClasses",guessedClasses)
    console.log("containedClasses",containedClasses)
    console.log("orphanProps",orphanProps)
    console.log('classList',classList)
    return classList
    // return returnClasses
}

/*
We assign a rank to a server based on what we have/want
 */

function rankServer(arrayOfClasses,server) {
  var temp = 0;
  Object.keys(server).forEach(function(cl){
    if ((arrayOfClasses.indexOf(cl)>-1)) {
      temp++
    }
  })
  // An den periexei kamia klash, tote mhn t baleis mesa
  if (temp==0) {
    return null
  } else {
    server['rank'] = temp
    return server
  }
}

/*
This is called on the servers Entrypoints
We could assign this procedure while fetching,
bu this allows as to have prefetched our servers,
or cached!
 */
/**
 * [prepareServers description]
 * @param  {[type]} servers [description]
 * @param  {[type]} data    [description]
 * @param  {[type]} schema  [description]
 * @return {[type]}         [description]
 */
function prepareServers(servers,arrayOfClasses,schema) {

    Object.keys(servers).forEach(function(server){
      var temp = rankServer(arrayOfClasses,servers[server])
      if (temp!=null) {
        servers[server] = temp
      }
    })
  return servers
}


/*
 Sort servers, based on their rank
 */
function sortRankedServers(rankServerArray) {

}

var prepareServersPromise = function(serversEntrypointsFetched) {
  return new Promise(function(resolve, reject) {
    serversEntrypointsFetched.then(function(serversArray){
      Object.keys(serversArray).forEach(function(server){
          // console.log(server)
          var numMatchedClasses = 0;
          Object.keys(serversArray[server]).forEach(function(serviceClass){
              var temp = serversArray[server][serviceClass]
              // console.log('temp',temp)
              serversArray[server][serviceClass] = {}
              serversArray[server][serviceClass]['link'] = temp
              // serversArray[server][serviceClass]['matched'] = false
              Object.keys(domainClasses).forEach(function(classDomain){
                  // console.log(domainClasses[classDomain].uri.split('/').pop())
                  if (serviceClass==domainClasses[classDomain].uri.split('/').pop()) {
                      serversArray[server][serviceClass]['matched'] = false
                      numMatchedClasses++
                  }
              })
              serversArray[server]['rank'] = numMatchedClasses
          })
      })
    })
    setTimeout(function(){
        if (hydraGraph.statements.length>0) {
            resolve(hydraGraph)
        } else {
            reject(Error('No Statement parsed! Time out!'))
        }
    },5000);
  });
}


/*
Create new array per Class rather than per server
for each class, we select its server and rank
 */
function arrangePerClass(serversArray,arrayOfClasses,dataEdited) {
  // console.log("serversArray",serversArray)
  // console.log('arrayOfClasses',arrayOfClasses)
  // console.log('dataEdited',dataEdited)
  var ObjPerClass = {};
  arrayOfClasses.forEach(function(cl){
    ObjPerClass[cl] = arrayOfClasses[cl]
    Object.keys(serversArray).forEach(function(svr){
      ObjPerClass[cl] = {}
      ObjPerClass[cl]['servers'] = {}
      Object.keys(serversArray[svr]).forEach(function(svrCl){
        // console.log("svrCl",svrCl + " " + timeTest)
        if(cl==svrCl) {
          ObjPerClass[cl]['servers'][svr] = {}
          ObjPerClass[cl]['servers'][svr]['rank'] = serversArray[svr]["rank"];
        }
      })
    })
  })
  console.log("ObjPerClass",ObjPerClass)
  return ObjPerClass
}
/*
Get the ranked servers and start fetching data/resolving
 */

/*
Procedure to find mathing server classes
for orpahn properties
 */
  function orphanProps() {

  }

function depthOf(object) {
    var level = 1;
    var key;
    for(key in object) {
        if (!object.hasOwnProperty(key)) continue;

        if(typeof object[key] == 'object'){
            var depth = depthOf(object[key]) + 1;
            level = Math.max(depth, level);
        }
    }
    return level;
}

/*
Create Class hierarchy from input data

 */
  function classTier(data,depth) {
    var classN = Object.keys(data);
    var classRank = [];
    var propRank = [];
    var propTemp = [];
    var classTierObj = {};
    var props = {};
    var classArray;
    var temp = [];

    console.log("input data for classTier:", data);
    classN.forEach(function(prop){
      if(data[prop].hasOwnProperty("value")) { //we have a simple N property
        props[prop] = {};
        props[prop] = data[prop].type;
        if (!(isDatatype(data[prop],datatypeList))) { //an exoume Class type
          propTemp.push(data[prop].type);
        }
      } else { // the N is a Class
        [props[prop],classRank,propRank] = classTier(data[prop],(depth-1));
        classRank[depth] = [];
        classRank[depth].push(prop);
      }
    });
    if (!(Array.isArray(propRank[depth]))) {
      propRank[depth] = propTemp;
    } else {
      propRank[depth] = propRank[depth].concat(propTemp);
    }
    console.log("Depth: ",depthOf(props));
    console.log("classRank: ",classRank);
    console.log("propRank: ",propRank);
    var out = [props,classRank,propRank];
    return out;
  }

/*
Class seriazer
 */
// function serial(data) {
//   var depth = depthOf(data);
//   var classRank = new Array(depth);
//   var currentClass = data;
//   var i = depth;
//   while(i>0) {
//     Object.keys(currentClass).forEach(function(prop){
//       if(!(currentClass[prop].hasOwnProperty("value"))) { //we have N class
//         classRank[i].push(prop);
//       } else {}
//   }
// }

/*
Check input, to servers' classes
Create the likelyhood table,
 */
     //http://localhost:8080/contexts/Flight
     //http://localhost:8080/postal_addresses
  function rankClasses(serversArray,arrayOfClasses,data) {
    //for every property, we assume that we have only 1 class
    Object.keys(data ).forEach(function(prop){
      // console.log(prop);
      if (data[prop].hasOwnProperty('type')) {
        console.log("name: ",prop);
        //we have property and not Class
      } else {
        //we have class
      }
    })
    return 0;
  }
// function rankClasses(serversArray,arrayOfClassesIn,dataEdited) {

//   var arrayOfClasses = arrangePerClass(serversArray,arrayOfClassesIn,dataEdited)
//   console.log("arrayOfClassesApoRankClasses",arrayOfClasses)
//   Object.keys(arrayOfClasses).forEach(function(cl){
//     arrayOfClasses[cl]['filled'] = 0
//     // arrayOfClasses.cl['matched'] = 0
//     arrayOfClasses[cl]['empty'] = 0
//     // arrayOfClasses.cl['searchable'] = 0

//     var inputProps = dataEdited[cl]["properties"];
//     console.log("inputProps",inputProps)
//     Object.keys(arrayOfClasses[cl]["properties"]).forEach(function(property){
//       inputProps[property] = {}
//               // Check for empty property
//       if (!inputProps[property]) {
//         inputProps[property]['filled'] = true;
//         arrayOfClasses.cl['filled']++
//       } else {
//         inputProps[property]['filled'] = false;
//         arrayOfClasses.cl['empty']++
//       }
//     })//END of properties iterator
//     arrayOfClasses.cl.servers.forEach(function(server){
//       inputProps['done'] = 0;
//       inputProps['searchable'] = 0;
//       inputProps['matched'] = 0;

//       // for every Class and every Server
//       // We need to fetch search vars and properties
//       var classApiLink = server + arrayOfClasses.cl.servers[server].link;
//       var classContextLink = server + '/contexts/' + cl
//       $http.get(classApiLink).then(function(response) {
//         var api = JSON.parse(response.data);
//         api["hydra:search"]["hydra:mapping"].forEach(function(variable){
//           var propName = api["hydra:search"]["hydra:mapping"][variable]['property'];
//           //check if property is searchable
//           if (inputProps.hasOwnProperty(propName)) {
//             inputProps[propName]['searchable'] = true
//             inputProps['searchable']++
//           } else {
//             inputProps[propName]['searchable'] = false
//           }
//         })
//         inputProps['done']++;
//       }) //END of search get
//       $http.get(classContextLink).then(function(response) {
//         var context = JSON.parse(response.data);
//         //get below @base
//         context['@context'].forEach(function(propName){
//           if (inputProps.hasOwnProperty(propName)) {
//             inputProps[propName]['matched'] = true
//             inputProps['matched']++
//           } else {
//             inputProps[propName]['matched'] = false
//           }

//           var propName = context['@context'][prop]
//           if (typeof propName === 'object') {
//           }

//         })
//         inputProps['done']++;
//       })// END of context get
//       // Exoun pragmatopoih8ei kai ta 2 get!!!
//       if (inputProps['done']==2) {
//         var svrRanks = arrayOfClasses.cl.servers[server];
//         var propSum = (arrayOfClasses.cl['empty']+arrayOfClasses.cl['filled'])
//         svrRanks = {}
//         svrRanks['matchedToAll'] = inputProps['matched']/propSum;
//         svrRanks['searchableToFilled'] = inputProps['searchable']/arrayOfClasses.cl['filled']
//         svrRanks['emptyAndMatched'] = 0;
//         inputProps.forEach(function(prop){
//           if((inputProps.prop['matched'])&& (!inputProps.prop['filled'])) {
//             svrRanks['emptyAndMatched']++
//           }
//         })
//         svrRanks['emptyMatchedToEmpty'] = svrRanks['emptyAndMatched']/arrayOfClasses.cl['empty'];
//       }

//     })//END of servers iterator
//   })
//   return 0;
// }
/*
Match classes based on the ranked class table
 */
function matchClasses(arrrangedPerClassSearch,arrayOfClasses) {
  var svrRanks = arrayOfClasses.cl.servers[server];
}

function fixData(data) {
  console.log("Classes ranked: ",classTier(data,0));
  var dataOut = {}
  Object.keys(data).forEach(function(cl) {
    dataOut[cl] = {}
    dataOut[cl]['properties'] = {}
    dataOut[cl]["properties"] = data[cl]
  })
  console.log("dataInFixed",dataOut)
  return dataOut
}

/*
Return the results from matching
 */
function returnData() {

}

function resolveApis(servers,data,schema) {
 var dataEdited = fixData(data);

  Object.keys(servers).forEach(function(svrName){
    console.log(svrName)
  })
  var arrayOfClasses = returnContainedClasses(data,schema)
  console.log("arrayOfClasses",arrayOfClasses);
  console.log("dataEditedStart: ", dataEdited);
  var serversArray = prepareServers(servers,arrayOfClasses,schema) //exoume etoimo ranked tous servers
  var arrrangedPerClassSearch = rankClasses(serversArray,arrayOfClasses,dataEdited)
  // var matchedClasses = matchClasses(arrrangedPerClassSearch,arrayOfClasses)
  // var out = returnData(matchedClasses)
     //Gia ka8e entrypoint pou tairiazei,
     return 0;
}

/*
Constants and arguments
 */
var entrypointList = ['http://localhost:8080','http://localhost:8081'];
var hydraObject;
var hydraGraph = $rdf.graph();
console.log(hydraGraph);
var hydraDoc = {};
var hydraServerUri = 'http://localhost:8080/';
var hydraServer = 'http://localhost:8080/apidoc';
var hydraPromise = jsonld.documentLoader(hydraServer);
var schemaKB = $rdf.graph();
// var schemaOrgServer = 'https://raw.githubusercontent.com/schemaorg/schemaorg/sdo-gozer/data/schema.rdfa';
var schemaOrgServer = 'http://localhost:9000/schema_3_2.rdfa';
var serversList = fetchHydraApis(entrypointList);
console.log('Hydra servers Entypoins',serversList);

// TEST INPUT DATA
var testData = {
  dataClass1: {
    departureTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    dataClass22: {
      type: "testClass",
      value: "2013-03-01T16:15:09+01:00"
    },
    arrivalTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    dataClass2: {
      level2Property: {
        type: "#Airport",
        value: "Thessaloniki"
      },
      dataClass3: {
        level3Property: {
          type: "#Airport",
          value: "Thessaloniki"
        },
        level4Property: {
          type: "#TestPropClass4",
          value: "Thessaloniki"
        }
      }
    }
  },
    departureAirport: {
        type: "#Airport",
        value: "Thessaloniki"
    },
    arrivalAirport: {
        type: "#Airport",
        value: "Chios"
    },
    PostalAddress: {
        type: "addressLocality",
        value: ''
    },
    addressLocality: {
        type: "string",
        value: "Chios"
    },
    adultNumber: {
        type: "xmls:integer",
        value: 1
    }
    // offer:{
    //     type: "#Offer",
    //     value: null
    // }
};

// TEST INPUT DATA
var testdataType = {
    departureTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    arrivalTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    departureAirport: {
        type: "#Airport",
        value: "Thessaloniki"
    },
    arrivalAirport: {
        type: "#Airport",
        value: "Chios"
    },
    PostalAddress: {
        type: "addressLocality",
        value: ''
    },
    addressLocality: {
        type: "string",
        value: "Chios"
    },
    adultNumber: {
        type: "xmls:integer",
        value: 1
    }
    // offer:{
    //     type: "#Offer",
    //     value: null
    // }
};


    // $scope variables
    $scope.freeTextInput = "free text input test";
    $scope.$watch("freeTextInput",function (input){    });
    $scope.modelReasoner = "test";

    var timeStartFetch = performance.now();
    $http.get(schemaOrgServer).then(function(response) {
        // var schemaDoc = response.data
        schemaParsedPromise(response.data,schemaKB,schemaOrgServer).then(function(schemaGraph){
            var timeReadySchema = performance.now();
            console.log('Time took to fetch & parse schemaorg: ' + (timeReadySchema-timeStartFetch) + ' ms')
            console.log('KB from SCHEMA',schemaKB);
            serversList.then(function(entrypointArray){
              // console.log("entrypointArray",entrypointArray["http://localhost:8080"])
              resolveApis(entrypointArray,testData,schemaKB);
            })
       }); // END of schemaOrg Parse!
    }, //END of schemaOrg Fetching
    function errorCallback(response) {
        alert('Failed to fetch schema.org');
    });// SCHEMA fetch to rdf graph

}]); // Controller END