/*
Reasoner
Parser
Matcher
Connector
Fetcher
*/

// sigma.js Grapgh

var g = {
    nodes: [],
    edges: []
};

var i,
    N = 2,
    E = 1
    // g = {
    //   nodes: [],
    //   edges: []
    // };
// Generate a random graph:
for (i = 0; i < N; i++)
  g.nodes.push({
    id: 'n' + i,
    label: 'Node ' + i,
    x: Math.random()*2,
    y: Math.random(),
    size: Math.random(),
    color: '#000'
  });
for (i = 0; i < E; i++)
  g.edges.push({
    id: 'e' + i,
    label: 'TEST1',
    source: 'n' + (Math.random() * N | 0),
    target: 'n' + (Math.random() * N | 0),
    size: Math.random()*10,
    color: '#ccc'
  });

    var troll ;//= $rdf.graph();

    var RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
    var RDFS = $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
    var FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
    var XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
    var SCHEMA = $rdf.Namespace("http://schema.org/");
    var HYDRA = $rdf.Namespace("http://www.w3.org/ns/hydra/core#");

    var rdfs = {
        subClassOf: $rdf.sym('rdfs:subClassOf'),
        subPropertyOf: $rdf.sym('rdfs:subPropertyOf'),
        label: $rdf.sym('rdfs:label'),
        comment: $rdf.sym('rdfs:comment'),
        test2: $rdf.sym('rdfs:subPropertyOf'),
        test34: $rdf.sym('rdfs:subPropertyOf')
    };

// TEST INPUT DATA
var testdataType = {
    departureTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    arrivalTime: {
        type: "xmls:dateTime",
        value: "2013-03-01T16:15:09+01:00"
    },
    departureAirport: {
        type: "#Airport",
        value: "Thessaloniki"
    },
    arrivalAirport: {
        type: "#Airport",
        value: "Chios"
    },
    adultNumber: {
        type: "xmls:integer",
        value: 1
    }
};

var sigmaGraph;

var inputFormData = Object.keys(testdataType);

    blogApp.controller('ReasonerCtrl', ['$scope','Restangular','$http', function ($scope, Restangular, $http) {

// $scope variables
$scope.freeTextInput = "free text input test";
$scope.$watch("freeTextInput",function (input){
});
$scope.modelReasoner = "test";

//  = new sigma({
//   graph: g,
//   renderers: [
//     {
//       container: document.getElementById('graph-container'),
//       type: 'canvas' // sigma.renderers.canvas works as well
//     }
//   ]
// });

  /*
Check if the property-term exists in schema.org and display the number of relations
*/
function checkProperty(propList,schema) {
    var relNum = [];
    propList.forEach(function(property) {
        var tempNum = schema.whether(SCHEMA(property),null,null);
        tempNum = tempNum + schema.whether(null,null,SCHEMA(property));
        relNum[property] = tempNum;
    });
    return relNum;
}

/*
Check the domainIncludes relation to specify if we can the superClass
of the properties we have. This can help us determine the Class
we need for API discovery
@propList: Array of properties
@schema: The KB we check
@return: the array of classes sorted descending
*/
function domainInc(propList,schema) {
    var commons = [];
    var domains = {};
    var arrayClass = [];
    propList.forEach(function(property) {
        domains[property] = schema.each(SCHEMA(property),SCHEMA('domainIncludes'),null);
        domains[property].forEach(function(node,index){
            if (isNaN(commons[node['uri']])) {
              commons[node['uri']] = 1
          } else {
            commons[node['uri']]++
        }
    });
    });
    // console.log(domains);
    // console.log('commons')
    // console.log(Object.keys(commons)[0])
    for (i=0;i<Object.keys(commons).length;i++) {
        arrayClass.push({
            uri: Object.keys(commons)[i],
            value: commons[Object.keys(commons)[i]]
        });
        arrayClass.sort(function (a, b) {
          if (a.value > b.value) {
            return -1;
        }
        if (a.value < b.value) {
            return 1;
        }
      // a must be equal to b
      return 0;
  });
    }
    return arrayClass
};

var testAPIObject = {
    className: {
        name: "range"
    }
};

/*
Try to match given data with an API.
We should call this function with every API we want to check against.
Returns an array of schema classes with the indication match 1/0
 */

function apiMatch(testAPIObject,propList,schema) {

    var classCounter = domainInc(propList,schema);
    var match = classCounter;
    var apiClasses = Object.keys(testAPIObject);
    apiClasses.forEach(function(apiClass){
        classCounter.forEach(function(dataClass,index){
            // console.log(SCHEMA(apiClass).uri)
            // console.log(dataClass.uri)
            if (SCHEMA(apiClass).uri == dataClass.uri){
                match[index].matched = 1
            } else {
                if (isNaN(match[index].matched)) {
                    match[index].matched = 0
                }
            }
        });
    });
    // console.log(match)
    return match
}

// function deferProperty(class,api,schema) {
// return
// }

/*
Check properties of Hydra object and data input
Return array of matched itesm
@hydraClass: 
 */
function propertyMatch(testAPIObject,hydraClass,dataClass) {
    var dataMatchedProp = dataClass;
    var hydraMatchedProp = hydraClass;
    var matchedProps = dataClass;
    var isLink = new RegExp('^#');
    var linkedClass = [];
    //     return isLink.test(value);
    // init as non matched for every data property
    Object.keys(matchedProps).forEach(function(dataPropName){
            matchedProps[dataPropName].matched = 0;
    });
    //check match against hydra properties
    Object.keys(hydraClass).forEach(function(hydraPropName,iHydra){
        if (!matchedProps.hasOwnProperty(hydraPropName)) {
            matchedProps[hydraPropName] = {
                type: hydraClass[hydraPropName],
                value: null,
                matched: 0
            };
            hydraMatchedProp[hydraPropName].matched = 0;
        } else {
            matchedProps[hydraPropName].matched = 1;
            hydraMatchedProp[hydraPropName].matched = 1;
        }
    });

    // Check properties that refer to classes
    Object.keys(matchedProps).forEach(function(prop){
        // console.log(matchedProps[prop])
        if (isLink.test(matchedProps[prop].type)) {
            var classString = matchedProps[prop].type.replace(/#/g, "");
            linkedClass.push(testAPIObject[prop[classString]]);
            console.log(testAPIObject[classString])
        }
    });
    return matchedProps;
}
/*
Match the data from the input to the API class
 */
function dataMatch(testAPIObject,dataInput,schema) {
    // var classCounter = domainInc(propList,schema);
    var selectedAPI = apiMatch(testAPIObject,Object.keys(dataInput),schema);

    var classes = dataInput
    selectedAPI.forEach(function(c){
        if (c.matched == 1 ){
            console.log('We have a match!')
            console.log(c.uri)
        }
    });
    //ASD Check split uri
    var matchedClass = testAPIObject['Flight'];
    // console.log(matchedClass)
    // console.log(dataInput)

    var matchedClassProp = propertyMatch(testAPIObject,matchedClass,dataInput);
    console.log(matchedClassProp);

return 0
}

function addNode(hydraGraph,g) {

var subj,obj,subjC,objC;
var n=0;
            hydraGraph.statements.forEach(function(st){
                // console.log('statement')
                // visualise graph
                var subj,obj,subjC,objC;
                if (st.subject.hasOwnProperty('value')) {
                    subj = st.subject.value;
                } else {
                    subj = st.subject.uri;
                };

                if (st.object.hasOwnProperty('value')) {
                    obj = st.object.value;
                } else {
                    obj = st.object.uri;
                };

                if(st.subject.hasOwnProperty('value')) {
                    g.nodes.push({
                        id: st.subject.value,
                        label: st.subject.value,
                        x: Math.random()*10,
                        y: Math.random()*10,
                        size: Math.random(),
                        color: '#87CEEB'
                    });
                } else {
                    g.nodes.push({
                        id: st.subject.uri,
                        label: st.subject.uri,
                        x: Math.random()*10,
                        y: Math.random()*10,
                        size: Math.random(),
                        color: '#F08080'
                    });
                }
                if(st.object.hasOwnProperty('value')) {
                    g.nodes.push({
                        id: st.object.value,
                        label: st.object.value,
                        x: Math.random()*10,
                        y: Math.random()*10,
                        size: Math.random(),
                        color: '#00008B'
                    });
                } else {
                    g.nodes.push({
                        id: st.object.uri,
                        label: st.object.uri,
                        x: Math.random()*10,
                        y: Math.random()*10,
                        size: Math.random(),
                        color: '#FF0000'
                    });
                }
                var ccc = '#ccc'
                if ((subj=='b0_b7')||(obj=='b0_b7')) {
                    ccc = '#000'
                }
                g.edges.push({
                    id: n,
                    label: st.predicate.uri,
                    source: subj,
                    target: obj,
                    size: 5,
                    type: "arrow",
                    color: ccc
                });
                n++;
            })//end of statement iterations

            var nodeNames  = [];
            g.nodes.forEach(function(node,index){
                nodeNames[index] = node.id;
            })

            // console.log(nodeNames)
            var uniq = nodeNames
            .map((name) => {
              return {count: 1, name: name}
            })
            .reduce((a, b) => {
              a[b.name] = (a[b.name] || 0) + b.count
              return a
            }, {})

            console.log('Number of unique URIs: '+Object.keys(uniq).length)

            g.nodes.forEach(function(node,index){
                Object.keys(uniq).forEach(function(uname,uindex){
                    if ((node.id == uname)&&(uniq[uname]>1)) {
                        console.log('match')
                        uniq[uname] = uniq[uname] - 1;
                        g.nodes[index] = undefined;
                    }
                })
            })
            var temp = g.nodes.filter(Boolean);
            g.nodes = temp;
            delete temp;
            console.log(g.nodes)
            sigmaGraph = new sigma({
        graph: g,
        renderer: {
            container: 'graph-container',
            type: 'canvas'
        },
        settings: {
            edgeLabelSize: 'proportional',
            // drawEdgeLabels: true,
            edgeLabelThreshold: 0,
            edgeLabelSize: 8,
            defaultEdgeLabelSize: 8,
            labelThreshold: 3,
            labelSizeRatio: 5,
            defaultNodeBorderColor: '#F9F694',
            edgeLabelSizePowRatio: 8,
            font: "arial",
            minNodeSize: 5,
            minEdgeSize: 1,
            edgeHoverSizeRatio: 15,
            minArrowSize: 5
            // fontStyle: "bold"
        }
});
        sigmaGraph.refresh();
}

//Hydra RDF Graph
var mits;
var hydraGraph = $rdf.graph();
var hydraDoc = {};
var hydraServer = 'http://localhost:8000/apidoc'
var lala = jsonld;
// console.log(lala)

// var jApi = jsonld.promises();
// hydraPromise.promises(jApi);
// console.log(jApi)
// console.log(hydraPromise)
// $rdf.sym('http://schema.org/Airport')
// SCHEMA('Airport')

var hydraPromise = jsonld.documentLoader(hydraServer)//, function(response){

//We have to wait in order for the rdf.parse to finish, since it's not a promise??
var hydraParsedPromise = function(hydraDoc,hydraGraph) {
    return new Promise(function(resolve, reject) {
        $rdf.parse(hydraDoc, hydraGraph, 'http://localhost:8000/apidoc', 'application/ld+json')
        setTimeout(function(){
            if (hydraGraph.statements.length>0) {
                resolve(hydraGraph)
            } else {
                reject(Error('No Statement parsed! Time out!'))
            }
        },1000);
    });
}

hydraPromise.then(function(response){
    console.log('JSON retrieved')
    hydraDoc = response.document;
    // var testHydra = jsonld.toRDF(hydraDoc, {format: 'application/nquads'}, function(err, nquads) {});
    // console.log(hydraDoc);
      // nquads is a string of nquads
      // var hydraGraph;

        // var mitsos = jApi.toRDF(hydraDoc)
        // console.log(mitsos)
console.log(hydraParsedPromise)

hydraParsedPromise(hydraDoc,hydraGraph).then(function(hydraGraph){
    // console.log(hydraGraph)
    // console.log(hydraGraph.statements.length)

    addNode(hydraGraph,g);

        var tt = {
            id: 1,
            value: 'b0_b11'
        }
        var bbn = $rdf.BlankNode;
        console.log('BlankNode')
        // hydraGraph.bnode(12)
        // console.log(hydraGraph.BlankNode(100));

        // setTimeout(function(){
            console.log('Hydra Graph ')
            // console.log(hydraGraph.statements[106].subject.value)
            // console.log(hydraGraph.isBlankNode(bbn))
            // hydraGraph.statements[106].subject.value = "b0_07"
            // hydraGraph.statements[106].subject.id = null
            console.log('exw t hydraGraph')
            console.log(hydraGraph.statements[106])
            // var mits = hydraGraph.each(hydraGraph.statements[106].subject,null,null);
            // console.log(mits)

        // }, 500);
        //  setTimeout(function(){
        // // console.log(sigmaGraph)
        // // console.log(g)
        // sigmaGraph.refresh();
        // }, 2000);
        // var mits = hydraGraph.whether($rdf.sym('http://schema.org/departureTime'),null,null);
        // console.log(mits)
        // mits = $rdf.serialize(hydraGraph);
        // console.log(mits)
        // hydraGraph.statements.forEach(function(st,index){
        //     console.log(index)
        //     // console.log(st.subject)
        // });
    // });
    // console.log(response.document)
})
})

//Other Method t get HydraDoc
// var tester = $rdf.graph;
// // console.log(tester)
//  $http({
//         method: 'GET',
//        url: 'http://localhost:8000/apidoc',//uri2,//request_url,
//        headers: {
//         'Accept': 'text/html'//application/json+ld
//     },
//     }).then(function successCallback(response) {
//         console.log(response);
//         jsonld.toRDF(response.data, function(err, tester) {
//       // nquads is a string of nquads
//           console.log(err)
//         });
//         console.log(tester)
//     },
//     function errorCallback(data) {
//      alert('Failed');
//     });
// setTimeout(function(){
        // console.log(sigmaGraph)
        console.log(g)
    
        // }, 5000);
    // var uri2 = 'https://raw.githubusercontent.com/schemaorg/schemaorg/sdo-deimos/data/schema.rdfa';
    var uri2 = 'https://raw.githubusercontent.com/schemaorg/schemaorg/sdo-gozer/data/schema.rdfa';
    // uri = 'http://www.worldcat.org/oclc/7977212';
    var uri = 'https://schema.org/Place';//'http://www.worldcat.org/oclc/7977212';
    var schemaKB = $rdf.graph();
    var request_url = 'https://raw.githubusercontent.com/schemaorg/schemaorg/sdo-gozer/data/schema.rdfa';
    $http({
        method: 'GET',
       url: request_url,//uri2,//request_url,
       headers: {
        'Accept': 'text/html'//application/rdf+xml
    },
    }).then(function successCallback(response) {
        // console.log('response');
        // console.log(response);

        //create the graph and load the data
        $rdf.parse(response.data, schemaKB, '', 'text/html');
        console.log('KB from SCHEMA');
        console.log(schemaKB);
        // PLAY with the rdf graph
        // var knows = FOAF('knows')
        // var friend = store.any(me, knows)  // Any one person
        // console.log(friend)

        // console.log(inputFormData[3]);
        // console.log(checkProperty(inputFormData,schemaKB));
        // console.log(findCommonClass(inputFormData,schemaKB));
        var classesArray = domainInc(inputFormData,schemaKB);
        // console.log(classesArray);

        var me = $rdf.sym('http://schema.org/departureTime');
        var me2 = $rdf.sym('http://www.w3.org/2000/01/rdf-schema/subClassOf');
        var me3 = SCHEMA('Airport');
        var me4 = 'http://schema.org/Airport';

        var t0 = performance.now();
        // troll = schemaKB.the(SCHEMA(inputFormData[0]),null,null);
        troll = schemaKB.topTypeURIs(me);
        // troll = schemaKB.transitiveClosure(SCHEMA(inputFormData[3]),rdfs.subClassOf,true);
        var t1 = performance.now();
        // console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.")
        // console.log('Trollll')
        // console.log(troll);
    },
    function errorCallback(response) {
     alert('Failed');
    });

$scope.data = {
    departureTime: "2013-03-01T16:15:09+01:00",
    arrivalTime: "2013-03-01T16:15:09+01:00",
    departureAirport: "Thessaloniki",
    arrivalAirport: "Chios",
    adults: 1
};

// postalAddress:Object
// addressCountry:"xmls:string"
// addressLocality: "xmls:string"
// addressRegion: "xmls:string"
// id: "xmls:integer"

// address: "#PostalAddress"
// description: "xmls:string"
// iataCode:"xmls:string"
// id: "xmls:integer"
// name: "xmls:string"

var test = {};
var testAPIObject = {
    className: {
        name: "range"
    }
};

//Fetch Hydra Server APIDoc
Restangular.all('apidoc').customGET().then(function (data) {
    // console.log(data);
    // var testHydra = jsonld.toRDF(data, {format: 'application/nquads'}, function(err, nquads) {
      // nquads is a string of nquads
    // });
    // console.log(testHydra)
    var classes = data['hydra:supportedClass'];
    var supportedClasses = [];
    // console.log(classes[0]['hydra:supportedProperty'][0]['hydra:property']);

// For Each supported Class - Object, create an object for every entity and it's properties
    angular.forEach(classes, function(clValue, clKey) {
        // console.log(key+" : "+value);
        var temp = clValue['hydra:title'];
        var obj = {};
        // obj['propsName'] = temp;
        // test[clKey]['propName'] = clValue['hydra:title'];
        // test.description = clValue['hydra:description'];
        var property = clValue['hydra:supportedProperty'];

        angular.forEach(property, function(propValue, propKey) {
            var propRange = propValue['hydra:property']['range'];
            var propName = propValue['hydra:property']['rdfs:label'];
            // console.log(propRange);
            // console.log(propName);
            obj[propName] = propRange;
        });
        test[temp] = obj;
        // console.log(clValue);
        // supportedClasses.push(value['hydra:title']);
    });
    console.log("Hydra API Classes")
    console.log(test);
    // console.log(supportedClasses[1]);
/*
CONNECT API with Agent
 */
        // sigmaGraph.refresh();

    var selectedAPI = apiMatch(test,inputFormData,schemaKB);
    console.log(selectedAPI);
    var lala = dataMatch(test,testdataType,schemaKB);
    console.log(lala)
/*
AUTOCREATED FORM!!

    // $scope.postTemplate = test['Flight'];
    angular.forEach($scope.postReceive, function(value, key){
        $scope.postReceive[key] = null;
        console.log($scope.postReceive);
    });
    // $scope.postTemplate.isLink = function(value){
    //     var reg = new RegExp('^#');
    //     return reg.test(value);
    // };
    // $scope.postReceive.createKey = function(value){
    //     return $scope.postReceive.value;
    // };
    */

  }); // Fetched APIDoc 

/*

*/


}]);

// <div vocab="http://schema.org/" typeof="LocalBusiness">
//   <h1><span property="name">Beachwalk Beachwear & Giftware</span></h1>
//   <span property="description"> A superb collection of fine gifts and clothing
//   to accent your stay in Mexico Beach.</span>
//   <div property="address" typeof="PostalAddress">
//     <span property="streetAddress">3102 Highway 98</span>
//     <span property="addressLocality">Mexico Beach</span>,
//     <span property="addressRegion">FL</span>
//   </div>
//   Phone: <span property="telephone">850-648-4200</span>
// </div>
// 
// 
// find common classes the properties refer to
// 
//   function findCommonClass(propList,schema) {
//     var everyClass = [];
//     var objects = {};
//     var subjects = {};
//     var test_rel;
//     var commons = {};
//     var nodes = [];
//     var domainInc = {};
//     // test_rel = schema.each(SCHEMA('departureTime'),null,null);
//     //SCHEMA('domainIncludes')
//     //SCHEMA('rangeIncludes')
//     propList.forEach(function(property) {
//         var subjectsPre = schema.each(SCHEMA(property),null,null);
//         // console.log(subjectsPre);
//         objects[property] = {};
//         subjectsPre.forEach(function(predicate,index){
//             // if ((predicate['uri']!='rdfs:label') && (predicate['uri']!='rdfs:comment')) {
//                 objects[property][predicate['uri']] = schema.each(SCHEMA(property),$rdf.sym(predicate['uri']),null);
//                 objects[property][predicate['uri']].forEach(function(node){
//                     nodes.push(node['uri']);
//                 });
//             });

//         var objectsPre = schema.each(null,null,SCHEMA(property));
//         subjects[property] = {};
//         objectsPre.forEach(function(predicate,index){
//             subjects[property][index] = schema.each(null,$rdf.sym(predicate['uri']),SCHEMA(property));
//         });

//         // everyClass[property] = subjectsPre+objectsPre;
//     });
// // console.log(nodes);
//     console.log('Objects')
//     console.log(objects);
//     console.log('Subjects')
//     console.log(subjects)
//     // find the same class conataining the superClass
//     // objects.forEach(function(prop,index){
//     //     prop.forEach(function(rel,index){
//     //       
//     //     });
//     // });
//     console.log('Objects Array')
//     // console.log(test_rel)
//     // console.log(everyClass)
//     return objects;

//   }