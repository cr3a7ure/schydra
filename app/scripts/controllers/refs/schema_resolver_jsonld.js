var RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
var RDFS = $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
var FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
var XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
var SCHEMA = $rdf.Namespace("http://schema.org/");
var HYDRA = $rdf.Namespace("http://www.w3.org/ns/hydra/core#");

    // $rdf.log.info = function(str) {console.log("info:  "+str)};
    // $rdf.log.debug = function(str) {console.log("debug: "+str)};


//Najularrrr
blogApp.controller('ReasonerCtrl', ['$scope','Restangular','$http', function ($scope, Restangular, $http) {
// Fetchers - Promises
// ################################

/**
 * Parse schema.org RDFa as RDF graph and return promise
 * @param  {[type]} schemaDoc       [description]
 * @param  {[type]} schemaGraph     [description]
 * @param  {[type]} schemaOrgServer [description]
 * @return {[type]}                 [description]
 */
var schemaParsedPromise = function(schemaDoc,schemaGraph,schemaOrgServer) {
    return new Promise(function(resolve, reject) {
        $rdf.parse(schemaDoc, schemaGraph, schemaOrgServer,'text/html')
        // setTimeout(function(){
            if (schemaGraph.statements.length>0) {
                resolve(schemaGraph)
            } else {
                reject(Error('No Statement parsed! Time out!'))
            }
        // },1000);
    });
}

// //We have to wait in order for the rdf.parse to finish, since it's not a promise??
// var hydraParsedPromise = function(hydraDoc,hydraGraph,hydraServer) {
//     return new Promise(function(resolve, reject) {
//         $rdf.parse(hydraDoc, hydraGraph, hydraServer,'application/ld+json')
//         setTimeout(function(){
//             if (hydraGraph.statements.length>0) {
//                 resolve(hydraGraph)
//             } else {
//                 reject(Error('No Statement parsed! Time out!'))
//             }
//         },8000);
//     });
// }

/**
 * Fetch Hydra servers' entrypoints and create an entity for them
 * ClassName: URI
 * @param  {[type]} entrypointList [description]
 * @return {[type]}                [description]
 */
var fetchHydraApis = function(entrypointList) {
    return new Promise(function(resolve, reject) {
        var entryClasses = {}
        var isContext = new RegExp('^@');
          entrypointList.forEach(function(entrypoint){
            $http.get(entrypoint).then(function(response) {
                entryClasses[entrypoint] = {}
                var hydraApiClasses = (response.data);
                Object.keys(hydraApiClasses).forEach(function(api){
                      if (!isContext.test(api)) {
                        entryClasses[entrypoint][api.capitalize()] = {}
                        entryClasses[entrypoint][api.capitalize()]['link'] = hydraApiClasses[api]
                        entryClasses[entrypoint][api.capitalize()]['rank'] = 0;
                      }
                      // console.log(entryClasses)
                })
            })
        })
        setTimeout(function(){
            if (Object.keys(entryClasses).length==entrypointList.length) {
                resolve(entryClasses)
                console.log("All good")
            }
             else {
                resolve(entryClasses)
                console.log('Fetching from servers timed out, some may not be included')
            }
        },2000);
    });
}

/**
 * Fetch Hydra servers' entrypoints and create an entity for them
 * ClassName: URI
 * @param  {[type]} entrypointList [description]
 * @return {[type]}                [description]
 */
var fetchHydraApiz = function(entrypoint) {
    return new Promise(function(resolve, reject) {
      var url = entrypoint + '/docs.jsonld';
      $http.get(url).then(function(response) {
      //Gia ka8e response
      var hydraDoc = response.data;
      // hydraDoc.toString().replace('#', 'http:\/\/localhost:8000\/');
      // console.log(hydraDoc)
      // Parse Hydra as JSON and modify it according to our needs!!!
      hydraObject = hydraDoc;//JSON.parse(hydraDoc);
      hydraObject["@context"]["@base"] = entrypoint;// "/apidoc"
      // hydraObject['hydra:supportedClass'].forEach(function(supClass){
      //   supClass['hydra:supportedProperty'].forEach(function(obj){
      //     obj['@id'] = obj['hydra:property']['@id']+'/hydra'
      //     // console.log(obj['hydra:property']['@id'])
      //   })
      //   // console.log(supClass["hydra:supportedOperation"])
      //   if (Array.isArray(supClass["hydra:supportedOperation"])) {
      //     supClass["hydra:supportedOperation"].forEach(function(obj){
      //       obj['@id'] = supClass['@id'] + '/' + obj["hydra:method"]
      //         // console.log(obj['@id'])
      //     })
      //   }
      //   if (supClass['@id'].includes("Entrypoint")) {
      //     supClass['hydra:supportedOperation']['@id'] = supClass['@id'] + '/' + supClass['hydra:supportedOperation']['hydra:method']
      //     console.log(supClass['hydra:supportedProperty'])
      //     supClass['hydra:supportedProperty'].forEach(function(obj){
      //       // console.log(obj)
      //       obj['hydra:property']['hydra:supportedOperation'].forEach(function(propObj){
      //         propObj['@id'] = obj['@id'] + '/' + propObj['hydra:method']
      //       })
      //   })
      //   } //Entrypoint specific
      // }) // forEACH hydra:supportedClass
      // Ready JSONLD for RDF
      hydraDoc = JSON.stringify(hydraObject);
      // console.log(hydraDoc);
        // setTimeout(function(){
      if (true) {
        resolve(hydraDoc)
        console.log("All good from server:",entrypoint)
      }
       else {
        resolve(hydraDoc)
        console.log('Fetching from servers timed out, some may not be included')
      }
        // },2000);
    });
  });
}

/**
 * [Parse Hydrajsonld as RDF graph and return a promise]
 * @param  {[type]} hydraDoc    [description]
 * @param  {[type]} hydraGraph  [description]
 * @param  {[type]} hydraServer [description]
 * @return {[type]}             [description]
 */
var hydraParsedPromise = function(hydraDoc,hydraGraph,hydraServer) {
    return new Promise(function(resolve, reject) {
      // console.log($rdf.parse(hydraDoc, hydraGraph, hydraServer,'application/ld+json'))
        // var ready = false;
        //  function cb(test,hydraGraph) {
        //   return new Promise(function(resolve, reject) {
        //   console.log("DONEEE");
        //   console.log(hydraGraph);
        //   console.log(test)
        // });
        // }

        $rdf.parse(hydraDoc, hydraGraph, hydraServer,'application/ld+json')
        // cb(test,hydraGraph).then
        setTimeout(function(){
            if (hydraGraph.statements.length>0) {
                resolve(hydraGraph)
            } else {
                reject(Error('No Statement parsed! Time out!'))
            }
        },2500);
    });
}

/*
 * Fetch server apidoc.jsonld
 * Return only the classes and it's properties
*/
var fetchApiDoc = function(svr) {
  return new Promise(function(resolve, reject) {
    var url = svr+"/apidoc";
    // var url = svr+"/apidoc.jsonld";
    var isContext = new RegExp('^@');
    var supportedClasses = {};
    var classObj = {};
    $http.get(url).then(function(response) {
      var hydraObject = (response.data);
      // Object.keys(hydraObject).forEach(function(api){
        var classes = hydraObject['hydra:supportedClass'];
        // console.log(classes[0]['hydra:supportedProperty'][0]['hydra:property']);
        // For Each supported Class - Object, create an object for every entity and it's properties
        classes.forEach(function(clValue, clKey) {
        // console.log(key+" : "+value);
        if ((clValue['rdfs:label'])){
            var temp = clValue['hydra:title'];
            var obj = {};
            // obj['propsName'] = temp;
            // classObj[clKey]['propName'] = clValue['hydra:title'];
            // classObj.description = clValue['hydra:description'];
            var property = clValue['hydra:supportedProperty'];
            property.forEach(function(propValue, propKey) {
                var propRange = propValue['hydra:property']['range'];
                var propName = propValue['hydra:property']['rdfs:label'];
                // console.log(propRange);
                // console.log(propName);
                obj[propName] = {
                    type: propRange
                }
            }); //END of properties iteration
            classObj[temp] = obj;
            supportedClasses[temp] = obj;
            classObj = {};
        // if (!isContext.test(api)) {
        //   entryClasses[entrypoint][api.capitalize()] = {}
        //   entryClasses[entrypoint][api.capitalize()]['link'] = hydraApiClasses[api]
        //   entryClasses[entrypoint][api.capitalize()]['rank'] = 0;
        }
      })
          // setTimeout(function(){
        if (Object.keys(supportedClasses).length>0) {
      resolve(supportedClasses)
      console.log("All good")
    } else {
      reject(supportedClasses)
      console.log('Fetching from server timed out!')
    }
      // },2000);
    })//END of get(URL).then
  })
}

var prepareServersPromise = function(serversEntrypointsFetched) {
  return new Promise(function(resolve, reject) {
    serversEntrypointsFetched.then(function(serversArray){
      Object.keys(serversArray).forEach(function(server){
          // console.log(server)
          var numMatchedClasses = 0;
          Object.keys(serversArray[server]).forEach(function(serviceClass){
              var temp = serversArray[server][serviceClass]
              // console.log('temp',temp)
              serversArray[server][serviceClass] = {}
              serversArray[server][serviceClass]['link'] = temp
              // serversArray[server][serviceClass]['matched'] = false
              Object.keys(domainClasses).forEach(function(classDomain){
                  // console.log(domainClasses[classDomain].uri.split('/').pop())
                  if (serviceClass==domainClasses[classDomain].uri.split('/').pop()) {
                      serversArray[server][serviceClass]['matched'] = false
                      numMatchedClasses++
                  }
              })
              serversArray[server]['rank'] = numMatchedClasses
          })
      })
    })
    setTimeout(function(){
        if (hydraGraph.statements.length>0) {
            resolve(hydraGraph)
        } else {
            reject(Error('No Statement parsed! Time out!'))
        }
    },2000);
  });
}

/*
  Request search patterns
  Since search is not inside apiDoc
  We have to fetch it otherwise
  toUri()!!
 */
var requestSearchPattern = function(svrName,clName) {
  return new Promise(function(resolve,reject){
    var result = {};
    var url = svrName + "/" + clName.toUri();
    $http.get(url).then(function(response) {
      // console.log(response)
      if (response.data.hasOwnProperty("hydra:search")) {
        var searchObj = response.data["hydra:search"];
        searchObj["hydra:mapping"].forEach(function(prop,i){
          var propName = searchObj["hydra:mapping"][i]["property"];
          result[propName] = {};
          result[propName] = searchObj["hydra:mapping"][i]["variable"];
          // console.log("searchObj1",result[prop])
        });
        resolve(result);
      } else {
        resolve(result);
      }
    })//END GET
    // setTimeout(function(){    });
  });
}

var searchPromise = function(svrName,clName,data) {
  return new Promise(function(resolve,reject){
    var result = [];
    var url = svrName + "/" + clName.toUri() + "?";
    setTimeout(function(){
      $http.get(url).then(function(response) {
        if (response.data["hydra:search"]) {
          //we have search field lets see the patern
        } else {
          //no search
        }
            entryClasses[entrypoint] = {}
            var hydraApiClasses = (response.data);
            Object.keys(hydraApiClasses).forEach(function(api){
                if (!isContext.test(api)) {
                    entryClasses[entrypoint][api.capitalize()] = {}
                    entryClasses[entrypoint][api.capitalize()]['link'] = hydraApiClasses[api]
                }
            })
        })
  })
  })
}

//NOT USED


/*
Check the domainIncludes relation to specify if we can, the superClass
of the properties we have. This can help us determine the Class
we need for API discovery
@propList: Array of properties
@schema: The KB we check against
@return: the array of classes sorted descending
array[0] = {
    matched: 1
    uri: "http://schema.org/Flight"
    value: 4
}
From the given list of properties we try to figure out it's domain
and if we can match it with a schema.org model
    //gia ka8e property mazeuw ta domains pou to periexoun!
    // gia ka8e domain pou anhkei ena property
    // gia ka8e property diko mas kai tou domain aukshse tn counter
    // Ara exoume enan pinaka me tis koines emfaniseis
*/
function domainInc(propList,schema) {
   // console.log('properties of Class PostalAddress',schema.each(null,SCHEMA('rangeIncludes'),SCHEMA('PostalAddress')))
   //  console.log('Classes containing property ',schema.each(SCHEMA('address'),SCHEMA('rangeIncludes'),null))
   // console.log('subClassOf PostalAddress',schema.each(null,SCHEMA('domainIncludes'),SCHEMA('address')))
   //  console.log('Classes containing property ',schema.each(SCHEMA('address'),SCHEMA('domainIncludes'),null))
   retrieveInstances('PostalAddress',schema)
  var commons = [];
  var domains = {};
  var arrayClass = [];
  var len = Object.keys(propList).length;
  Object.keys(propList).forEach(function(property) {
    domains[property] = schema.each(SCHEMA(property),SCHEMA('domainIncludes'),null);
    domains[property].forEach(function(node){
        // console.log(node)
      if (!(typeof commons[node] == 'object')) {
        commons[node] = {};
        commons[node]['assigned'] = [];
        commons[node]['value'] = 1
      } else {
        commons[node]['value']++
      }
      commons[node]['ratio'] = (commons[node]['value']/len);
      commons[node]['assigned'].push(property);
    });
  });
  console.log("domainIcludes HyperClasses: ",commons);
  for (i=0;i<Object.keys(commons).length;i++) {
    arrayClass.push({
      uri: Object.keys(commons)[i],
      value: commons[Object.keys(commons)[i]]['value'],
      ratio: commons[Object.keys(commons)[i]]['ratio'],
      assigned: commons[Object.keys(commons)[i]]['assigned']
    });
  };
  arrayClass.sort(function(a, b) {
    return b.ratio - a.ratio;
  });
  // console.log("DomainInclludes Classes: with sauce",arrayClass)
  return arrayClass
};

/*
  Create clusters of orphans
  We have already found all the possible,
  classes that include our orphans!
  We want to find non overlapping classes!
  Better do recursive clustering
 */
function clusterOrphans(orphansArray,schema,data) {
  console.log('orphansArray:',orphansArray);
  var clusters = [];
  for (var i=0;i<orphansArray.length;i++) {// for every orphan
    var temp = {}
    temp["ratio"] = orphansArray[i].ratio;
    temp["classes"] = []
    temp["classObj"] = [];
    temp["classObj"].push(orphansArray[i]); //init cluster
    temp["classes"].push(orphansArray[i].uri); // add Classes URIs
    var assignedPropsI = orphansArray[i]["assigned"]; // fill with all the props we have already accumulated
    // console.log("assignedPropsI:", assignedPropsI)
    for (var k=0; k<orphansArray.length;k++) { //gia tis upoloipes props
      if (k==i) {
        continue;
      }
      var overlapProperty = []; //properties already
      var assignedPropsK = orphansArray[k]["assigned"];
      assignedPropsI.forEach(function(clName,j){
        if(assignedPropsK.indexOf(clName)>-1) {
          // console.log("Overlaping Class: ",clName);
          overlapProperty.push(clName);
        }
      })//END of cross-class check
      if (overlapProperty.length==0) {
        // console.log("URI to cluster",orphansArray[k]["uri"]);
        temp["classObj"].push(orphansArray[k]); //class-i  can go with class-[]
        assignedPropsI = assignedPropsI.concat(orphansArray[k]["assigned"]); // bale to asigned, an den einai??!!!
        temp["ratio"] += orphansArray[k].ratio;
        temp["classes"].push(orphansArray[k].uri);
      }
    }//END of k=i+1 classes iteration
    temp["classes"].sort();
    // console.log(temp);
    // console.log(clusters.length)
    var counter = 0;
    for (var k=0;k<clusters.length;k++) {
      // console.log(clusters[k]["classes"])
      // console.log(temp["classes"]);
      if (temp["classes"].toString()==clusters[k]["classes"].toString()) {
        counter++;
        // console.log("Found common cluster! Do not add.");
        // break;
      }
    } //END of k iteration
    if (counter==0) {
      clusters.push(temp);
    };
  };
  clusters.sort(function(a, b) {
      return b.ratio - a.ratio;
  });
  // We have clusters we need to decide what to use
  console.log("clusters of classes!:",clusters);
  return assignOrphans(clusters,data);
}

/*
Create data object from orphans and assign them class
 */
function assignOrphans(clusters,data) {
  // Select the first one without any thinking for now+
  var remainingOrphans = JSON.parse(JSON.stringify(data));
  var selectedCluster = clusters[0];
  var dataFixed = {};
  selectedCluster.classObj.forEach(function(cl,key){
    var propNames = selectedCluster['assigned'];
    var className = selectedCluster.classObj[key]['uri'].toClassName();
    remainingOrphans[className] = {};
    selectedCluster.classObj[key].assigned.forEach(function(propName,key){
      // var temp = data[propName];
      remainingOrphans[className][propName] = remainingOrphans[propName];
      delete remainingOrphans[propName];
    });
  });
  // Assign remaining props to main Class
  var mainClass = selectedCluster.classObj[0]['uri'].toClassName();
  // console.log("remaining data:",remainingOrphans);
  Object.keys(remainingOrphans).forEach(function(propName,key){
    if (remainingOrphans[propName].hasOwnProperty("value")) {
      remainingOrphans[mainClass][propName] = remainingOrphans[propName];
      delete remainingOrphans[propName];
    };
  });
  console.log("data:",data);
  console.log("Data after clustering assignments",remainingOrphans);
  console.log("Selected class for orphans!",clusters[0]['ratio']);
  return remainingOrphans;
}


/*
Get the ranked servers and start fetching data/resolving
 */
var isContext = new RegExp('^@');

/*
Procedure to find matching server classes
for orphan properties. Only at top level!
 */
function orphanProps(data) {
  var orphan = {};
  Object.keys(data).forEach(function(prop){
   if (!(isContext.test(prop))) {
    if(!(data[prop].hasOwnProperty("value"))) { //we have a simple 0lvl property
      orphan[prop] = data[prop];
    }
  }
  });
  return orphan;
}

/*
  Retrurn guessed classes for orphans
 */
function returnGuessed(data,schema) {
    var guessedClasses = domainInc(data,schema);
    return guessedClasses;
}


function callApi(svrName) {
  var test;
}

/*
Create Class hierarchy from input data
 */
  function classTier(data,depth) {
    var classN = Object.keys(data);
    var classRank = [];
    var propRank = [];
    var propTemp = [];
    var classTierObj = {};
    var props = {};
    var classArray;
    var temp = [];
    var levelData = [];
    var lvlTemp = [];
    console.log("input data for classTier:", data);
    classN.forEach(function(prop){
      if(data[prop].hasOwnProperty("value")) { //we have a simple N property
        props[prop] = {};
        props[prop] = data[prop].type;
        if (!(isDatatype(data[prop],datatypeList))) { //an exoume Class type
          propTemp.push(data[prop].type);
        }
        lvlTemp[prop] = data[prop];
        } else { // the N is a Class
          lvlTemp[prop] = data[prop];
          [props[prop],classRank,propRank,levelData] = classTier(data[prop],(depth+1));
          classRank[depth] = [];
          classRank[depth].push(prop);
        }
    });
    if (!(Array.isArray(propRank[depth]))) {
      propRank[depth] = propTemp;
    } else {
      propRank[depth] = propRank[depth].concat(propTemp);

    }if (!(Array.isArray(levelData[depth]))) {
      levelData[depth] = lvlTemp;
    } else {
      levelData[depth] = levelData[depth].concat(lvlTemp);
    }
    // console.log("Depth: classRank: propRank: ",depthOf(props) );
    // console.log("classRank: ",classRank);
    // console.log("propRank: ",propRank);
    // console.log("levelData",levelData)
    var out = [props,classRank,propRank,levelData];
    return out;
  }


/*
This is called on the servers Entrypoints
We could assign this procedure while fetching,
bu this allows as to have prefetched our servers,
or cached!
 */
/**
 * [prepareServers description]
 * @param  {[type]} servers [description]
 * @param  {[type]} data    [description]
 * @param  {[type]} schema  [description]
 * @return {[type]}         [description]
 */
function prepareServers(servers,classSelected,schema) {
  var serversDraft = [];
  var rankedClasses = [];
  Object.keys(servers).forEach(function(server){
    var temp = rankServer(classSelected,servers[server],rankedClasses,server)
    if (temp!=null) {
      serversDraft[server] = temp[0];
      rankedClasses = temp[1];
    }
  });
  //Rank based on overall
  serversDraft.sort(function (a, b) {
    return a.rank[0] - b.rank[0];
  });
  return [serversDraft,rankedClasses];
}

/*
  We assign a rank to a server based on what we have/want
*/
function rankServer(classSelected,server,rankedClasses,svrName) {
  console.log("server for ranking: ",server);
  var sum = 0;
  var maxDepth = 0;
  var offset = 1;
  Object.keys(classSelected).forEach(function(classArray,k){ //for every type of CLASS
    maxDepth = Math.max(maxDepth,Object.keys(classSelected[classArray]).length);
  });
  var rankArray = new Array(maxDepth+offset).fill(0);
  var serverClassEntries = Object.keys(server);
  // console.log("server's contained Classes: ",serverClassEntries)
  // console.log("maxDepth:",maxDepth);
  // console.log(serverClassEntries);
  Object.keys(classSelected).forEach(function(classArray,k){ //for every type of CLASS
    Object.keys(classSelected[classArray]).forEach(function(levelName,i){ //for every LEVEL
      // INIT Array if not
      if (!(Array.isArray(rankedClasses[i]))) {
        rankedClasses[i] = {};
      }
      classSelected[classArray][levelName].forEach(function(clName,j){ // for every CLASS
        if (!(Array.isArray(rankedClasses[i][clName]))) {
          rankedClasses[i][clName] = []; //rankedClasses array, no duplicates!!
        }
        if ((serverClassEntries.indexOf(clName)>-1)) {
          //GO FOR APIDOC
          if (apiDocStash[svrName]==undefined) {
            apiDocStash[svrName] = fetchApiDoc(svrName);
          }
          // console.log("levelName and ClassName: ",levelName + " " + clName);
          rankArray[i+offset]++;
          sum++;
          rankedClasses[i][clName].push(svrName); //rankedClasses array, no duplicates!!
        } else {
          // rankedClasses[i][clName] = null;
        }
      })
    })
  })
  // console.log(rankArray);
  // If none found return NULL
  if (sum==0) {
    return null
  } else {
    rankArray[0] = sum;
    server['rank'] = rankArray;
    return [server,rankedClasses];
  }
}

/*
  Assign servers to a class
  Take given class and it's subclasses as seen in properties.
 */
function servers2Class(classOne,servers,hierClass) {
  var matchingServers = [];
  var className = Object.keys(className);
  var reqClasses = [];
  reqClasses.push(className);
  Object.keys(hierClass[className]).forEach(function(property,i){
  });

}

/*
  Retrieve a similar data
 */

/*
  Resolve property-class
  If an URI is required but we have data, then try a search pattern
 */
function searchProperty(svrName,clName,data) {

}

/*
  Resolve a property
 */
function resolveProperty(){

}



/*
  Sometimes a referenced Class may not be at our server
  but a Class of higher hierarchy may be!
  So we have to pull out the 'container' class and mark it as similar
 */


/*
  Property check
 */
function prepareProperty(property) {

}
/*
  Resolve a class to it's servers.
  Define local best in order to continue on higher classes
  We have the class, it's properties! the servers to look for
 */
function resolveClass(clName,rCl,data,apiDocStash) {
  //Resolving one class means hitting many doors
  console.log("class for matching data:", data)
  console.log("Resolving class:",clName);
  // console.log("From servers: ", rCl);
  var svrArray = 1;
  // prepare class data
  var filledProps = 0;
  var emptyProps = 0;
  var linkPropsObj = {};
  var searchVars = {};
  var tbResolved = {};
  //resolve specific "metrics"
  var searchVarsNames = [];
  var canResolve = {};
  var matchedNames = {};
  var nonMatchedNames = {};
  var ratio = [];

  //search test
  var searchPatterns = requestSearchPattern("http://localhost:8080","PostalAddress");
  searchPatterns.then(function(searchObj){
    console.log("Search Test Object for PostalAddress",searchObj);
  });

  Object.keys(data).forEach(function(prop,i){
    console.log(isEmpty(data[prop]))
    if(isEmpty(data[prop])){
      emptyProps++;
      tbResolved[prop] = data[prop];
    } else { // property has value
        if (isDataOK(data[prop],datatypeList)) {
          searchVars[prop] = data[prop];
          searchVarsNames.push(prop);
        } else { //we have not datatype match
          if ((isDatatype(data[prop]))) { // it's datatype not link
            console.error("Type of data is different than the one given!");
          } else { //we have link and not URI
            //resolve search!!!! based on it's data!
          }//LINK check
        } //DATATYPE check
      } //Value check
  })
  if  (emptyProps==0) { //class is full we need to resolve with a link maybe
    // full class
  } else if (Object.keys(data).length==emptyProps){ //stayed empty...
    // class empty
  } else { //here goes the search function

  }
  console.log(emptyProps)
  console.log(searchVars)
  console.log(tbResolved)

  rCl.forEach(function(svrName,j){
    apiDocStash[svrName].then(function(svrData){
      var searchPatterns = requestSearchPattern(svrName,clName);
      searchPatterns.then(function(searchObj){
        console.log("searchObj",searchObj)
        if(svrData.length==0) {
          console.error("Failed to fetch from server:",svrName)
        } else {
          console.log("Server class",svrData);
          Object.keys(svrData[clName]).forEach(function(prop,i){
            console.log()
            if (Object.keys(data).indexOf(prop)>-1) {
              //we have match
              matchedNames[prop] = svrData[clName][prop];
              if (Object.keys(tbResolved).indexOf(prop)>-1) {
                canResolve[prop] = svrData[clName][prop];
              }
            } else {
              nonMatchedNames[prop] = svrData[clName][prop];
            }
          });
          ratio["overall"] = Object.keys(matchedNames).length/Object.keys(data).length;
          ratio["offeredTotal"] = Object.keys(matchedNames).length/Object.keys(svrData[clName]).length;
          ratio["resolveable"] = Object.keys(tbResolved).length/Object.keys(canResolve).length;
          ratio["other"] = Object.keys(matchedNames).length/Object.keys(svrData[clName]).length;
          console.log("Ratios for matching: ",ratio);
          console.log([matchedNames,svrData[clName],tbResolved,canResolve])
        }
      });
    });
    // console.log("Querying server:",svrName );
  }) //END of per server resolve
  return 0;
}

/*
  Resolve a class from server, based on criteria.
  We are requesting more information from the server,before proceding
 */
function prepareResolve(rankedClasses,classMain,levelData,apiDocStash,data) {

  var classResult;
  console.log("apiDocStash: ",apiDocStash);
  console.log("levelData: ",levelData);
  console.log("classMain",classMain)
  // Start resolving from the opposite direction, deeper to 0
  // for each level and each class
  for (var i=classMain.length-1;i>=0;i--) { //from classMain we get the names and levels of classes
    Object.keys(classMain[i]).forEach(function(clName,j){ //for every name that apperas in classMain per level
      var name = classMain[i][clName]; // otherwise returns 0..1
      if (  (rankedClasses[i][name])) { //check if it is inside the servers' log
        // console.log(rankedClasses[i][clName])
        console.log("Chekare plh8os server",Object.keys(rankedClasses[i][name]).length)
        classResult = resolveClass(name,rankedClasses[i][name],levelData[i][name],apiDocStash);
      } else {
        // console.log("No server to ask for class: ", name);
      }
    })
  }
}

/*
  Assign servers to each class!
  Create the likelyhood table,
 */
  //http://localhost:8080/contexts/Flight
  //http://localhost:8080/postal_addresses
  function rankClasses(serversArray,arrayOfClasses,data) {
    //for every property, we assume that we have only 1 class
    var propsLevels = arrayOfClasses[1];
    var classLevels = arrayOfClasses[0];
    var maxDepth = 0;
    var temp = [];
    var index = 0;
    var rankedClasses;
    for (var i=0;i<arrayOfClasses.length-1;i++) {
      if (arrayOfClasses[i].length>=arrayOfClasses[i+1].length) {
        overallLevels = arrayOfClasses[i];
        maxDepth = arrayOfClasses[i].length;
        index = i;
      }
    }
    var overallLevels = arrayOfClasses[index];

    for (var i=0;i<maxDepth;i++) {
      for( var k = 0;k<=maxDepth;k++) {
        if (k!=index) {
          overallLevels[i] = overallLevels[i].concat(arrayOfClasses[k][i]);
        }
      }
    }
    // remove undefined!
    overallLevels = overallLevels.filter(function(element){
      return element = element.filter(function(sndElem){
        return sndElem !==undefined;
      });
    });
    // console.log("overallLevels: ",overallLevels);
    //One array of arrays per level!
    //Create an array of Classes containing servers!
    for (var i=maxDepth-1;i>=0;i--) {
      overallLevels[i].forEach(function(clName,k){

      })
    }
    return 0;
  }

/*
Match classes based on the ranked class table
 */
function matchClasses(arrrangedPerClassSearch,arrayOfClasses) {
  var svrRanks = arrayOfClasses.cl.servers[server];
}

/*
  Data to depth levels
 */
function levelData(data) {
}
/*
Return the results from matching
 */
function returnData() {
}


// //We have to wait in order for the rdf.parse to finish, since it's not a promise??
// var hydraParsedPromise = function(hydraDoc,hydraGraph,hydraServer) {
//     return new Promise(function(resolve, reject) {
//         $rdf.parse(hydraDoc, hydraGraph, hydraServer,'application/ld+json')
//         setTimeout(function(){
//             if (hydraGraph.statements.length>0) {
//                 resolve(hydraGraph)
//             } else {
//                 reject(Error('No Statement parsed! Time out!'))
//             }
//         },2000);
//     });
// }

// fetchData().then(function(data) {
//   return prepareDataForCsv(data);
// }).then(function(data) {
//   return writeToCsv(data);
// }).then(function() {
//   console.log('your csv has been saved');
// });


var parsedHydraApis = function(entrypointList,hydraGraph) {
  return new Promise(function(resolve, reject) {
  var promiseArray = [];
  var counter = 1;
  entrypointList.forEach(function(entrypoint){
    var url = entrypoint + "/docs.jsonld";
    promiseArray[entrypoint] = fetchHydraApiz(entrypoint).then(function(jsonld) {
      return hydraParsedPromise(jsonld,hydraGraph,url);
    }).then(function(){
      console.log('parsed');
      counter++;
          if (counter==entrypointList.length) {
      resolve(hydraGraph)
    }
    });
    console.log(promiseArray);
      // console.log(hydraDoc);
      // return hydraParsedPromise(hydraDoc,hydraGraph,url).then(function(hydraGraph){
      //   console.log('Hydra KB');
      //   counter++;
      // });
    }) // end then-promise resolved
  // });
  // return new Promise(function(resolve, reject) {
    // if (counter==entrypointList.length) {
    //   resolve(hydraGraph)
    // }
     // return Promise.all(promiseArray)
    //  .then(function(){
    //   console.log('End of hydra APIs pardsing')
    //   resolve(hydraGraph);
    // });
    });
 }
// var parsedHydraApis = function(entrypointList,hydraGraph) {
//   return new Promise(function(resolve, reject) {
//   var promiseArray = [];
//   var counter = 1;
//   entrypointList.forEach(function(entrypoint){
//     var url = entrypoint + "/docs.jsonld";
//     promiseArray[entrypoint] = fetchHydraApiz(entrypoint).then(function(jsonld) {
//       return hydraParsedPromise(jsonld,hydraGraph,url);
//     }).then(function(){
//       console.log('parsed');
//       counter++;
//     });
//       // console.log(hydraDoc);
//       // return hydraParsedPromise(hydraDoc,hydraGraph,url).then(function(hydraGraph){
//       //   console.log('Hydra KB');
//       //   counter++;
//       // });
//     }) // end then-promise resolved
//   // });
//   // return new Promise(function(resolve, reject) {
//     Promise.all(promiseArray).then(function(){
//       console.log('End of hydra APIs pardsing')
//       resolve(hydraGraph);
//     });
//     });
// }

/**
 * Fetch Hydra servers' entrypoints and create an entity for them
 * ClassName: URI
 * @param  {[type]} entrypointList [description]
 * @return {[type]}                [description]
 */
var parseHydraApis = function(entrypointList,hydraGraph) {
    return new Promise(function(resolve, reject) {
        var counter = 1;
        var isContext = new RegExp('^@');
          entrypointList.forEach(function(entrypoint){
            var url = entrypoint + "/docs.jsonld";
            $http.get(url).then(function(response) {
              //Gia ka8e response
              var hydraDoc = response.data;
                // hydraDoc.toString().replace('#', 'http:\/\/localhost:8000\/');
                // console.log(hydraDoc)
                // Parse Hydra as JSON and modify it according to our needs!!!
                hydraObject = hydraDoc;//JSON.parse(hydraDoc);
                hydraObject["@context"]["@base"] = entrypoint;// "/apidoc"

              //   hydraObject['hydra:supportedClass'].forEach(function(supClass){
              //     supClass['hydra:supportedProperty'].forEach(function(obj){
              //       obj['@id'] = obj['hydra:property']['@id']+'/hydra'
              //       // console.log(obj['hydra:property']['@id'])
              //     })
              //     // console.log(supClass["hydra:supportedOperation"])
              //     if (Array.isArray(supClass["hydra:supportedOperation"])) {
              //       supClass["hydra:supportedOperation"].forEach(function(obj){
              //         obj['@id'] = supClass['@id'] + '/' + obj["hydra:method"]
              //           // console.log(obj['@id'])
              //       })
              //     }
              //     if (supClass['@id'].includes("Entrypoint")) {
              //       supClass['hydra:supportedOperation']['@id'] = supClass['@id'] + '/' + supClass['hydra:supportedOperation']['hydra:method']
              //       console.log(supClass['hydra:supportedProperty'])
              //       supClass['hydra:supportedProperty'].forEach(function(obj){
              //         // console.log(obj)
              //         obj['hydra:property']['hydra:supportedOperation'].forEach(function(propObj){
              //           propObj['@id'] = obj['@id'] + '/' + propObj['hydra:method']
              //         })
              //     })
              //   } //Entrypoint specific
              // }) // forEACH hydra:supportedClass
                // Ready JSONLD for RDF
                hydraDoc = JSON.stringify(hydraObject);
                // console.log(hydraDoc);

                return hydraParsedPromise(hydraDoc,hydraGraph,url).then(function(hydraGraph){
                  console.log('Hydra KB');
                  counter++;
                });
            }) // end then-promise resolved
        }) //forEACH server
        // setTimeout(function(){
            if (counter == entrypointList.length) {
                resolve(hydraGraph)
                console.log("All good")
            }
             else {
                resolve(hydraGraph)
                console.log('Fetching from servers timed out, some may not be included')
            }
        // },12000);
                  // resolve(hydraGraph);
    });
}

// parseHydraApis(entrypointList,hydraGraph).then


function resolveApis(servers,data,schema) {
  var orphan = orphanProps(data);
  console.log("Number of Orphans at 0 level:",Object.keys(orphan).length);
  var guessed = domainInc(orphan,schema);
  // var orphansDone = clusterOrphans(guessed,schema,data);

  // console.log("guessed Classes from Schema",guessed);
  // var [classObj,classMain,classProp,levelData] = classTier(data,0);
  var [classObj,classMain,classProp,levelData] = classTier(data,0);
  console.log("Class Hierarchy as Object",classObj);
  var classSelected = [classMain,classProp];
  console.log("Classes and Properties per level Arrays: ",classSelected);
  // Object.keys(servers).forEach(function(svrName){
  //   console.log(svrName)
  // })
  var [serversArray,rankedClasses] = prepareServers(servers,classSelected,schema) //exoume etoimo ranked tous servers
  console.log("serversArray",serversArray); //ready for resolving
  console.log("rankedClasses: ",rankedClasses)
  var classesArray = rankClasses(serversArray,classSelected,data);
  var test = prepareResolve(rankedClasses,classMain,levelData,apiDocStash,data);

  return 0;
}

/*
Constants and arguments
 */
var entrypointList = ['http://localhost:8083','http://localhost:8082','http://localhost:8081'];
var hydraObject;
var hydraGraph = $rdf.graph();
var hydraTEST = $rdf.graph();
// console.log(hydraGraph);
var hydraDoc = {};
var hydraServerUri = 'http://localhost:8080/';
var hydraServer = 'http://localhost:8080/apidoc';
var hydraPromise = jsonld.documentLoader(hydraServer);
var schemaKB = $rdf.graph();
// var schemaOrgServer = 'https://raw.githubusercontent.com/schemaorg/schemaorg/sdo-gozer/data/schema.rdfa';
var schemaOrgServer = 'http://localhost:9000/schema_3_2.rdfa';
var serversList = fetchHydraApis(entrypointList);
// console.log('Hydra servers Entypoins',serversList);

var apiDocStash = []; //apiDocStash.push(svr);


var onResult = function(result) {
        //console.log("TEST RESULT:");
        var str = "\tTEST Result: ";
        console.log(result)
        for (var v in result) {
            str += "   "+v+'->'+result[v];
        }
        console.log(str);
    };

    var onDone = function() {
        console.log("\tTEST DONE -- final callback")
    }

/*
  INIT process of resolving
  Retrieve SCHEMA KnowledgeBase Data and start API-Discovery and resolve
 */
  var timeStartFetch = performance.now();
  $http.get(schemaOrgServer).then(function(response) {
    // var schemaDoc = response.data
    schemaParsedPromise(response.data,schemaKB,schemaOrgServer).then(function(schemaGraph){
      var timeReadySchema = performance.now();
      console.log('Time took to fetch & parse schemaorg: ' + (timeReadySchema-timeStartFetch) + ' ms')
      console.log('KB from SCHEMA',schemaKB);
      // RDF TESTS
      // rdfExp(schemaKB);
     var hydraStart = performance.now();

     parsedHydraApis(entrypointList,hydraGraph).then(function(resa){
    var hydraEnd = performance.now();
            console.log('timeAllHydra: ' + (hydraEnd-hydraStart) + ' ms');

        // console.log(tst);
        console.log(hydraGraph);
      // });
      // parseHydraApis(entrypointList,hydraTEST).then(function(hydraRDF){
      //   console.log(hydraRDF);
      //   console.log(hydraTEST);

      // hydraTEST = mergeBlankNodes(hydraTEST)
      hydraTEST = hydraGraph
      var timeReadyHydra = performance.now();
      // console.log('timeReadyHydra: ' + (timeReadyHydra-timeStartHydraParse) + ' ms');
      // visualiseGraph(hydraTEST,g);
      // var hydraProperty = hydraTEST.each(null,HYDRA('property'),null);
      // console.log('hydraProperty',hydraProperty)
      // var bn = hydraTEST.each(null,HYDRA('property'),null)
      // var bn2 = hydraTEST.each(hydraProperty[1],HYDRA('supportedProperty'), null)
      // var bn3 = hydraTEST.each(hydraProperty[2],null, null)
      // console.log(bn)
      // console.log(bn2)
      // console.log(bn3)
      // console.log(bn[0].compareTerm(bn[0]))
      // var bn_ref = hydraTEST.each(null,null,"_:n1" )
      // console.log(bn_ref)
      var prefixSPARQL = 'PREFIX  schema: <http://schema.org/> \n \
                    PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n \
                    PREXIF  rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n \
                    PREFIX  foaf: <http://xmlns.com/foaf/0.1/> \n \
                    PREFIX  hydra: <http://www.w3.org/ns/hydra/core#> \n \
                    PREFIX  xsd: <http://www.w3.org/2001/XMLSchema#> \n ';

//SPARQL QUERIES!!!!!!
// //
// var classL = 'Airport';
// var asd=' "test" ';
// console.log("PAISKEEEEEE: ",asd);

// //               FILTER(?who = "10"). \ doulepse san constraint
// //              FILTER ( regexp(?who,"bbb") ). \
//               ?blankProp  hydra:title ?troll .\
//               ?blanks hydra:SupportedProperty ?blankProp .\


// var sparQ2 = prefixSPARQL +
//             'SELECT * \ ' +
//             'WHERE { \
//               ?blanks rdfs:domain <http://localhost:8083#Entrypoint> . \
//               ?test ?pred ?blanks. \
//               ?test hydra:title ?ttitle . \
//               ?test hydra:readable ?readAB . \
//             } ';

// var sparQ2 = prefixSPARQL +
//             'SELECT * \ ' +
//             'WHERE { \
//               ?blanks rdfs:domain <http://localhost:8083#Entrypoint> . \
//               ?test ?pred ?blanks. \
//               ?test hydra:property ?ttitle . \
//               ?ttitle rdfs:label ?readAB . \
//             } ';

            // var sparQ2 = prefixSPARQL +
            // 'SELECT * \ ' +
            // 'WHERE { \
            //   ?blanks rdfs:domain <http://localhost:8083#Entrypoint> . \
            //   ?test ?pred ?blanks. \
            //   ?test hydra:property ?ttitle . \
            //   ?ttitle rdfs:label ?readAB . \
            // } ';
            //
            // We get all the classes + entrypoints + constraints
            // var sparQ2 = prefixSPARQL +
            // 'SELECT * \ ' +
            // 'WHERE { \
            //   ?p hydra:supportedClass schema:GeoCoordinates  . \
            // } ';
            var sparQ2 = prefixSPARQL +
            'SELECT * \ ' +
            'WHERE { \
              schema:Place hydra:supportedProperty ?te  . \
              ?te hydra:property  <http://localhost:8083#Place/name> .\
              ?te hydra:title ?propName. \
            } ';

// var sparQ2 = prefixSPARQL +
//             'SELECT * \ ' +
//             'WHERE { \
//               ?blanks rdfs:domain <http://localhost:8083#Entrypoint> . \
//               ?test ?pred ?blanks. \
//               ?test hydra:property ?ttitle . \
//               ?ttitle rdfs:label ?readAB . \
//             } ';

// var sparQ22 = prefixSPARQL +
//             'SELECT ?test \ ' +
//             'WHERE { \
//               schema:Airport hydra:supportedProperty ?test . \
//               }';

// //               FILTER REGEXP("bbb",?who) . \

// //               FILTER regex(?test, \"^localhost\"). \

// // var queryFromSPQ = $rdf.SPARQLToQuery(sparQ,true,hydraTEST)
var queryFromSPQ = $rdf.SPARQLToQuery(sparQ2,true,hydraTEST)

//       var subject = $rdf.variable('subject');
//       var object = $rdf.variable('object');

//       var qHydra = new $rdf.Query('test',3);
//       qHydra.pat.add(subject,SCHEMA('domainIncludes'),SCHEMA('Airport'))
//       qHydra.pat.constraints['?subject ="aaa"'];// add(subject,HYDRA('title'),object)
//       console.log(qHydra)
//       var qSchema = new $rdf.Query('test',3);

//       hydraTEST.add(10,SCHEMA('domainIncludes'),SCHEMA('Airport')); //to phre
//       hydraTEST.add('aaa',SCHEMA('domainIncludes'),SCHEMA('Airport')); //to phre
//       hydraTEST.add('bbb',SCHEMA('domainIncludes'),SCHEMA('Airport')); //to phre
//       hydraTEST.add('bbb',HYDRA('title'),SCHEMA('Airport')); //to phre
//       hydraTEST.add(SCHEMA('Skata'),SCHEMA('domainIncludes'),SCHEMA('Airport')); //to phre
//       hydraTEST.add(SCHEMA('SkOXIII'),SCHEMA('domainIncludes'),SCHEMA('Airport')); //to phre
//       hydraTEST.add(SCHEMA('Skata'),HYDRA('title'),SCHEMA('LOLEN')); //to phre
//       // console.log(hydraTEST.each(null,HYDRA('domainIncludes'), SCHEMA('Airport')))

//         qSchema.pat.add(subject,SCHEMA('domainIncludes'),SCHEMA('Airport')); //douleuei
//         qSchema.pat.constraint = ['subject','>','10']; //douleuei
//         schemaKB.query(qSchema, onResult, undefined, onDone);
//         hydraTEST.query(qHydra, onResult, undefined, onDone);
        // hydraTEST.query(queryFromSPQ, onResult, undefined, onDone);
//         console.log(hydraTEST);

        var timeEachHydra = performance.now();
        console.log('timeEachHydra: ' + (timeEachHydra-timeReadyHydra) + ' ms');
        // console.log(hydraProperty);
      }) //hydraPRASE   
      serversList.then(function(entrypointArray){
      // console.log("entrypointArray",entrypointArray["http://localhost:8080"])
      resolveApis(entrypointArray,data,schemaKB);
      //   resolveApis(entrypointArray,testData,schemaKB);
      });
    }); // END of schemaOrg Parse!
  }, //END of schemaOrg Fetching
  function errorCallback(response) {
    alert('Failed to fetch schema.org');
  });// SCHEMA fetch to rdf graph


}]); // Controller END