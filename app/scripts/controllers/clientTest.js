// app/scripts/controllers/angClient.js
'use strict';
// Client for our testing
// Automated API discovery based on schemantics and vocabularies


blogApp.controller('ReasonerCtrl', ['$log', '$scope','schydraAngular','$http', '$q', function ($log,$scope,schydraAngular, $http, $q) {

  var rawJSON = {
    "@context" : {
      "expects" : {
        "@id" : "http://www.w3.org/ns/hydra/core#expects",
        "@type" : "@id"
      },
      "returns" : {
        "@id" : "http://www.w3.org/ns/hydra/core#returns",
        "@type" : "@id"
      },
      "method" : {
        "@id" : "http://www.w3.org/ns/hydra/core#method"
      },
      "title" : {
        "@id" : "http://www.w3.org/ns/hydra/core#title"
      },
      "label" : {
        "@id" : "http://www.w3.org/2000/01/rdf-schema#label"
      },
      "writable" : {
        "@id" : "http://www.w3.org/ns/hydra/core#writable",
        "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
      },
      "required" : {
        "@id" : "http://www.w3.org/ns/hydra/core#required",
        "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
      },
      "readable" : {
        "@id" : "http://www.w3.org/ns/hydra/core#readable",
        "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
      },
      "property" : {
        "@id" : "http://www.w3.org/ns/hydra/core#property",
        "@type" : "@id"
      },
      "object": {
        "@id": "schema:object",
        "@type": "@id"
      },
      "result": {
        "@id": "schema:result",
        "@type": "@id"
      },
      "target": {
        "@id": "schema:target",
        "@type": "@id"
      },
      "query": {
        "@id": "schema:query"
      },
      "description" : {
        "@id" : "http://www.w3.org/ns/hydra/core#description"
      },
      "supportedProperty" : {
        "@id" : "http://www.w3.org/ns/hydra/core#supportedProperty",
        "@type" : "@id"
      },
      "supportedOperation" : {
        "@id" : "http://www.w3.org/ns/hydra/core#supportedOperation",
        "@type" : "@id"
      },
      "@vocab" : "http://schema.org/",
      "local" : "http://localhost/",
      "schema" : "http://schema.org/",
      "hydra" : "http://www.w3.org/ns/hydra/core#",
      "xmls" : "http://www.w3.org/2001/XMLSchema#",
      "owl" : "http://www.w3.org/2002/07/owl#",
      "rdf" : "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      "xsd" : "http://www.w3.org/2001/XMLSchema#",
      "rdfs" : "http://www.w3.org/2000/01/rdf-schema#",
    },
    "offer": {
      "@id": "local:Offer",
      "@type": "Offer",
      "priceCurrency": '',
      "price": '',
      "seller": '',
      "itemOffered": { "@type": "Flight" },
      "potentialAction": {
        "@type": "searchAction",
        "query": {"@type": "schema:Flight"},
        "object": "schema:Offer",
        "result": "schema:Offer",
        "target": "schema:Offer"
      }
    },
    "flight": {
      "@id": "local:Flight",
      "@type": "Flight",
      "arrivalAirport": {"@type": "Airport"},
      "departureAirport": {"@type": "Airport"},
      "departureTime": {"@type": "xmls:dateTime"},
      "arrivalTime": {"@type": "xmls:dateTime"},
      "provider": ""
    },
    "postalAdress": {
      "@id": "local:postalAdress",
      "@type": "PostalAddress",
      "addressCountry": '',
      "addressLocality": ''
    },
    "airport": {
      "@id": "local:Airport",
      "@type": "Airport",
      "iataCode": '',
      "address": { "@type": "PostalAddress" },
      "geo": {"@type": "GeoCoordinates"},
      "potentialAction": {
        "@type": "searchAction",
        "object": "schema:Airport",
        "result": "schema:Airport",
        "target": "schema:Airport",
        "query": {"@type": "schema:PostalAddress"}
      }
    },
    "poi": {
      "@id": "local:TouristAttraction",
      "@type": "TouristAttraction",
      "name": "",
      "description": "",
      "url": "",
      "hasMap": "",
      "geo": {"@type": "GeoCoordinates"},
      "image": "",
      "potentialAction": {
        "@type": "searchAction",
        "object": "schema:TouristAttraction",
        "result": "schema:TouristAttraction",
        "target": "schema:TouristAttraction",
        "query": {"@type": "schema:GeoCoordinates"}
      }
    },
    "geoloc": {
      "@id": "local:GeoCoordinates",
      "@type": "GeoCoordinates",
      "latitude": "",
      "longitude": ""
    },
    "hotels": {
      "@id": "local:Hotel",
      "@type": "Hotel",
      "name": "",
      "description": "",
      "address": {"@type":"schema:PostalAddress"},
      "faxNumber": "",
      "telephone": "",
      "geo": {"@type": "schema:GeoCoordinates"},
      "potentialAction": {
        "@type": "searchAction",
        "object": "schema:Hotel",
        "result": "schema:Hotel",
        "target": "schema:Hotel",
        "query": {"@type": "schema:GeoCoordinates"}
      }
    }
  };

  // var rawJSON = {
  //   "@context" : {
  //     "expects" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#expects",
  //       "@type" : "@id"
  //     },
  //     "returns" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#returns",
  //       "@type" : "@id"
  //     },
  //     "method" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#method"
  //     },
  //     "title" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#title"
  //     },
  //     "label" : {
  //       "@id" : "http://www.w3.org/2000/01/rdf-schema#label"
  //     },
  //     "writable" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#writable",
  //       "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
  //     },
  //     "required" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#required",
  //       "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
  //     },
  //     "readable" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#readable",
  //       "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
  //     },
  //     "property" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#property",
  //       "@type" : "@id"
  //     },
  //     "object": {
  //       "@id": "schema:object",
  //       "@type": "@id"
  //     },
  //     "result": {
  //       "@id": "schema:result",
  //       "@type": "@id"
  //     },
  //     "target": {
  //       "@id": "schema:target",
  //       "@type": "@id"
  //     },
  //     "query": {
  //       "@id": "schema:query"
  //     },
  //     "description" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#description"
  //     },
  //     "supportedProperty" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#supportedProperty",
  //       "@type" : "@id"
  //     },
  //     "supportedOperation" : {
  //       "@id" : "http://www.w3.org/ns/hydra/core#supportedOperation",
  //       "@type" : "@id"
  //     },
  //     "@vocab" : "http://schema.org/",
  //     "schema" : "http://schema.org/",
  //     "hydra" : "http://www.w3.org/ns/hydra/core#",
  //     "xmls" : "http://www.w3.org/2001/XMLSchema#",
  //     "owl" : "http://www.w3.org/2002/07/owl#",
  //     "rdf" : "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
  //     "xsd" : "http://www.w3.org/2001/XMLSchema#",
  //     "rdfs" : "http://www.w3.org/2000/01/rdf-schema#",
  //   },
  //   "offer": {
  //     "@id": "Offer",
  //     "@type": "Offer",
  //     "priceCurrency": '',
  //     "price": '',
  //     "seller": '',
  //     "itemOffered": { "@type": "Flight" },
  //     "potentialAction": {
  //       "@type": "searchAction",
  //       "query": {"@type": "schema:Flight"},
  //       "object": "schema:Offer",
  //       "result": "schema:Offer",
  //       "target": "schema:Offer"
  //     }
  //   },
  //   "flight": {
  //     "@id": "Flight",
  //     "@type": "Flight",
  //     "arrivalAirport": {"@type": "Airport"},
  //     "departureAirport": {"@type": "Airport"},
  //     "departureTime": {"@type": "xmls:dateTime"},
  //     "arrivalTime": {"@type": "xmls:dateTime"},
  //     "provider": ""
  //   },
  //   "postalAdress": {
  //     "@id": "postalAdress",
  //     "@type": "PostalAddress",
  //     "addressCountry": '',
  //     "addressLocality": ''
  //   },
  //   "airport": {
  //     "@id": "Airport",
  //     "@type": "Airport",
  //     "iataCode": '',
  //     "address": { "@type": "PostalAddress" },
  //     "geo": {"@type": "GeoCoordinates"},
  //     "potentialAction": {
  //       "@type": "searchAction",
  //       "object": "schema:Airport",
  //       "result": "schema:Airport",
  //       "target": "schema:Airport",
  //       "query": {"@type": "schema:PostalAddress"}
  //     }
  //   },
  //   "poi": {
  //     "@id": "TouristAttraction",
  //     "@type": "TouristAttraction",
  //     "name": "",
  //     "description": "",
  //     "url": "",
  //     "hasMap": "",
  //     "geo": {"@type": "GeoCoordinates"},
  //     "image": "",
  //     "potentialAction": {
  //       "@type": "searchAction",
  //       "object": "schema:TouristAttraction",
  //       "result": "schema:TouristAttraction",
  //       "target": "schema:TouristAttraction",
  //       "query": {"@type": "schema:GeoCoordinates"}
  //     }
  //   },
  //   "geoloc": {
  //     "@id": "GeoCoordinates",
  //     "@type": "GeoCoordinates",
  //     "latitude": "",
  //     "longitude": ""
  //   },
  //   "hotels": {
  //     "@id": "Hotel",
  //     "@type": "Hotel",
  //     "name": "",
  //     "description": "",
  //     "address": {"@type":"schema:PostalAddress"},
  //     "faxNumber": "",
  //     "telephone": "",
  //     "geo": {"@type": "schema:GeoCoordinates"},
  //     "potentialAction": {
  //       "@type": "searchAction",
  //       "object": "schema:Hotel",
  //       "result": "schema:Hotel",
  //       "target": "schema:Hotel",
  //       "query": {"@type": "schema:GeoCoordinates"}
  //     }
  //   }
  // };

/*
    "hotels": {
      "@type": "Hotel",
      "name": "",
      "description": "",
      "address": {"@type":"schema:PostalAddress"},
      "faxNumber": "",
      "telephone": "",
      "geo": {"@type": "schema:GeoCoordinates"},
      "starRating": {"@type": "schema:starRating"},
      "makesOffer": "",
      "priceRange": {"@type": "schema:PriceSpecification"},
      "award": "",
      "potentialAction": {
        "@type": "searchAction",
        "object": "schema:Hotel",
        "result": "schema:Hotel",
        "target": "schema:Hotel",
        "query": {"@type": "schema:GeoCoordinates"}
      }
    }
 */;
  console.log(schydraAngular);

// // main
  $scope.log = $log;
  var resolverServer = 'http://vps362714.ovh.net:8091/api_ref/special';
  // var resolverServer = 'http://0.0.0.0:8091/api_ref/special';
  // var entrypointList = ['http://localhost:8091'];
  var graph = $rdf.graph();
  var today = new Date();
  var schydra = schydraAngular.init(rawJSON,resolverServer,graph);
  schydra.then(function(hydraClass){
    $scope.log = schydraAngular.log;
    $scope.error = schydraAngular.error;

//Create User/Depart GeoLocation
    var location = new hydraClass['GeoCoordinates']();
    location.label = "Current Location";
    location.set('latitude','Latitude','label');
    location.set('longitude','Longitude','label');
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function(position) {
        location.set('latitude',position.coords.latitude);
        location.set('longitude',position.coords.longitude);
      });
    } else {
      location.set('latitude',"");
      location.set('longitude', "");
    }
//Create User/Depart GeoLocation
    var destinationGeo = new hydraClass['GeoCoordinates']();
    destinationGeo.label = "Destination Location";
    destinationGeo.set('latitude','Latitude','label');
    destinationGeo.set('longitude','Longitude','label');
    destinationGeo.set('latitude','');
    destinationGeo.set('longitude','');
    // console.log('User/Departure Location',location);

//Search Address
    var searchAddress = new hydraClass['PostalAddress'];
    searchAddress.label = 'Departure address';
    searchAddress.set('addressCountry','GR');
    searchAddress.set('addressLocality','chio');
    searchAddress.set('addressCountry','Country Code','label');
    searchAddress.set('addressLocality','City','label');
    searchAddress.set('addressCountry','GR');

//search Airport
    var searchAirport = new hydraClass['Airport']();
    searchAirport.label = 'Search Airport';
    searchAirport.set('iataCode','');
    searchAirport.set('iataCode','IATA code','label');
    searchAirport.set('address',searchAddress);
    searchAirport.set('geo',location);

//Departure Address
    var departureAddress = new hydraClass['PostalAddress'];
    departureAddress.label = 'Departure address';
    departureAddress.set('addressCountry','GR');
    departureAddress.set('addressLocality','ath');
    departureAddress.set('addressCountry','Country Code','label');
    departureAddress.set('addressLocality','City','label');
    departureAddress.set('addressCountry','GR');

//Departure Airport
    var departureAirport = new hydraClass['Airport']();
    departureAirport.label = 'Departure Airport';
    departureAirport.set('iataCode','NYC');
    departureAirport.set('iataCode','IATA code','label');
    departureAirport.set('address',departureAddress);
    departureAirport.set('geo',location);
    // console.log('departureAirport:',departureAirport);

//Arrival Address
    var arrivalAddress = new hydraClass['PostalAddress']();
    arrivalAddress.label = 'destination address';
    arrivalAddress.set('addressCountry','Country Code','label');
    arrivalAddress.set('addressLocality','City','label');

//Arrival Airport
    var arrivalAirport = new hydraClass['Airport']();
    arrivalAirport.label = 'Arrival Airport';
    arrivalAirport.set('iataCode','LAX');
    arrivalAirport.set('iataCode','IATA code','label');
    arrivalAirport.set('address',arrivalAddress);
    // arrivalAirport.set('geo',location);
    // console.log('arrivalAirport',arrivalAirport);

    var pois = new hydraClass['TouristAttraction']();
    pois.label = 'Points of Interest';
    pois.set('geo',destinationGeo);// = destinationGeo.value;

    var hotels = new hydraClass['Hotel'];
    hotels.label = 'Available hotels in the area';
    hotels.set('geo',destinationGeo);// destinationGeo.value;
    // console.log('hotels',hotels);

    $scope.retrievedPois = {};
    $scope.fetchPois = function() {
      pois.actions.searchAction(graph,'').then(function(response){
          $scope.retrievedPois = response;
        });
    };
//Sun Apr 09 2017 02:03:00 GMT+0300 (EEST)

//Helper function to return next week!
    // function nextweek(){
      // var today = new Date();
      // var nextweek = new Date(today.getFullYear(), today.getMonth(), today.getDate()+7);
    //   return nextweek;
    // }
      $scope.selectedDeparture = {};
        // console.log(arrivalAirport.actions.searchAction(graph,''));

//Flight
    var searchFlight = new hydraClass['Flight']();
    searchFlight.label = 'Flight query';
    searchFlight.set('departureTime',new Date(today.getFullYear(), today.getMonth(), today.getDate()));
    searchFlight.set('departureTime','Departure Date ','label');
    searchFlight.set('arrivalTime',new Date(today.getFullYear(), today.getMonth(), today.getDate()+7));
    searchFlight.set('arrivalTime','Arrival Date ','label');
    console.log('date test',searchFlight.value.arrivalTime.get());
    searchFlight.set('departureAirport',departureAirport);// = departureAirport.value;
    searchFlight.set('departureAirport','Departure Airport ','label');
    searchFlight.set('arrivalAirport',arrivalAirport);// arrivalAirport.value;
    searchFlight.set('arrivalAirport','Arrival Airport ','label');
    // console.log('searchFlight',searchFlight);

    var flightOffers = new hydraClass['Offer'];
    flightOffers.value.itemOffered.value = searchFlight.value;
    flightOffers.label = 'flight search query';

      $scope.searchAddress = searchAddress;
      $scope.searchFlight = searchFlight;
      $scope.departureAirport = departureAirport;
      $scope.arrivalAirport = arrivalAirport;
      $scope.input = arrivalAirport;
      $scope.offer = flightOffers;
      $scope.retrievedAirport = {};
      $scope.retrievedOffers = [];
      $scope.retrievedHotels = [];
      $scope.retrievedPois = [];

//Data Counters
      $scope.offersLength = 0;
      $scope.hotelsLength = 0;
      $scope.poisLength = 0;

      $scope.fetchOffers = function() {
        // Fetch Flight Offers
        $scope.offer.actions.searchAction(graph,'').then(function(response){
          console.log('response',response);
          Object.keys(response).forEach(function(obj,i){
            $scope.retrievedOffers[i] = assign(response[obj],hydraClass);
          });
          $scope.offersLength = $scope.retrievedOffers.length;
          console.log('FORMATED DATA:',$scope.retrievedOffers);
        });
        //Fetch POIs
        pois.actions.searchAction(graph,'').then(function(response){
          Object.keys(response).forEach(function(obj,i){
            // console.log(response[0][obj]);
            $scope.retrievedPois[i] = assign(response[obj],hydraClass);
          });
          console.log('POIS RETR',$scope.retrievedPois);
          $scope.poisLength = $scope.retrievedPois.length;
          // $scope.retrievedPois = response;
        });
        //Fetch Hotels
        hotels.actions.searchAction(graph,'').then(function(response){
          Object.keys(response).forEach(function(obj,i){
            $scope.retrievedHotels[i] = assign(response[obj],hydraClass);
          });
          console.log('HOTELS RETR',$scope.retrievedHotels);
          $scope.hotelsLength = $scope.retrievedHotels.length;
        });
        console.log('PLH8OS',$scope.retrievedHotels.length);

        // $scope.retrievedAirport = response['data']['hydra:member'];
        console.log('w8 for Offers! w8!');
              console.log($scope.departureAirport);

      };


      function isContext(str) {
        var reg = new RegExp('^@');
        return reg.test(str);
      }


      var assign = function(obj,model,returnObject) {
        var classType = obj['@type'].split('/').pop();
        var returnObj;
        if((returnObject === null)||(typeof returnObject === "undefined")||(returnObject === '')) {
          if (model[classType]) {
            returnObj = new model[classType];
          } else {
            return null;
          }
        } else {
          returnObj = returnObject;
        }
        // console.log('EXW obj:',obj);
        returnObj['@id'] = obj['@id'];
        Object.keys(obj).forEach(function(prop){
          if (!(isContext(prop))) {
             // returnObj.value[prop] = {};
            if (( typeof obj[prop] === "object")&&( obj[prop])) {
              // console.log('Exw nested', prop);
              returnObj.set(prop,assign(obj[prop],model));
            } else {
              returnObj.set(prop,obj[prop]);
            }
          }
        });
        return returnObj;
      };

      //AutoComplete
      $scope.assignDepartureAirport = function(data) {
        if(data.hasOwnProperty('originalObject')) {
          $scope.departureAirport = assign(data.originalObject,hydraClass,$scope.departureAirport);
          $scope.searchFlight.set('departureAirport',$scope.departureAirport);
        }
      };

      $scope.assignArrivalAirport = function(data) {
        if(data.hasOwnProperty('originalObject')) {
          $scope.arrivalAirport = assign(data.originalObject,hydraClass,$scope.ArrivalAirport);
          $scope.searchFlight.set('arrivalAirport',$scope.arrivalAirport);
          pois.set('geo',$scope.arrivalAirport.get('geo'));
          hotels.set('geo',$scope.arrivalAirport.get('geo'));
        }
      };

      $scope.localSearch = function(str) {
        var matches = [];
        var results = $q.defer();
        searchAddress.set('addressLocality',str);
        console.log(searchAddress.value);
        searchAirport.set('address',searchAddress);
          searchAirport.actions.searchAction(graph,'').then(function(response){
            console.log('autocomplete Response:',response);
            response.forEach(function(obj,i){
              console.log('IATA:',response[i].iataCode);
              // var temp = JSON.parse(JSON.stringify(assign(response[i],hydraClass)));
              matches.push(response[i]);
            });
            // console.log(matches);
            results.resolve(matches);
          });
        return results.promise;
      };

      console.log($scope.input);
      // console.log($log);
  });//end of init>?!?!

  console.log('SCHYDRA Promise, Classes',schydra);

  }]);//END of anjular Controller
