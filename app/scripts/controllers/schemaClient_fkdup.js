// app/scripts/controllers/schemaClient.js
'use strict';
// Client for our testing
// Automated API discovery based on schemantics and vocabularies

// blogApp.factory('schydraAngular', ['$scope','Restangular','$http', '$q', function ($scope, Restangular, $http, $q) {
blogApp.factory('schydraAngular', ['$log','$http', '$q', function ($log,$http, $q) {

  var schydraAngular = {};
  var hydraDocument = '';
  var log = 'Initiated\n';
  var error = '';
  var RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
  var RDFS = $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
  var FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
  var XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
  var SCHEMA = $rdf.Namespace("http://schema.org/");
  var HYDRA = $rdf.Namespace("http://www.w3.org/ns/hydra/core#");

/*

 */
String.prototype.toClassNameSimple = function() {
  var str = this;
  str = str.split('/').pop();
  return str;
}

/*

 */
String.prototype.toClassNameGraph = function() {
  var str = this;
  str = str.split('#').pop();
  return str;
}

/**
 * [This is a test function to get the UIRs from schema classes
 * We should use https://github.com/tc39/proposal-intl-plural-rules
 * but for now is ok]
 * @return {[type]} [description]
 */
String.prototype.toUri = function() {
    var str = this
    var checkEnd = new RegExp(/[s]$/g)
    str = str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1_').toLowerCase();
    if (checkEnd.test(str)) {
        str = str + 'es';
        return str;
    } else {
        str = str + 's';
        return str;
    }
}

String.prototype.toClassName = function() {
  var str = this
  // console.log(this);
  // var checkEnd = new RegExp(/[s]$/g)
  str = str.split('/').pop();
  return str.replace('>','');
}

String.prototype.getBase = function() {
  var str = this;
  var parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
  var parts = parse_url.exec( str );
  // console.log('Url BASE',parts);
  var result = parts[1]+':'+parts[2]+parts[3];
  result = parts[4] ? result+':'+parts[4] : result;
  return result;
};

String.prototype.getBaseSimple = function() {
  var str = this;
  var parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
  var parts = parse_url.exec( str );
  // console.log('Url BASE',parts);
  var result = parts[3];
  result = parts[4] ? result+':'+parts[4] : result;
  return result;
};

String.prototype.splitURI = function() {
  var str = this;
  var parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
  var parts = parse_url.exec( str );
  return parts;
};

function isSchema(property,schema) {
    var schemaUri = SCHEMA(property);
    var tempNum = schema.whether(schemaUri,null,null);
    tempNum = tempNum + schema.whether(null,null,schemaUri);
    if (tempNum===0){
        return false;
    } else {
        return true;
    }
}

// var isContext = new RegExp('^@');
  function isContext(str) {
    var reg = new RegExp('^@');
    return reg.test(str);
  }

/*
 Split empty properties
 Check the value field
 */
function isEmpty(property) {
  if (property===undefined||property===""||property===null) {
    // console.log('property value:',property);
    return true;
  } else {
    return false;
  }
}

/**
 * [isComplete description]
 * @param  {[Object]}  classObj [description]
 * @return {Boolean}          [description]
 */
  function isComplete(classObj) {
    // console.log(classObj);
    return Object.keys(classObj).every(function(prop,k){
      return (isEmpty(classObj[prop]));
    });
  }

  /**
 * [isEmptyClass description]
 * @param  {[Object]}  classObj [description]
 * @return {Boolean}          [description]
 */
  function isEmptyClass(classObj) {
    return Object.keys(classObj).some(function(prop,k){
      if (!(isContext(prop))) {
        if(!(isEmpty(classObj[prop]))) {
          return false;
        }
      }
    });
  }

// var datatypeList = ["xmls:dateTime","xmls:integer",'date','datetime','boolean','number','integer','float','text','string','time'];

// function isDatatype(property,datatypeList) {
//     if (datatypeList.indexOf(property.type)>-1){
//         return true;
//     } else {
//         return false;
//     }
// }

/*
  Return depth of object hierarchy
 */
function depthOf(object) {
  var level = 1;
  var key;
  for(key in object) {
    if (!object.hasOwnProperty(key)) {continue;}
    if(typeof object[key] === 'object') {
      var depth = depthOf(object[key]) + 1;
      level = Math.max(depth, level);
    }
  }
  return level;
}

/**
 * [createObjects description]
 * @param  {[type]} rawJSON [description]
 * @param  {[type]} graph   [description]
 * @param  {[type]} other   [description]
 * @return {[type]}         [description]
 */
function createObjects(rawJSON,graph,other) {
  var objectArray = {};
  console.log(rawJSON);
  Object.keys(rawJSON).forEach(function(cl,i){
    if(cl!=='@context') {
      var classID = rawJSON[cl]['@type'];
      objectArray[classID] = function hydraObject() {
        var that = this;
        this['@type'] = classID;
        this['@id'] = '0';
        this['value'] = {};
        this['label'] = 'class label';
        this['actions'] = '';
        this['graph'] = graph;
        this['get'] = function(property,value) {
          if(typeof that.value[property] === "object") {
            if((value === null)||(typeof value === "undefined")||(value === 'value')) {
              if (that.value[property].range==='xmls:dateTime') {
                var date = that.value[property].value;
                var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
                // return dateString;
                return that.value[property].value;
              } else {
                return that.value[property].value;
              }
            } else if(value == 'range') {
              return that.value[property].range;
            } else if (value == 'label')
            return that.value[property].label;
          } else {
            console.log('This class does not contain property:', property);
            return 0;
          }
        };
        this['set'] = function(property,data,value) {
          if(typeof that.value[property] === "object") {
            if((value == null)||(typeof value === "undefined")||(value === 'value')) {
              if (that.value[property].range ==='xmls:dateTime') {
                that.value[property].value = new Date(data);
              } else {
                that.value[property].value = data;
              }
              return that.value[property].value;
            } else if(value === 'range') {
              that.value[property].range = data;
              return that.value[property].range;
            } else if (value === 'label')
            that.value[property].label = data;
            return that.value[property].label;
          } else {
            // console.log('This class does not contain property:', property);
            return 0;
          }
        };
        var actions = {};
        var prop = {};

      Object.keys(rawJSON[cl]).forEach(function(property,k){
      if(property ==='potentialAction') {
        console.log('property',rawJSON[cl][property]);
        //searchaction
        Object.keys(rawJSON[cl]['potentialAction']).forEach(function(action,j){
          var actionType = rawJSON[cl]['potentialAction']['@type'];
          if(actionType ==='searchAction') {
            actions['testAction'] = function() {
              return that.label;
            };
           actions['searchAction'] = function(graph,input,type, action,fetchall) {
            input = input ? input : that['value'];
            type = that['@type'];
            graph = that['graph'];
            action = action ? action : actionType;
            fetchall = (typeof fetchall === 'undefined' || fetchall === null) ? fetchall : true;
            // var result = searchAction(graph,input,type,'');
            return schemaSearchAction(graph,input,type,actionType,true);
            // return retrieveAction(graph,input,type,actionType,true);
          };//endof function
         } else if(actionType ==='deleteAction') {
          console.log('test');
         }//end of action selection

        });// end of potentian action iteration
      } else if((property !== '@type')&&(property !== '@id')&&(property !== 'potentialAction')) {
        prop[property] = {};
        if(typeof rawJSON[cl][property] === "object") {
          prop[property]['range'] = rawJSON[cl][property]['@type'];
          // console.log('TYPEEE',rawJSON[cl][property]['@type']);
        } else {
          prop[property]['range'] = 'Property range';
        }
        if ( prop[property]['range'] === 'xmls:dateTime') {
            prop[property].get = function() {
              return prop[property].value.toDateString();
          };
          prop[property].set = function(data) {
            prop[property]['value'] = new Date(data);
            return prop[property].value.toJSON();
          };
        } else {
          prop[property].value = '';
          // prop[property].get = function() {
          //   return prop[property]['value'];
          // }
          // prop[property].set = function(data) {
          //   prop[property]['value'] = data;
          //   return true;
          // }
        }
        prop[property].label = 'Property Label';
      }
    });
      this.value = prop;
      this.actions = actions;
    }; //end of definition
  }
});//end of eachclass
  return objectArray;
}//end of createObject

/**
 * [Parse Hydrajsonld as RDF graph and return a promise]
 * @param  {[type]} hydraDoc    [description]
 * @param  {[type]} hydraGraph  [description]
 * @param  {[type]} hydraServer [description]
 * @return {[type]}             [description]
 */
var hydraParsedPromise = function(hydraDoc,hydraGraph,hydraServer) {
  var previousGraph = hydraGraph.statements.length;
    return new Promise(function(resolve, reject) {
        $rdf.parse(hydraDoc, hydraGraph, hydraServer,'application/ld+json',function(error,kb){
          if ((error===null)&&(kb.statements.length>previousGraph)) {
            console.log('kb-length',kb.statements.length);
            log += 'Added ' + kb.statements.length + ' statements to default graph\n';
            resolve(kb);
          } else {
            // reject(error);
          }
        });
    });
};

function parseHydraObject(object,asd) {
  var props = graph.each(null,HYDRA('supportedProperty'),null);
  //retrievePropertyLiterals(node,graph)
  //retrieveHydraProperties(classNode,graph)
}

function parseHydra(graph) {
  console.log('parseHydra-GRAPH',graph);
  var classNodeArray = graph.each(null,RDF('type'),HYDRA('Class'));
  // var collectionNodeArray = graph.each(null,RDF('type'),HYDRA('Link'));
  var collectionNodeArray = retrieveCollectionNodes(graph);//graph.each(null,RDF('type'),HYDRA('PartialCollectionView'));
  var classLiterals = {};
  var collectionLiterals = {};
  var propNodes = [];
  var methodNodes = [];
  classNodeArray.forEach(function(classNode,i){
    var classPropsLiterals = {};
    var classMethodsLiterals = {};
    classLiterals[classNodeArray[i].value] = {};
    classLiterals[classNodeArray[i].value]['literals'] = retrieveClassLiterals(classNodeArray[i],graph);
    [propNodes,methodNodes] = retrieveClass(graph,classNodeArray[i]);

    propNodes.forEach(function(prop,k){
      var currentProp = retrievePropertyLiterals(prop,graph);
      classPropsLiterals[currentProp['label']] = currentProp;
    });
    classLiterals[classNodeArray[i].value]['properties'] = classPropsLiterals;

    methodNodes.forEach(function(prop,k){
      var currentMethod = retrieveOperationLiterals(methodNodes[k],graph);
      classMethodsLiterals[currentMethod['method']] = currentMethod;
    });
    classLiterals[classNodeArray[i].value]['operations'] = classMethodsLiterals;
  });
  collectionNodeArray.forEach(function(collectionNode,i){
    var collectionActionsLiterals = {};
    var collectionMethodsLiterals = {};
    var collectionNamedNode = graph.the(null,RDFS('range'),collectionNode);
    var pred =  graph.the(null,HYDRA('property'),collectionNode);
    console.log(pred);
    var currentProp = retrieveHydraPropertyLiterals(collectionNamedNode,graph);
    collectionLiterals[collectionNamedNode.value] = {};
    collectionLiterals[collectionNamedNode.value]['literals'] = currentProp;
    [propNodes,methodNodes] = retrieveClass(graph,collectionNamedNode);
    // collectionNodeArray[currentProp['label']] = currentProp;
    methodNodes.forEach(function(prop,k){
      var currentMethod = retrieveActionLiterals(methodNodes[k],graph);
      var actionName = currentMethod['potentialAction'].toClassNameSimple();
      var methodName = currentMethod['method'].toClassNameSimple();
      collectionActionsLiterals[actionName] = currentMethod;
      collectionMethodsLiterals[methodName] = currentMethod;
    });
    collectionLiterals[collectionNamedNode.value]['actions'] = collectionActionsLiterals;
    collectionLiterals[collectionNamedNode.value]['operations'] = collectionMethodsLiterals;
  });
  console.log(classLiterals);
  console.log(collectionLiterals);
  return [classLiterals,collectionLiterals];
}

function assignDuplicates(graph) {
  return true;
}

function clusterClasses(nodeArray,graph) {
  var clusters = {};
  nodeArray.forEach(function(classNode,i){
    var types = graph.each(classNode,RDF('type'),null);
    var counter = 0;
    var classTypes = [];
    types.forEach(function(cl,i){
      if (types[i].value!==HYDRA('Class').value) {
        classTypes[counter] = types[i].value;
        counter++;
      }
    });
    classTypes.forEach(function(type,k){
      if (clusters.hasOwnProperty(type)) {
        clusters[type].push(classNode);
      } else {
        clusters[type] = [];
        clusters[type].push(classNode);
      }
    });
  });
  return clusters;
}

function assignProperties(propertiesLiterals) {
  var prop = {};
  Object.keys(propertiesLiterals).forEach(function(property,k){
    var obj = propertiesLiterals[property] ;
    prop[property] = {};
    if((property !== '@type')&&(property !== '@id')&&(property !== 'potentialAction')) {
      prop[property] = obj;
      // prop[property]['value'] = {};
      obj['types'].forEach(function(value,i){
        if (value === HYDRA('Link').value) {
          // prop[property]['types'] = 
        }
      })
      prop[property].label = 'Property Label';
    }
  });
  return prop;
}

//works on schema/target
function retrieveSearchTemplate(url,graph) {
  var searchMaps;
  var urlTemplate;
  console.log('mlka urll',url);
  // console.log($rdf);
  var baseNode = $rdf.namedNode(url);
  var opNode = graph.each(null,SCHEMA('target'),baseNode);
  var blanky = graph.each(null,HYDRA('supportedOperation'),opNode[0]);
  var rangeObj = graph.each(blanky[0],RDFS('range'),null);
  var rangeType = graph.each(rangeObj[0],RDF('type'),null);
  // console.log('blanky',blanky);
  if ((rangeType[0].value===HYDRA('PartialCollectionView').value)||(rangeType[0].value===HYDRA('Collection').value)) {
    var hydraSearchNode = graph.each(rangeObj[0],HYDRA('search'),null);
    var hydraRepresenation = graph.each(hydraSearchNode[0],HYDRA('variableRepresentation'),null);
    var hydraTemplate = graph.each(hydraSearchNode[0],HYDRA('template'),null);
    var hydraMapping = graph.each(hydraSearchNode[0],HYDRA('mapping'),null);
    urlTemplate = urltemplate.parse(hydraTemplate[0].value);
    var currentMapping = [];
    hydraMapping.forEach(function(map,j){
      currentMapping[j] = retrieveSearchLiterals(hydraMapping[j],graph);
    });
    searchMaps = currentMapping;
    // console.log('Search URI template',hydraTemplate[0].value);
  }
  return [urlTemplate,searchMaps];
}

function assignActions(actionLiterals,hydraObject,action,referenceObject,instance,graph) {
  // var finalPromise;// = $q.defer();
  var finalPromiseResolve, finalPromiseReject;
  var finalPromise = new Promise(function(resolve, reject){
    finalPromiseResolve = resolve;
    finalPromiseReject = reject;
  });
  var actionPromises = [];
  var multipleServers = [];
  multipleServers.push(actionLiterals);
  if (typeof actionLiterals !== 'object') {
    finalPromiseResolve('No action Literals found');
    return finalPromise;
  }
  //For every action
  console.log('multipleServers',multipleServers);

  // for (var i = 0; i < Object.keys(multipleServers).length; i++) { 
  Object.keys(actionLiterals).forEach(function(base,i){
    console.log('base',base);
    // var base = Object.keys(multipleServers);
    var actionLits = actionLiterals[base];
    console.log('lolol',actionLits);
    var meth = actionLits['method'];
    var data = {};
    // Object.keys(hydraObject.value).forEach(function(property,i){
    //   data[property] = hydraObject.value[property].value;
    // });
    console.log('payload of HTTP request: ',data);
    var targetURL;
    // var targetURL = 'http://0.0.0.0:8091/software_applications';
    if (hydraObject['@id']) {
      targetURL = hydraObject['@id'];
    } else if (actionLits['target']) {
        targetURL = actionLits['target'];
    } else {
      console.log('No URL given, check @id field of hydraClass or hydraCollection. Or Action target property is missing.');
        // return finalPromise;
        // return; //failed to create binding skip iteration
    console.log('targetURL',targetURL);
    console.log('actionLits',actionLits);
      // continue;
    }

    var defaultOptions = {
      method: meth,
      headers: {'Content-Type': 'application/ld+json'},
      url: targetURL
    };

    //switch based on action name, if nothing special proceed based on http method
    switch(action) {
      case 'schemasearch':
        var searchMaps;
        var urlTemplate;
        [urlTemplate,searchMaps] = retrieveSearchTemplate(targetURL,graph);
        var getOptions = generateSearchURL(urlTemplate,searchMaps,data);
        defaultOptions.url = targetURL.getBase() + getOptions;
        // var prom = new Promise(function(resolve,reject) {
        //     resolve(hydraResponsePromise(defaultOptions,referenceObject));
        // });
        // actionPromises.push(prom);
        actionPromises.push(hydraResponsePromise(defaultOptions,referenceObject));
        break;
      case 'schemaAdd':
        break;
      case 'schema':
        break;
      case 'schemaTest':
        break;
    }
    if(instance) {
      switch(meth) {
        case 'DELETE':
            return $http(defaultOptions);
        break;
        case 'GET':
          return hydraResponsePromise(defaultOptions,referenceObject);
        break;
        case 'PUT':
            defaultOptions.data = data;
            return $http(defaultOptions);
        break;
        case 'POST':
            defaultOptions.data = data;
            return $http(defaultOptions);
        break;
      }
    } else {
      switch(meth) {
        case 'GET':
          return hydraResponsePromise(defaultOptions,referenceObject);
          break;
        case 'DELETE':
          return $http(defaultOptions);
          break;
        case 'PUT':
            defaultOptions.data = data;
            return $http(defaultOptions);
          break;
          case 'POST':
            defaultOptions.data = data;
            return $http(defaultOptions);
          break;
      }
    }
  // }//END of FOR
  });//END of servers iteration

  // console.log('mlka',actionPromises);
  Promise.all(actionPromises)
  .then(function(resolved) {
    console.log("ALL INITIAL PROMISES RESOLVED for action "+ action);
    console.log('resolved',resolved);
    var temp = [];//Array.isArray()
    var resolvedData = [];
    // if (Array.isArray(resolved)) {
    //   resolved.forEach(function(prom,i){
    //     if (Array.isArray(resolved[i])){
    //       if (resolved[i].length>0) {
    //         resolvedData = resolvedData.concat(resolved[i]);
    //       }
    //     }
    //   });
    // }
    console.log('resolvedData',resolvedData);
    finalPromiseResolve(resolved[0]);
  })
  .catch(function(resolved){
    $log.debug('One or more promises failed.');
    console.log('resolved',resolved);
    var temp = [];
    if (typeof resolved === "object") {

    } else {
      temp = resolved;
    }
    var resolvedData = [];
    temp.forEach(function(prom,i){
      if ((temp[i].length!==0)){
        resolvedData = resolvedData.concat(temp[i]);
      }
    });
      console.log('resolvedData',resolvedData);
      finalPromiseResolve(resolved[0]);
  });
  return finalPromise;
}

function instantiate(constructor, args) {
    var instance = Object.create(constructor.prototype);
    constructor.apply(instance, args);
    return instance;
}

function parseNested(obj,referenceObj,url) {
  var source = url.getBaseSimple();
  var currentType = obj['@type'].split('/').pop();
  var returnObj;
  if (Object.keys(referenceObj).indexOf(currentType)===-1) { //check
    console.log('Class type not defined anywhere! Requested Class type: ',currentType);
    return null;
  } else {
    returnObj = new referenceObj[currentType](url);
  }
  Object.keys(obj).forEach(function(prop,k){
      if (!(isContext(prop))) {
        if (( typeof obj[prop] === "object")&&( obj[prop])) {
          var id = source+obj[prop]['@id'];
          returnObj.set(prop,parseNested(obj[prop],referenceObj,id));
        } else {
          returnObj.set(prop,obj[prop]);
        }
      }
    });
  console.log('nestedObject',returnObj);
  return returnObj;
}

var parseHydraObj = function(response,referenceObj) {
  var members = [];
  var url = '';
  if (response.status!==200) {
    console.log('Server respond with '+response.status,response);
    return responseObjects[null];
  } else {
    url = response.config.url;
    members = response.data['hydra:member'];
  }
  members = Array.isArray(members) ? members : [members];
  console.log('members',members);
  var responseObjects = [];
  var source = url.getBaseSimple();
  var currentType = members[0]['@type'].split('/').pop();
  var test = hydraBindInstance(currentType,url);
  // console.log('test',test);
  // if (Object.keys(referenceObj).indexOf(currentType)===-1) { //check
  //   console.log('Class type not defined anywhere! Requested Class type: ',members[0]['@type']);
  //   return responseObjects[null];
  // }
  members.forEach(function(memberObj,k){
    var classType = members[k]['@type'].split('/').pop();
    // var returnObj = new referenceObj[currentType](url);
    responseObjects.push(hydraBindInstance(currentType,url));
    // responseObjects.push(new referenceObj(currentType,url));
    Object.keys(members[k]).forEach(function(prop,l){
      if (!(isContext(prop))) {
         // returnObj.value[prop] = {};
        if (( typeof members[k][prop] === "object")&&( members[k][prop])) {
          // console.log('Exw nested', prop);
          // var id = source+members[k][prop]['@id'];
          // responseObjects[k].set(prop,parseNested(members[k][prop],referenceObj,'',id));
        } else {
          console.log('prop',prop);
          console.log('memberObj[prop]',members[k][prop]);
          responseObjects[k].set(prop,members[k][prop]);
          // console.log('returnObj',returnObj);
        }
      }
    });
    // responseObjects[k] = returnObj;
  });
  console.log('AAAAresponseObjects',responseObjects);
  return responseObjects;
}

// var hydraResponsePromise = function(options,referenceObject) {
//   return $http(options)

// };

var hydraResponsePromise = function(options,referenceObject) {
  return $http(options);
  // var finalPromiseResolve, finalPromiseReject;
  // var finalPromise = new Promise(function(resolve, reject){
  //   finalPromiseResolve = resolve;
  //   finalPromiseReject = reject;
  // });
  // // var temp = new Promise(function(resolve,reject) {
  //   $http(options)
  //   .then(function(response){
  //     // pasre response
  //     console.log('Raw hydra response, before saving',response);
  //     var parsed;
  //     if (response.status!==200) {
  //       parsed = [];
  //     } else {
  //       var url = response.config.url;
  //       parsed = parseHydraObj(response.data['hydra:member'],referenceObject,url);
  //     }
  //     return finalPromiseResolve({response: response, parsedObjects: parsed});
  //   },
  //   function(error) { finalPromiseReject(error); } );
  // // });
  // return finalPromise;
};

function assignMethods(methodLiterals,hydraObject,meth,referenceObject,instance) {
 if (!(instance)) {
    instance = false;
  }
  console.log('Assigning Methods');
  // console.log('meth',meth);
  // console.log(hydraObject);
  // console.log('methodLiterals',methodLiterals);
  var data = {};
  Object.keys(hydraObject.value).forEach(function(property,i){
    data[property] = hydraObject.value[property].value;
  });
  console.log('payload of HTTP request: ',data);
  // var url = 'http://0.0.0.0:8091/software_applications';
  var url;
  if (typeof hydraObject['@id'] === 'undefined') {
    console.log('No URL given, check @id field of hydraClass or hydraCollection.');
    return false;
    url = '';
  } else {
    url = hydraObject['@id'];
  }
  var options;
  var defaultOptions = {
    method: meth,
    headers: {'Content-Type': 'application/ld+json'},
    url: url
  };
  if(instance) {
    switch(meth) {
      case 'DELETE':
          return $http(defaultOptions);
      break;
      case 'GET':
        return hydraResponsePromise(defaultOptions,referenceObject);
      break;
      case 'PUT':
          defaultOptions.data = data;
          return $http(defaultOptions);
      break;
      case 'POST':
          defaultOptions.data = data;
          return $http(defaultOptions);
      break;
    }
  } else {
    switch(meth) {
      case 'GET':
        return hydraResponsePromise(defaultOptions,referenceObject);
        break;
      case 'DELETE':
        return $http(defaultOptions);
        break;
      case 'PUT':
          defaultOptions.data = data;
          return $http(defaultOptions);
        break;
        case 'POST':
          defaultOptions.data = data;
          return $http(defaultOptions);
        break;
    }
  }
}

function commonNode(classArray) {
  return classArray[0];
}


// HELP!!!!!!!!!!!!!!!!!!!!!!!!!!
// /////////////////////////////////////////////////

  var hydraProtoClass = {};
  var hydraProtoCollection = {};
  var hydraBindObject = {};
  var hydraProtoClassList = {};
  // var hydraBindInstance;
  // var classNodeArray = graph.each(null,RDF('type'),HYDRA('Class'));
  // var clusterClassArray = clusterClasses(classNodeArray,graph);
  var classLiterals = {};
  var collectionLiterals = {};
  var propNodes = [];
  var methodNodes = [];
  var hydraBindInstance = function(type,url) {
    console.log('TYPE',type);
    url = url ? url : null;
    if (url) {
      var baseURL = url.getBaseSimple();
      var testProto = new hydraProtoClass[type](baseURL);
      // console.log('mlka',testProto);
      if (Object.keys(testProto).length>1) {
        // console.log('We have a known object of type '+classID+' and with baseURL: '+baseURL);
        var obj = new hydraProtoClass[type](url);
        // obj.set('@id',URL);
        return obj;
      }
    }
    if (Object.keys(hydraBindObject).indexOf(type)>-1) { //check
      var obj = new hydraBindObject[type](url);
      console.log(obj); //Should show the a and b variables
      console.log('Using custom created obj baseURL: ', URL);
      return obj;
    } else {
      return null;
    }
  }
// var hydraBindObject = {};
var createProtos = function(graph,keepAll,rawJSON) {
  console.log('Creating protoype Objects - createProtos');
  // var hydraProtoClass = {};
  // var hydraProtoCollection = {};
  // var hydraBindObject = {};
  // var hydraProtoClassList = {};
  var classNodeArray = graph.each(null,RDF('type'),HYDRA('Class'));
  var clusterClassArray = clusterClasses(classNodeArray,graph);
  var classLiterals = {};
  var collectionLiterals = {};
  var propNodes = [];
  var methodNodes = [];
  console.log('clusterClassArray',clusterClassArray);

  // var hydraBindInstance = function(type,url) {
  //   console.log('TYPE',type);
  //   url = url ? url : null;
  //   if (url) {
  //     var baseURL = url.getBaseSimple();
  //     var testProto = new hydraProtoClass[type](baseURL);
  //     // console.log('mlka',testProto);
  //     if (Object.keys(testProto).length>1) {
  //       // console.log('We have a known object of type '+classID+' and with baseURL: '+baseURL);
  //       var obj = new hydraProtoClass[type](url);
  //       // obj.set('@id',URL);
  //       return obj;
  //     }
  //   }
  //   if (Object.keys(hydraBindObject).indexOf(type)>-1) { //check
  //     var obj = new hydraBindObject[type](url);
  //     console.log(obj); //Should show the a and b variables
  //     console.log('Using custom created obj baseURL: ', URL);
  //     return obj;
  //   } else {
  //     return null;
  //   }
  // }

  Object.keys(clusterClassArray).forEach(function(classArray,l){
    var current = clusterClassArray[classArray];
    // console.log('current',current);
    var classType = current[0].value.toClassNameGraph();
    classLiterals[classType] = {};
    //create common class
    classLiterals[classType]['common'] = {};
    var classLocalNode = graph.each(null,RDF('type'),SCHEMA(classType));
/////////////////////////////////////////////
    // console.log('classLocalNode',classLocalNode);
    var val = "http://localhost/"+classType;
    var localSelected = [];
    classLocalNode.forEach(function(node,l){
      if (node.value===val) {
        localSelected = node;
      }
    });

    var localLits = retrieveClassLiterals(localSelected,graph);
    // console.log('localLits',localLits);
    var props = retrieveLocalClass(graph,localSelected);
    // console.log('Local props',props);
    var classProp = [];
    props.forEach(function(prop,k){
      var currentProp = retrieveLocalPropertyLiterals(prop,graph);
      classProp[prop.value.toClassName()] = currentProp;
    });

    classLiterals[classType]['common']['properties'] = classProp;
    classLiterals[classType]['common']['literals'] = localLits;
    classLiterals[classType]['common']['operations'] = [];
    classLiterals[classType]['common']['actions'] = [];
////////////////////////////////////////////////////////////////////////////////
    current.forEach(function(classNode,j){
      [propNodes,methodNodes] = retrieveClass(graph,classNode);
      var baseURL = classNode.value.getBaseSimple();
      // console.log('mlka baseURL',baseURL);
      var classPropsLiterals = {};
      var classMethodsLiterals = {};
      var classActionsLiterals = {};
      classLiterals[classType][baseURL] = {};
      classLiterals[classType][baseURL]['literals'] = retrieveClassLiterals(classNode,graph);

      propNodes.forEach(function(prop,k){
        var currentProp = retrievePropertyLiterals(prop,graph);
        classPropsLiterals[currentProp['label']] = currentProp;
      });
      classLiterals[classType][baseURL]['properties'] = classPropsLiterals;

      methodNodes.forEach(function(prop,k){
        var currentMethod = retrieveOperationLiterals(methodNodes[k],graph);
        classMethodsLiterals[currentMethod['method']] = currentMethod;
      });
      methodNodes.forEach(function(prop,k){
        var currentAction = retrieveActionLiterals(methodNodes[k],graph);
        classActionsLiterals[currentAction['potentialAction']] = currentAction;
      });

      classLiterals[classType][baseURL]['operations'] = classMethodsLiterals;
      classLiterals[classType][baseURL]['actions'] = classActionsLiterals;
      // console.log('classLiterals',classLiterals);

      hydraProtoClass[classType] = function hydraClass(url) {
        var that = this;
       // if (this.constructor == hydraProtoClass[classType])
       //    alert('hydraProtoClass '+ classType +'called with new');
       //  else
       //    alert('hydraProtoClass '+ classType +'called as function');
        var baseURL = url ? url.getBaseSimple().trim() : null;
        this['classLits'];// = classLiterals[classType]['common'];
        // console.log('classLits',classLits);
        if ((baseURL!==null)&&(Object.keys(classLiterals[classType]).indexOf(baseURL)>-1)) {
          // console.log('Using prototype Class from server\'s description:',baseURL);
          this['classLits'] = classLiterals[classType][baseURL];
        } else {
          console.log('Such server was not found:',baseURL );
          // console.log(classLiterals[classType]);
          return null;
        }
        var temp = assignProperties(this['classLits']['properties']);
        this['props'] = {};// temp;//assignProperties(this['classLits']['properties']);
        Object.keys(classLiterals[classType][baseURL]['properties']).forEach(function(propertyName,o){
          that['props'][propertyName] = {};
        });
        this['@id'] = url;
        this['source'] = baseURL;
        this['type'] = this['classLits']['literals']['classTypes'][0];
        this['label'] = this['classLits']['literals']['label'];
        this['graph'] = graph;
        this['name'] = '';

// var props = {};
// Object.keys(classLiterals[classType][baseURL]['properties']).forEach(function(propertyName,o){
// // that[propertyName] = {};
// // that[propertyName]['value'] = 'test';

//       Object.defineProperties(that, {
//         [propertyName]: {
//               // value: {},
//               // writable: true,
//               get: function()    {
//                 return this[propertyName];
//                 // var out;
//                 // if(typeof this.value[property] === "object") {
//                 //   if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
//                 //     if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
//                 //       var date = this.value[property].value;
//                 //       var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
//                 //       // return dateString;
//                 //       return this.value[property].value;
//                 //     } else if (this.value[property].range===XSD('date')) {
//                 //       var date = this.value[property].value;
//                 //       var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
//                 //       // return dateString;
//                 //       return this.value[property].value;
//                 //     } else if (Object.keys(this.value[property].value).indexOf('@id')) {
//                 //       out = this.value[property].value ? this.value[property].value : false;
//                 //       return out;
//                 //     }
//                 //   } else if( typeof spec === 'string') {
//                 //       out = this.value[property].spec ? this.value[property].spec : false;
//                 //       return out;
//                 //   } else {
//                 //     console.log('This class does not contain property:', property);
//                 //     return false;
//                 //   }
//                 // } else if(Object.keys(this.value).indexOf(property)>-1) {
//                 //   out = this.value[property].value ? this.value[property].value : false;
//                 //   return out;
//                 // } else {
//                 //   console.log('This class does not contain property:', property);
//                 //   return false;
//                 // }
//               }
//               ,set: function(val) {
//                 // that[propertyName] = {};
//                 this[propertyName] = val;
//               // // console.log()
//                 // if (!(Object.keys(this.value).indexOf(property)>-1)) {
//                 //   return null;
//                 // } else {
//                 //           this.value[property].value = data;
//                 // }
//                 // var saved;
//                 // console.log('property',property);
//                 // if(typeof this.value[property] === "object") {
//                 //   if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
//                 //     //datetime
//                 //     if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
//                 //       this.value[property].value = new Date(data);
//                 //     } else {
//                 //       this.value[property].value = data;
//                 //     }
//                 //     // return this.value[property].value;
//                 //   } else if (typeof spec === 'string') {
//                 //       saved = this.value[property].spec ? this.value[property].spec : false;
//                 //       this.value[property].value = data;
//                 //       // return saved;
//                 //   } else {
//                 //     console.log('This class does not contain property:', property);
//                 //     // return false;
//                 //   }
//                 // } else if(typeof this.value[property] === "string") {
//                 //   this.value[property].value = data;
//                 //   // return data;
//                 // } else {
//                 //   this.value[property].value = null;
//                 // }
//               }
//         }
//       });

// })


      //   this['get'] = function(property,spec) {
      //     var out;
      //     if(typeof that.value[property] === "object") {
      //       if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
      //         if ((that.value[property].range===XSD('dateTime').value)||(that.value[property].range==='xmls:dateTime')) {
      //           var date = that.value[property].value;
      //           var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
      //           // return dateString;
      //           return that.value[property].value;
      //         } else if (that.value[property].range===XSD('date')) {
      //           var date = that.value[property].value;
      //           var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
      //           // return dateString;
      //           return that.value[property].value;
      //         } else if (Object.keys(that.value[property].value).indexOf('@id')) {
      //           out = that.value[property].value ? that.value[property].value : false;
      //           return out;
      //         }
      //       } else if( typeof spec === 'string') {
      //           out = that.value[property].spec ? that.value[property].spec : false;
      //           return out;
      //       } else {
      //         console.log('This class does not contain property:', property);
      //         return false;
      //       }
      //     } else {
      //       console.log('This class does not contain property:', property);
      //       return false;
      //     }
      //   };
      // this['set'] = function(property,data,spec) {
      //   console.log('MLKA',this);
      //   console.log(that);
      //   console.log('123',this.value);

      //   // // console.log()
      //   if (!(Object.keys(this.value).indexOf(property)>-1)) {
      //     return null;
      //   } else {
      //             this.value[property].value = data;
      //   }
      //   //   var saved;
      //   //   console.log('property',property);
      //   //   if(typeof this.value[property] === "object") {
      //   //     if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
      //   //       //datetime
      //   //       if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
      //   //         this.value[property].value = new Date(data);
      //   //       } else {
      //   //         this.value[property].value = data;
      //   //       }
      //   //       return this.value[property].value;
      //   //     } else if (typeof spec === 'string') {
      //   //         saved = this.value[property].spec ? this.value[property].spec : false;
      //   //         this.value[property].value = data;
      //   //         return saved;
      //   //     } else {
      //   //       console.log('This class does not contain property:', property);
      //   //       return false;
      //   //     }
      //   //   } else if(typeof this.value[property] === "string") {
      //   //     this.value[property].value = data;
      //   //     return data;
      //   //   } else {
      //   //     this.value[property].value = null;
      //   //   }
      //   //   this.value[property].value = temp;
      //   };

      } //END of contructor





     // hydraProtoClass[classType].prototype.set = function(property,data,spec) {
     //    console.log('MLKA',this);
     //    console.log(that);
     //    console.log('123',this.value);

     //    // // console.log()
     //    if (!(Object.keys(this.value).indexOf(property)>-1)) {
     //      return null;
     //    } else {
     //              this.value[property].value = data;
     //    }
     //    //   var saved;
     //    //   console.log('property',property);
     //    //   if(typeof this.value[property] === "object") {
     //    //     if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
     //    //       //datetime
     //    //       if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
     //    //         this.value[property].value = new Date(data);
     //    //       } else {
     //    //         this.value[property].value = data;
     //    //       }
     //    //       return this.value[property].value;
     //    //     } else if (typeof spec === 'string') {
     //    //         saved = this.value[property].spec ? this.value[property].spec : false;
     //    //         this.value[property].value = data;
     //    //         return saved;
     //    //     } else {
     //    //       console.log('This class does not contain property:', property);
     //    //       return false;
     //    //     }
     //    //   } else if(typeof this.value[property] === "string") {
     //    //     this.value[property].value = data;
     //    //     return data;
     //    //   } else {
     //    //     this.value[property].value = null;
     //    //   }
     //    //   this.value[property].value = temp;
     //    };

      hydraProtoClass[classType].prototype.action = function(name) {
        // return this['classLits']['actions'][name];
        if (Object.keys(this['classLits']['actions']).indexOf(name)>-1) {
          return assignActions({'custom': this['classLits']['actions'][name]},this,name,'',true,graph);
        } else {
          return null;
        }
      };
      hydraProtoClass[classType].prototype.method = function(name) {
        // return this['classLits']['actions'][name];
        if (Object.keys(this['classLits']['operations']).indexOf(name.toUpperCase())>-1) {
          return assignMethods({'custom': this['classLits']['operations'][name.toUpperCase()]},this,name,'',true,graph);
        } else {
          return null;
        }
      };
    })
  });
  // PARSE & CREATE Collection Objects
  // var collectionNodeArray = graph.each(null,RDF('type'),HYDRA('Link'));
  var collectionNodeArray = retrieveCollectionNodes(graph);//graph.each(null,RDF('type'),HYDRA('PartialCollectionView'));
  var collectionLiterals = {};
  var propNodes = [];
  var methodNodes = [];
  console.log('collectionNodeArrayasdddddd',collectionNodeArray);

  collectionNodeArray.forEach(function(collectionNode,i){
    var collectionActionsLiterals = {};
    var collectionMethodsLiterals = {};
    var collectionNamedNode = graph.the(null,RDFS('range'),collectionNode);

    // console.log('collectionNamedNode',collectionNamedNode);
    var pred =  graph.the(null,HYDRA('property'),collectionNode);
    // console.log(pred);
    var currentProp = retrieveHydraPropertyLiterals(collectionNamedNode,graph);
    var baseURL = collectionNamedNode.value.getBaseSimple();
    var collectionClassType = currentProp['range'].toClassNameGraph();
    if (collectionLiterals[collectionClassType]) {
      collectionLiterals[collectionClassType][baseURL] = {};
    } else {
      collectionLiterals[collectionClassType] = {};
      collectionLiterals[collectionClassType][baseURL] = {};
    }
    collectionLiterals[collectionClassType][baseURL]['literals'] = currentProp;
    [propNodes,methodNodes] = retrieveClass(graph,collectionNamedNode);
    // collectionNodeArray[currentProp['label']] = currentProp;
    methodNodes.forEach(function(prop,k){
      var currentMethod = retrieveActionLiterals(methodNodes[k],graph);
      var actionName = currentMethod['potentialAction'].toClassNameSimple();
      var methodName = currentMethod['method'].toClassNameSimple();
      collectionActionsLiterals[actionName] = currentMethod;
      collectionMethodsLiterals[methodName] = currentMethod;
    });
    collectionLiterals[collectionClassType][baseURL]['actions'] = collectionActionsLiterals;
    collectionLiterals[collectionClassType][baseURL]['operations'] = collectionMethodsLiterals;

    // create Collection Array Objects
    hydraProtoCollection[collectionClassType] = function hydraCollection(baseURL) {
      var that = this;
      baseURL = baseURL ? baseURL : 'common';
      var collectionLits = collectionLiterals[collectionClassType][Object.keys(collectionLiterals[collectionClassType])[0]];
      // console.log('classLits',classLits);
      if (baseURL==='common') {
        // console.log('Using described object');
      } else if(collectionLiterals[collectionClassType][baseURL]) {
        collectionLits = collectionLiterals[collectionClassType][baseURL];
      } else {
        console.log('Using default described object, such server was not found: ',baseURL );
      }
      this['collectionLits'] = collectionLits;
      this['@id'] = collectionLits['literals']['id'];
      this['member'] = collectionLits['literals']['member'];
      this['label'] = collectionLits['literals']['label'];
      this['value'];
      this['obj'] = hydraProtoClass;//classRef;
      this['graph'] = graph;
      // this['get'] = function(property,spec) {
      //   var out;
      //   if(typeof that.value[property] === "object") {
      //     if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
      //       if ((that.value[property].range===XSD('dateTime').value)||(that.value[property].range==='xmls:dateTime')) {
      //         var date = that.value[property].value;
      //         var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
      //         // return dateString;
      //         return that.value[property].value;
      //       } else if (that.value[property].range===XSD('date')) {
      //         var date = that.value[property].value;
      //         var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
      //         // return dateString;
      //         return that.value[property].value;
      //       } else {
      //         out = that.value[property].value ? that.value[property].value : false;
      //         return out;
      //       }
      //     } else if( typeof spec === 'string') {
      //         out = that.value[property].spec ? that.value[property].spec : false;
      //         return out;
      //     } else {
      //       console.log('This class does not contain property:', property);
      //       return false;
      //     }
      //   } else {
      //     console.log('This class does not contain property:', property);
      //     return false;
      //   }
      // };
      // this['set'] = function(property,data,spec) {
      //   var saved;
      //   if(typeof that.value[property] === "object") {
      //     if((spec == null)||(typeof spec === "undefined")||(spec === 'value')) {
      //       if ((that.value[property].range===XSD('dateTime').value)||(that.value[property].range==='xmls:dateTime')) {
      //         that.value[property].value = new Date(data);
      //       } else {
      //         that.value[property].value = data;
      //       }
      //       return that.value[property].value;
      //     } else if (typeof spec === 'string') {
      //         saved = that.value[property].spec ? that.value[property].spec : false;
      //         that.value[property].spec = data;
      //         return saved;
      //     } else
      //       console.log('This class does not contain property:', property);
      //       return false;
      //   } else {
      //     // console.log('This class does not contain property:', property);
      //     return 0;
      //   }
      // };
      // this['actions'] = {};
      // this['methods'] = {};
      // Object.keys(collectionLits['operations']).forEach(function(meth,i){
      //   that['methods'][meth.toLowerCase()] = function() {
      //     return assignMethods(collectionLits['operations'][meth],that,meth,that['obj'],false);
      //   }
      // });
      // Object.keys(collectionLits['actions']).forEach(function(actionName,i){
      //   // console.log('collectionLits[collectionNamedNode.value]',collectionLits['actions'][actionName]);
      //   that['actions'][actionName] = function() {
      //     return assignActions(collectionLits['actions'][actionName],that,actionName,that['obj'],true,graph);
      //   }
      // });
    }// END of collection proto
    hydraProtoCollection[collectionClassType].prototype.get = function(property,spec) {
          var out;
          if(typeof this.value[property] === "object") {
            if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
              if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
                var date = this.value[property].value;
                var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
                // return dateString;
                return this.value[property].value;
              } else if (this.value[property].range===XSD('date')) {
                var date = this.value[property].value;
                var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
                // return dateString;
                return this.value[property].value;
              } else if (Object.keys(this.value[property].value).indexOf('@id')) {
                out = this.value[property].value ? this.value[property].value : false;
                return out;
              }
            } else if( typeof spec === 'string') {
                out = this.value[property].spec ? this.value[property].spec : false;
                return out;
            } else {
              console.log('This class does not contain property:', property);
              return false;
            }
          } else {
            console.log('This class does not contain property:', property);
            return false;
          }
    };
    hydraProtoCollection[collectionClassType].prototype.set = function(property,data,spec) {
        var saved;
        if(typeof this.value[property] === "object") {
          if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
            //datetime
            if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
              this.value[property].value = new Date(data);
            } else {
              this.value[property].value = data;
            }
            return this.value[property].value;
          } else if (typeof spec === 'string') {
              saved = this.value[property].spec ? this.value[property].spec : false;
              this.value[property].value = data;
              return saved;
          } else {
            console.log('This class does not contain property:', property);
            return false;
          }
        } else if(typeof this.value[property] === "string") {
          this.value[property].value = data;
          return data;
        }
    };
    hydraProtoCollection[collectionClassType].prototype.action = function(name) {
      // return this['classLits']['actions'][name];
      if (Object.keys(this['collectionLits']['actions']).indexOf(name)>-1) {
        return assignActions({'custom': this['collectionLits']['actions'][name]},this,name,'',true,graph);
      } else {
        return null;
      }
    };
   hydraProtoCollection[collectionClassType].prototype.method = function(name) {
      // return this['classLits']['actions'][name];
      if (Object.keys(this['collectionLits']['operations']).indexOf(name.toUpperCase())>-1) {
        return assignMethods({'custom': this['collectionLits']['operations'][name.toUpperCase()]},this,name,'',true,graph);
      } else {
        return null;
      }
    };

  });

// Create custom User Objects
  Object.keys(rawJSON).forEach(function(cl,i){
    if(cl!=='@context') {
      var classID = rawJSON[cl]['@type'];
      var pro = classID+'bind';
      var jsonObj = rawJSON[cl];

      hydraBindObject[classID] = function hydraBind(URL) {//Object.prototype.pro;
       // if (this.constructor == hydraBindObject[classID])
       //    alert('hydraBindObject '+ classID +'called with new');
       //  else
       //    alert('hydraBindObject '+ classID +'called as function');
        var objOut = {};
        var customObj = {};
        var that = this;
        // this['type'] = classID;
        this['@id'] = URL;
        // this['protoClass'] = hydraProtoClass;
        // this['protoCollection'] = hydraProtoCollection;
        this['source'];
        URL = URL ? URL : null;
        if (URL) {
          var baseURL = URL.getBaseSimple();
          console.log('Using custom created obj baseURL: ', URL);
          this['source'] = baseURL;
          // console.log(URL);
          // console.log(baseURL);
          var testProto = new hydraProtoClass[classID](baseURL);
          // console.log('mlka',testProto);
          // if (Object.keys(testProto).length>1) {
          //   // console.log('We have a known object of type '+classID+' and with baseURL: '+baseURL);
          //   // var obj = new hydraProtoClass[classID](URL);
          //   // obj.set('@id',URL);
          //   // return obj;
          // } else {

          // }
        } else {
          this['source'] = 'none';
        }

        this['value'] = {};
        this['label'] = 'class label';
        this['graph'] = graph;
        // this['get'] = function(property,spec) {
        //   var out;
        //   if(typeof this.value[property] === "object") {
        //     if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
        //       if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
        //         var date = this.value[property].value;
        //         var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
        //         // return dateString;
        //         return this.value[property].value;
        //       } else if (this.value[property].range===XSD('date')) {
        //         var date = this.value[property].value;
        //         var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
        //         // return dateString;
        //         return this.value[property].value;
        //       } else {
        //         out = this.value[property].value ? this.value[property].value : false;
        //         return out;
        //       }
        //     } else if( typeof spec === 'string') {
        //         out = this.value[property].spec ? this.value[property].spec : false;
        //         return out;
        //     } else {
        //       console.log('This class does not contain property:', property);
        //       return false;
        //     }
        //   } else {
        //     console.log('This class does not contain property:', property);
        //     return false;
        //   }
        // };
        // this['set'] = function(property,data,spec) {
        //   var saved;
        //   if(typeof this.value[property] === "object") {
        //     if((spec == null)||(typeof spec === "undefined")||(spec === 'value')) {
        //       if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
        //         this.value[property].value = new Date(data);
        //       } else {
        //         this.value[property].value = data;
        //       }
        //       return this.value[property].value;
        //     } else if (typeof spec === 'string') {
        //         saved = this.value[property].spec ? this.value[property].spec : false;
        //         this.value[property].spec = data;
        //         return saved;
        //     } else
        //       console.log('This class does not contain property:', property);
        //       return false;
        //   } else {
        //     // console.log('This class does not contain property:', property);
        //     return 0;
        //   }
        // };
        // this['actions'] = {};
        // this['methods'] = {};
        console.log(Object.keys(hydraProtoClass[classID]));
        var actions = {};
        var prop = {};
        var possibleActions = {};
        this['possibleActions'] = possibleActions;
        var contextKeywords = ['@type','@id','potentialAction'];
        Object.keys(jsonObj).forEach(function(property,k){
          if(property ==='potentialAction') {
            var actionArray = [];
            if (!(Array.isArray(jsonObj['potentialAction']))) {
              actionArray.push(jsonObj['potentialAction']);
            } else {
              actionArray = jsonObj['potentialAction'];
            }
            actionArray.forEach(function(action,j){
              var actionType = action['@type'].replace(/Action/i,"");
              actionType = 'schema' + actionType;
              possibleActions[actionType] = {};
              Object.keys(classLiterals[classID]).forEach(function(base,l){
                // console.log('classLiterals[classID][base]',classLiterals[classID][base]['actions']);
                if (Object.keys(classLiterals[classID][base]['actions']).indexOf(actionType)>=0) {
                  possibleActions[actionType][base] = classLiterals[classID][base]['actions'][actionType];
                  // console.log('actionType Class',classLiterals[classID][base]['actions'][actionType]);
                }
                // console.log('We have those baseURLs to play with Classes: ',base);
              })// hydraProtoClass[classID]
              Object.keys(collectionLiterals[classID]).forEach(function(base,l){
                // console.log('collectionLiterals[classID][base]',collectionLiterals[classID][base]['actions']);
                if (Object.keys(collectionLiterals[classID][base]['actions']).indexOf(actionType)>=0) {
                  possibleActions[actionType][base] = collectionLiterals[classID][base]['actions'][actionType];
                  // console.log('actionType Collection',collectionLiterals[classID][base]['actions'][actionType]);
                }
                  // console.log('We have those baseURLs to play with Collections: '+ base+' and actionType: ' );
              })// hydraProtoCollection[classID]
           });// end of potentian action iteration
          } else if(contextKeywords.indexOf(property)<0) {
            prop[property] = {};
            if(typeof rawJSON[cl][property] === "object") {
              prop[property]['range'] = rawJSON[cl][property]['@type'];
              prop[property].types = rawJSON[cl][property]['@type'];
              // console.log('TYPEEE',rawJSON[cl][property]['@type']);
            } else {
              prop[property]['range'] = 'Property range';
            }
            prop[property].label = 'Property Label';
            prop[property].value = '';
            prop[property].description = '';
            prop[property].domain = 'http://localhost/';
          }
        });
        // this.value = prop;
        // return that;
      }// FUNCTION
      hydraBindObject[classID].prototype.get = function(property,spec) {
        var out;
        if(typeof this.value[property] === "object") {
          if((spec === null)||(typeof spec === "undefined")||(spec === 'value')) {
            if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
              var date = this.value[property].value;
              var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
              // return dateString;
              return this.value[property].value;
            } else if (this.value[property].range===XSD('date')) {
              var date = this.value[property].value;
              var dateString = date.getFullYear() + ' ' + date.getMonth() + ' ' + date.getDate();
              // return dateString;
              return this.value[property].value;
            } else {
              out = this.value[property].value ? this.value[property].value : false;
              return out;
            }
          } else if( typeof spec === 'string') {
              out = this.value[property].spec ? this.value[property].spec : false;
              return out;
          } else {
            console.log('This class does not contain property:', property);
            return false;
          }
        } else {
          console.log('This class does not contain property:', property);
          return false;
        }
      };
      hydraBindObject[classID].prototype.set = function(property,data,spec) {
        var saved;
        if(typeof this.value[property] === "object") {
          if((spec == null)||(typeof spec === "undefined")||(spec === 'value')) {
            if ((this.value[property].range===XSD('dateTime').value)||(this.value[property].range==='xmls:dateTime')) {
              this.value[property].value = new Date(data);
            } else {
              this.value[property].value = data;
            }
            return this.value[property].value;
          } else if (typeof spec === 'string') {
              saved = this.value[property].spec ? this.value[property].spec : false;
              this.value[property].spec = data;
              return saved;
          } else
            console.log('This class does not contain property:', property);
            return false;
        } else {
          // console.log('This class does not contain property:', property);
          return 0;
        }
      };
      hydraBindObject[classID].prototype.action = function(name,refObj) {
        // return this['classLits']['actions'][name];
        console.log('this',this);
        if (Object.keys(this['possibleActions']).indexOf(name)>-1) {
          return assignActions(this['possibleActions'][name],this,name,hydraBindInstance,false,this['graph']);
        } else {
          return null;
        }
      };
    }//EDN IF
  });
  // hydraBindObject[classID].prototype
  // console.log('hydraProtoClass',hydraProtoClass);
  // console.log(classLiterals);
  // console.log(collectionLiterals);
  return {
    'classLiterals': classLiterals,
    'collectionLiterals': collectionLiterals,
    'hyClass':hydraProtoClass,
    'hyCollection': hydraProtoCollection,
    'hyBind': hydraBindObject,
    'hyAuto': hydraBindInstance,
    'hyParse': parseHydraObj
  };//[classLiterals,collectionLiterals,hydraProtoClass,hydraProtoCollection,hydraBindObject,hydraBindInstance];
}

/*
    Retrieve the properties a schema Class may contain
    input: class name only
    output: array of NameNodes
 */
function retrieveHydraProperties(classNode,graph) {
    var props = graph.each(classNode,HYDRA('supportedProperty'),null);
    var methods = graph.each(classNode,HYDRA('supportedOperation'),null);

    props.forEach(function(prop,k){
      // console.log(props[k]);
      var test = graph.each(props[k],HYDRA('property'),null);
      console.log('test',test);

      console.log(graph.each(props[k],HYDRA('title'),null));
      console.log('Test2:',graph.each(test[0],RDF('type'),null));

      test.forEach(function(bl,l){
        var test2 = graph.each(test[l],RDF('type'),null);
        console.log('RDF(type)',test2);
      })
      console.log(test);
    });
    console.log('subClassOf PostalAddress',props);
    return props;
}

/**
 * [retrieveClass description]
 * @return {[Array of Blank nodes]} [description]
 */
  function retrieveLocalClass(graph,classNode) {
    var props = graph.each(classNode,null,null);
    var properties = [];
    props.forEach(function(predicateNode,i){
      var predicate = predicateNode.value;
      switch(predicate) {
        case SCHEMA('potentialAction').value:
          //we got actions
          break;
        case RDF('type').value:
          //we got the type
          break;
        default:
          //we got a property
          properties.push(predicateNode);
      }
    });
    var methods = graph.each(classNode,null,null);

    return properties;
  }

/**
 * [retrieveClass description]
 * @return {[Array of Blank nodes]} [description]
 */
  function retrieveClass(graph,classNode) {
    var props = graph.each(classNode,HYDRA('supportedProperty'),null);
    var methods = graph.each(classNode,HYDRA('supportedOperation'),null);
    // test = {};
    // test['@type'] = 'Airport';
    // searchAction(graph,test,null,null);
    return [props,methods];
  }

/**
 * [retrieveCollectioNodes description]
 * @param  {[type]} graph [description]
 * @return {[type]}       [description]
 */
  function retrieveCollectionNodes(graph) {
    var collectionNodes = [];
    var subnodes = graph.each(null,RDF('type'),HYDRA('PartialCollectionView'));
   subnodes = subnodes.concat(graph.each(null,RDF('type'),HYDRA('Collection')));
    console.log('subnodes',subnodes);
    // subnodes.forEach(function(a,i){
    //   collectionNodes.push(graph.each(null,null,subnodes[i])[0]);
    //   // var temp = graph.each(null,null,subnodes[i]);
    //   // console.log('nonono',temp);
    // });
    // console.log('CollectionNodes:',collectionNodes);
    return subnodes;
  }

function retrieveOperationLiterals(node,graph) {
  var props = {};
  props.method = graph.each(node,HYDRA('method'),null)[0] ? graph.each(node,HYDRA('method'),null)[0].value : null;
  props.title = graph.each(node,HYDRA('title'),null)[0] ? graph.each(node,HYDRA('title'),null)[0].value : null;
  props.label = graph.the(node,RDFS('label'),null) ? graph.the(node,RDFS('label'),null).value : null;
  props.returns = graph.each(node,HYDRA('returns'),null)[0] ? graph.each(node,HYDRA('returns'),null)[0].value : null;
  props.expects = graph.each(node,HYDRA('expects'),null)[0] ? graph.each(node,HYDRA('expects'),null)[0].value : null;
  return props;
}

function retrieveHydraPropertyLiterals(node,graph) {
  var props = {};
  props.id = node ? node.value : null ;
  props.label = graph.each(node,RDFS('label'),null)[0] ? graph.each(node,RDFS('label'),null)[0].value : null;
  props.domain = graph.each(node,RDFS('domain'),null)[0] ? graph.each(node,RDFS('domain'),null)[0].value : null;
  var rangeBlankNode = graph.each(node,RDFS('range'),null)[0] ? graph.each(node,RDFS('range'),null)[0] : null;
  props.range = graph.each(node,RDFS('range'),null)[0] ? graph.each(node,RDFS('range'),null)[0].value : null;
  var memberBlanknode = graph.the(rangeBlankNode,HYDRA('member'),null) ? graph.the(rangeBlankNode,HYDRA('member'),null) : null;
  var memberType;
  if (memberBlanknode) {
    memberType = graph.each(memberBlanknode,RDF('type'),null) ? graph.each(memberBlanknode,RDF('type'),null)[0] : null;
    // console.log('memberType',memberType);
    var classTypeArray = graph.each(memberType,RDF('type'),null) ? graph.each(memberType,RDF('type'),null) : null;
    var counter = 0;
    var classTypes = [];
    classTypeArray.forEach(function(cl,i){
      if (classTypeArray[i].value!==HYDRA('Class').value) {
        classTypes[counter] = classTypeArray[i].value;
        counter++;
      }
    })
    props.member = classTypes[0];
    props.range = memberType.value;
  }

  var propTypes = graph.each(node,RDF('type'),null);
  var counter = 0;
  props.types = [];
  propTypes.forEach(function(cl,i){
    if (propTypes[i].value!==RDF('Property').value) {
      props.types[counter] = propTypes[i].value;
      counter++;
    }
  });
  return props;
}

function retrieveActionLiterals(node,graph) {
  var props = {};
  props.method = graph.each(node,HYDRA('method'),null)[0] ? graph.each(node,HYDRA('method'),null)[0].value : null;
  props.title = graph.each(node,HYDRA('title'),null)[0] ? graph.each(node,HYDRA('title'),null)[0].value : null;
  props.label = graph.the(node,RDFS('label'),null) ? graph.the(node,RDFS('label'),null).value : null;
  props.returns = graph.each(node,HYDRA('returns'),null)[0] ? graph.each(node,HYDRA('returns'),null)[0].value : null;
  props.expects = graph.each(node,HYDRA('expects'),null)[0] ? graph.each(node,HYDRA('expects'),null)[0].value : null;
  props.target = graph.each(node,SCHEMA('target'),null)[0] ? graph.each(node,SCHEMA('target'),null)[0].value : null;
  props.result = graph.each(node,SCHEMA('result'),null)[0] ? graph.each(node,SCHEMA('result'),null)[0].value : null;
  props.query = graph.each(node,SCHEMA('query'),null)[0] ? graph.each(node,SCHEMA('query'),null)[0].value : null;
  props.object = graph.each(node,SCHEMA('object'),null)[0] ? graph.each(node,SCHEMA('object'),null)[0].value : null;
  var propTypes = graph.each(node,RDF('type'),null);
  var counter = 0;
  // props.types = [];
  props.potentialAction = '';
  propTypes.forEach(function(cl,i){
    if (propTypes[i].value!==HYDRA('operation').value) {
      props.potentialAction = propTypes[i].value;
      var actionURI = props.potentialAction.splitURI();
      var actionName = actionURI[3].split('.')[0] + actionURI[5].replace(/Action/i,"");
      // console.log('actionName!!!!!!!!!!!',actionName);
      props.potentialAction = actionName ;
      counter++;
    }
  });
  return props;
}

function retrievePropertyLiterals(node,graph) {
  var props = {};
  // searchOp['method'] = graph.each(node,HYDRA('method'),null)[0] ? graph.each(node,HYDRA('method'),null)[0].value : null;

  props.title = graph.each(node,HYDRA('title'),null)[0] ? graph.each(node,HYDRA('title'),null)[0].value : null;
  props.description = graph.each(node,HYDRA('description'),null)[0] ? graph.each(node,HYDRA('description'),null)[0].value : null;
  props.required = graph.each(node,RDFS('required'),null)[0] ? graph.each(node,RDFS('required'),null)[0].value : null;
  props.readable = graph.each(node,HYDRA('readable'),null)[0] ? graph.each(node,HYDRA('readable'),null)[0].value : null;
  props.writable = graph.each(node,HYDRA('writable'),null)[0] ? graph.each(node,HYDRA('writable'),null)[0].value : null;
  var hydraProps = graph.each(node,HYDRA('property'),null)[0] ? graph.each(node,HYDRA('property'),null)[0] : node;
  var propses = graph.each(hydraProps[0],null,null);
  // console.log('hydraProps',propses);
  var out = Object.assign({},props,retrieveHydraPropertyLiterals(hydraProps,graph));
  return out;
}

function retrieveLocalPropertyLiterals(node,graph) {
  var props = {};
  // console.log('NODEEEEEEE',node.value);
  var propertySubj = graph.each(null,node,null);
  var propertyBlanks = graph.each(propertySubj[0],node,null);
  var propLits;
  props.types = [];
  propertyBlanks.forEach(function(blank,k){
    var types = graph.each(blank,RDF('type'),null) ? graph.each(blank,RDF('type'),null) : null;
    // console.log('types',types);
    if (types.length>0) {
      props.types.push(types[0].value);
    }
    // var predicate = graph.each(blank,null,null);
    // console.log('predicate',predicate[0]);
  });
  // console.log('propBlanks',propertyBlanks);

  // searchOp['method'] = graph.each(node,HYDRA('method'),null)[0] ? graph.each(node,HYDRA('method'),null)[0].value : null;

  props.range = props.types.length>0 ? props.types[0] : null;
  props.domain = 'http://localhost/';
  props.label = props.range ? props.range.toClassName() : null;
  // props.label = props.types.length>0 ? props.types[0] : null;
  props.title = graph.each(node,HYDRA('title'),null)[0] ? graph.each(node,HYDRA('title'),null)[0].value : null;
  props.description = graph.each(node,HYDRA('description'),null)[0] ? graph.each(node,HYDRA('description'),null)[0].value : null;
  props.required = graph.each(node,RDFS('required'),null)[0] ? graph.each(node,RDFS('required'),null)[0].value : null;
  props.readable = graph.each(node,HYDRA('readable'),null)[0] ? graph.each(node,HYDRA('readable'),null)[0].value : null;
  props.writable = graph.each(node,HYDRA('writable'),null)[0] ? graph.each(node,HYDRA('writable'),null)[0].value : null;
  // console.log('hydraProps',propses);
  var out = props;//Object.assign({},props,retrieveHydraPropertyLiterals(hydraProps,graph));
  return out;
}

function retrieveClassLiterals(node,graph) {
  var props = {};
  // searchOp['method'] = graph.each(node,HYDRA('method'),null)[0] ? graph.each(node,HYDRA('method'),null)[0].value : null;
  props.title = graph.each(node,HYDRA('title'),null)[0] ? graph.each(node,HYDRA('title'),null)[0].value : null;
  props.label = graph.the(node,RDFS('label'),null) ? graph.the(node,RDFS('label'),null).value : null;
  var types = graph.each(node,RDF('type'),null);
  var counter = 0;
  props.classTypes = [];
  types.forEach(function(cl,i){
    if (types[i].value!==HYDRA('Class').value) {
      props.classTypes[counter] = types[i].value;
      counter++;
    }
  })
  return props;
}

function retrieveSearchLiterals(node,graph) {
  var props = {};
  props.property = graph.each(node,HYDRA('property'),null)[0] ? graph.each(node,HYDRA('property'),null)[0].value : null;
  props.variable = graph.each(node,HYDRA('variable'),null)[0] ? graph.each(node,HYDRA('variable'),null)[0].value : null;
  props.required = graph.each(node,RDFS('required'),null)[0] ? graph.the(node,HYDRA('required'),null)[0].value : false;
  return props;
}

/**
 * [js_traverse description]
 * @param  {[type]} o        [description]
 * @param  {[type]} property [description]
 * @return {[type]}          [description]
 */
function js_traverse(o,property) {
  var type = typeof o;
  var out = {};
  if (type === "object") {
      for (var key in o) {
          if (key===property) {
            return o;
          } else {
            out = js_traverse(o[key],property);
            if (typeof out === "object") {
            if (out.hasOwnProperty(property)) {return out;}
          }
          }
        }
  } else {
    return false;
  }
}

function nestedProperty(o,chain) {
  var child = chain[0];
  var temp;
  // console.log('child',child);
  // console.log(o);
  if ((o.hasOwnProperty(child))) {
    if ((typeof o[child].value === "object")) {
      if (chain.length>1) {
        var next = chain.slice(1);
        temp = nestedProperty(o[child].value,next);
        if(temp){
          return temp;
        } else {
          return false;
        }
      } else {
        return o[child];
      }
    } else {
      return o[child];
    }
  } else if(o.hasOwnProperty('value')){
    if ((typeof o.value === "object")) {
      if (chain.length>=1) {
        // var next = chain.slice(1);
        temp = nestedProperty(o.value,chain);
        if(temp){
          return temp;
        } else {
          return false;
        }
      } else {
        return o[child];
      }
    } else {
      return o[child];
    }
  } else {
    return false;
  }
}

/**
 * [searchDataBinds description]
 * @param  {[type]} mapObj [description]
 * @param  {[type]} data   [description]
 * @return {[type]}        [description]
 */
function searchDataBinds(mapObj,data) {
  // console.log('dataBinds',data);
  // console.log('mapObj',mapObj);
  mapObj.forEach(function(map,i){
    var chainProps = mapObj[i]['variable'].split(".");
    // console.log('chainProps',chainProps);
    var propNode = SCHEMA(chainProps[chainProps.length-1]);
    var property = (chainProps[chainProps.length-1]);
    // console.log(propNode);
    // console.log('datatat',data);
    // console.log('DepthOfObject',depthOf(data));
    // console.log('classTier',classTier(data,0));
    // mapObj[i]['value'] = data['departureAirport']['value'];//assign value for search
    var values = nestedProperty(data,chainProps);
    // console.log('temp',temp);
    // var values = js_traverse(data,property);
    // console.log('VALUESSS',values);
    if(values) {
      // console.log('property',property);
      if (values.value instanceof Date) {
        mapObj[i]['value'] = values.value.toDateString();
      } else {
        mapObj[i]['value'] = values.value;//assign value for search
      }
    } else {
      mapObj[i]['value'] = '';
      // console.log('propertyNOTSearch',property);
          //Should to some resaerch let for later*
    }
  });
  return mapObj;
}
/**
 * [generateSearchURL description]
 * @param  {[type]} urlTempl [description]
 * @param  {[type]} mapObj   [description]
 * @param  {[type]} data     [description]
 * @return {[type]}          [description]
 */
  function generateSearchURL(urlTempl,mapObj,data) {

    var readyMap = searchDataBinds(mapObj,data);
    // console.log('readyMap',readyMap);
    var parameters = {};
    readyMap.forEach(function(parameter,i){
      if(readyMap[i].value) {
        var varName = readyMap[i]['variable'];
        parameters[varName] = readyMap[i].value;
      } else {
        // console.log('Not searchable property: ', readyMap[i]['variable']);
        // We have a value that maybe is custom,
        // it has it's own name:value, and does not
        // meet the selected class
      }
    });
    // console.log('urlTempl',urlTempl);
    // console.log('parameters',parameters);
    var out = urlTempl.expand(parameters) ? urlTempl.expand(parameters) : null;
    console.log(out);
    return out;//srting
  }

  var parseInput = function(input,graph) {
      var deferred = $q.defer();
      var data = {};
      deferred.notify('About to parse data.');
      hydraParsedPromise(JSON.stringify(input),graph,'').then(function(graph) {
        console.log('graph with input',graph);
        Object.keys(input).forEach(function(cl,i){
          if(!(isContext(cl))) {
            data[cl] = input[cl];
          }
        });
        deferred.resolve(data);
        return data;
      });
      return deferred.promise;
  };

/**
 * [resolveAPI description]
 * @param  {[type]} graph [description]
 * @return {[type]}       [description]
 */
  var resolveAPI = function(graph,rawJSON) {
    // var arrao = createProtos(graph,true,rawJSON);
    // console.log('createProtos!',arrao);
    // var testClass = new arrao[2].Flight('TROLLLL');
    // var testClass2 = new arrao[4].Airport('TROLLLL');
    // console.log('testClass!',testClass);
    // console.log('TEST BIND!!!',testClass2);

    // var testCollection = new arrao[3].TouristAttraction('testClass');
    // testCollection.set('@id','http://vps362714.ovh.net:8091/software_applications');
    // console.log('testCollection!',testCollection);
    // testClass['@id'] = 'http://vps362714.ovh.net:8091/software_applications';
    // // testCollection['@id'] = 'http://vps362714.ovh.net:8091/software_applications';
    // var testColOp = testCollection.methods.get();
    // var testColAction = testCollection.actions.schemasearch();
    // console.log('testColAction',testColAction);
    // testColOp.then(function(resp){
    //   console.log('Final response',resp);
    // });
      // var lool = testClass.methods.get();
      // // var lool2 = testClass.methods.delete();
      // console.log('lool',lool);
      // lool.then(function(resp){
      //   console.log(resp);
      // });
      // console.log('TEST OBJ',test);
      // console.log('RESOLVED!!!:',graph.statements.length);
    // var objects = createObjects(data,graph,'');
    var deferred = $q.defer();
    deferred.resolve(true);
    return deferred.promise;
  };

var fetchHydra = function(data,graph,req) {
  return new Promise(function(resolve,reject) {
    $http(req)
    .then(function(response){ resolve({hydra:response.data,graph:graph}); },
    function(error) { reject(error); } );
  });
  };

$log.debug( "Error logging failed" );
schydraAngular.log = log;
schydraAngular.error = error;
schydraAngular['init'] = function(rawJSON,resolverServer,graph){
  var deferred = $q.defer();
  var req = {
    method: 'PUT',
    url: resolverServer,
    headers: {
    'Content-Type': 'application/ld+json'
    },
    data: rawJSON
  };
    parseInput(rawJSON,graph)
    .then(function(){ fetchHydra(rawJSON,graph,req)
    .then(function(obj) { hydraDocument = obj.hydra; hydraParsedPromise(JSON.stringify(obj.hydra),obj.graph,'')
    .then(function(graph){ resolveAPI(graph,rawJSON)
    .then(function(resolved){  console.log('resolved',resolved);
      deferred.resolve(createObjects(rawJSON,graph,'other'));  },
      function(error) { console.log(error); } );
    },
      function(error) { console.log(error); } );
    },
      function(error) { console.log(error); } );
    },
      function(error) { console.log(error); } );

  console.log('Init Called');
  return deferred.promise;
};

schydraAngular['test'] = function(rawJSON,resolverServer,graph){
  var deferred = $q.defer();
  var req = {
    method: 'PUT',
    url: resolverServer,
    headers: {
    'Content-Type': 'application/ld+json'
    },
    data: rawJSON
  };
    parseInput(rawJSON,graph)
    .then(function(){ fetchHydra(rawJSON,graph,req)
    .then(function(obj) { hydraDocument = obj.hydra; hydraParsedPromise(JSON.stringify(obj.hydra),obj.graph,'')
    .then(function(graph){ resolveAPI(graph,rawJSON)
    .then(function(resolved){  console.log('resolved',resolved);
      deferred.resolve(createProtos(graph,null,rawJSON));  },
      function(error) { console.log(error); } );
    },
      function(error) { console.log(error); } );
    },
      function(error) { console.log(error); } );
    },
      function(error) { console.log(error); } );

  console.log('Init Called');
  return deferred.promise;
};

// Retrieve Literals from hydra response
schydraAngular['getLiterals'] = function(graph) {
  return parseHydra(graph);
}
// Retrieve JSON-LD format
schydraAngular['getJsonld'] = function() {
  return hydraDocument;
}

  return schydraAngular;
}]);//END of anjular Controller console.log('Raw Json Answer:',JSON.stringify(obj.hydra));
