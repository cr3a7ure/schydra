// app/scripts/controllers/angClient.js
'use strict';
// Client for our testing
// Automated API discovery based on schemantics and vocabularies


blogApp.controller('ReasonerCtrl', ['$log', '$scope','schydraAngular','$http', '$q', function ($log,$scope,schydraAngular, $http, $q) {

//  console.log('initiate');
  var rawJSON = {
    "@context" : {
      "expects" : {
        "@id" : "http://www.w3.org/ns/hydra/core#expects",
        "@type" : "@id"
      },
      "returns" : {
        "@id" : "http://www.w3.org/ns/hydra/core#returns",
        "@type" : "@id"
      },
      "method" : {
        "@id" : "http://www.w3.org/ns/hydra/core#method"
      },
      "title" : {
        "@id" : "http://www.w3.org/ns/hydra/core#title"
      },
      "label" : {
        "@id" : "http://www.w3.org/2000/01/rdf-schema#label"
      },
      "writable" : {
        "@id" : "http://www.w3.org/ns/hydra/core#writable",
        "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
      },
      "required" : {
        "@id" : "http://www.w3.org/ns/hydra/core#required",
        "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
      },
      "readable" : {
        "@id" : "http://www.w3.org/ns/hydra/core#readable",
        "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
      },
      "property" : {
        "@id" : "http://www.w3.org/ns/hydra/core#property",
        "@type" : "@id"
      },
      "object": {
        "@id": "schema:object",
        "@type": "@id"
      },
      "result": {
        "@id": "schema:result",
        "@type": "@id"
      },
      "target": {
        "@id": "schema:target",
        "@type": "@id"
      },
      "query": {
        "@id": "schema:query"
      },
      "description" : {
        "@id" : "http://www.w3.org/ns/hydra/core#description"
      },
      "supportedProperty" : {
        "@id" : "http://www.w3.org/ns/hydra/core#supportedProperty",
        "@type" : "@id"
      },
      "supportedOperation" : {
        "@id" : "http://www.w3.org/ns/hydra/core#supportedOperation",
        "@type" : "@id"
      },
      "@vocab" : "http://schema.org/",
      "schema" : "http://schema.org/",
      "hydra" : "http://www.w3.org/ns/hydra/core#",
      "xmls" : "http://www.w3.org/2001/XMLSchema#",
      "owl" : "http://www.w3.org/2002/07/owl#",
      "rdf" : "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      "xsd" : "http://www.w3.org/2001/XMLSchema#",
      "rdfs" : "http://www.w3.org/2000/01/rdf-schema#",
    },
    "offer": {
      "@type": "Offer",
      "priceCurrency": '',
      "price": '',
      "seller": '',
      "itemOffered": { "@type": "Flight" },
      "potentialAction": {
        "@type": "searchAction",
        "query": {"@type": "schema:Flight"},
        "object": "schema:Offer",
        "result": "schema:Offer",
        "target": "schema:Offer"
      }
    },
    "flight": {
      "@type": "Flight",
      "arrivalAirport": {"@type": "Airport"},
      "departureAirport": {"@type": "Airport"},
      "departureTime": {"@type": "xmls:dateTime"},
      "arrivalTime": {"@type": "xmls:dateTime"},
      "provider": ""
    },
    "postalAdress": {
      "@type": "PostalAddress",
      "addressCountry": '',
      "addressLocality": ''
    },
    "airport": {
      "@type": "Airport",
      "iataCode": '',
      "address": { "@type": "PostalAddress" },
      "geo": {"@type": "GeoCoordinates"},
      "potentialAction": {
        "@type": "searchAction",
        "object": "schema:Airport",
        "result": "schema:Airport",
        "target": "schema:Airport",
        "query": {"@type": "schema:PostalAddress"}
      }
    },
    "poi": {
      "@type": "TouristAttraction",
      "name": "",
      "description": "",
      "url": "",
      "hasMap": "",
      "geo": {"@type": "GeoCoordinates"},
      "image": "",
      "potentialAction": {
        "@type": "searchAction",
        "object": "schema:TouristAttraction",
        "result": "schema:TouristAttraction",
        "target": "schema:TouristAttraction",
        "query": {"@type": "schema:GeoCoordinates"}
      }
    },
    "geoloc": {
      "@type": "GeoCoordinates",
      "latitude": "",
      "longitude": ""
    },
    "hotels": {
      "@type": "Hotel",
      "name": "",
      "description": "",
      "address": {"@type":"schema:PostalAddress"},
      "faxNumber": "",
      "telephone": "",
      "geo": {"@type": "schema:GeoCoordinates"},
      "potentialAction": {
        "@type": "searchAction",
        "object": "schema:Hotel",
        "result": "schema:Hotel",
        "target": "schema:Hotel",
        "query": {"@type": "schema:GeoCoordinates"}
      }
    }
  };

/*
    "hotels": {
      "@type": "Hotel",
      "name": "",
      "description": "",
      "address": {"@type":"schema:PostalAddress"},
      "faxNumber": "",
      "telephone": "",
      "geo": {"@type": "schema:GeoCoordinates"},
      "starRating": {"@type": "schema:starRating"},
      "makesOffer": "",
      "priceRange": {"@type": "schema:PriceSpecification"},
      "award": "",
      "potentialAction": {
        "@type": "searchAction",
        "object": "schema:Hotel",
        "result": "schema:Hotel",
        "target": "schema:Hotel",
        "query": {"@type": "schema:GeoCoordinates"}
      }
    }
 */;
//  console.log(schydraAngular);
//function assignActions(actionLiterals,hydraObject,action,referenceObject,instance,graph) {

var getBase = function(str) {
  var parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
  var parts = parse_url.exec( str );
//  console.log('Url BASE',parts);
  var result = parts[1]+':'+parts[2]+parts[3];
  result = parts[4] ? result+':'+parts[4] : result;
  return result;
};
//console.log('LOL',getBase('http://vps454845.ovh.net:8084/path1/path2/path3/docs.jsonld#Airport'));
// 'http://vps454845.ovh.net:8084/path1/path2/path3/docs.jsonld#Airport'

// // main
  $scope.log = $log;
  var resolverServer = 'http://vps454845.ovh.net:8091/api_ref/special';
  // var resolverServer = 'http://0.0.0.0:8091/api_ref/special';
  // var entrypointList = ['http://localhost:8091'];
  var graph = $rdf.graph();
  var today = new Date();
  // $scope.retrievedPois = [];
  // $scope.poisLength = 0;

  var schydra = schydraAngular.test(rawJSON,resolverServer,graph);
  schydra.then(function(hydraObj){

//    console.log('hydraObj',hydraObj);
    var classLiterals = hydraObj['classLiterals'];
    var collectionLiterals = hydraObj['collectionLiterals'];
    var hyClass = hydraObj['hyClass'];
    var hyCollection = hydraObj['hyCollection'];
    var hyBind = hydraObj['hyBind'];
    var hyAuto = hydraObj['hyAuto'];
    var hyParse = hydraObj['hyParse'];
//    console.log('hyClass',hyClass);
    // var ar = [];
    // var tt = new hyCollection.TouristAttraction('vps454845.ovh.net:8085');
    // console.log('tt',tt);
    // for (var i =0 ; i<5; i++) {
    //   var temp = new hyClass.TouristAttraction('vps454845.ovh.net:8085/'+i);
    //   // temp.name = 'Skata'+i;
    //   temp.name = 'Skata'+i;
    //   // temp.name = 'Skata'+i;
    //   ar.push(temp);
    // }
    // ar.forEach(function(rr,k){
    //   // console.log('NAME',rr.props.name.value);
    //   // console.log('NAME',rr.name);
    // })
    // console.log('ar',ar);
    // var poiTest = new hyClass['TouristAttraction']('vps454845.ovh.net:8085');
    // console.log('test',poiTest);
    // var poi = new hyClass.TouristAttraction('vps454845.ovh.net:8085');
    // poi.set('name','mlk1');
    // var poi2 = new hyClass.TouristAttraction('vps454845.ovh.net:8085');
    // poi2.set('name','mlk2');
    // 
    // 
    // var poi23 = new hyBind.TouristAttraction('vps454845.ovh.net:8085');
    // poi23.set('name','mlk2');
    // console.log('poi23',poi23);
    var temp = hyAuto('TouristAttraction');
    temp.name = 'alala';
//    console.log('temp',temp);

    // console.log('poi User Object',poi.get('name'));
    // console.log('poi2 User Object',poi2.get('name'));
    // var testy = test.action('schemaDelete');
    // console.log('testy',testy);
    // var poiSearch = poi.action('schemasearch',hydraObj[4]);
    // 
    // var poiSearch = temp.action('schemaSearch');
    // poiSearch
    // .then(function(response){
    //   console.log('POI Response',response);
    //   $scope.retrievedPois = response['parsedObjects'];
    //   $scope.poisLength = $scope.retrievedPois.length;
    // });

    // // .catch(function(err) {
    // //   console.log(err);
    // // });
    // var flights = hyAuto('Offer');
    // console.log('flight offers user object',flights);
    // var flightSearch = flights.action('schemaSearch');
    // console.log('flightSearch',flightSearch);
    // flightSearch
    // .then( function(results){
    //   console.log('Flights Response',results);
    //   $scope.retrievedOffers = results['parsedObjects'];
    //   $scope.offersLength = $scope.retrievedOffers.length;
    // })
    // .catch(function(err) {
    //   console.log('Something failed',err);
    // });
    // var hotels = hyAuto('Hotel');
    // console.log('Hotel offers user object',hotels);
    // var hotelSearch = hotels.action('schemaSearch');
    // hotelSearch.then(function(results){
    //   console.log('Hotel Response',results);
    //   $scope.retrievedHotels = results['parsedObjects'];
    //   $scope.hotelsLength = $scope.retrievedHotels.length;
    // })
    // .catch(function(err) {
    //   console.log(err);
    // });


//   var schydra = schydraAngular.init(rawJSON,resolverServer,graph);
//   schydra.then(function(hydraClass){
//     $scope.log = schydraAngular.log;
//     $scope.error = schydraAngular.error;

//Create User/Depart GeoLocation
    var location = hyAuto('GeoCoordinates');
    location.label = "Current Location";
    location.latitudeLabel = 'Latitude';
    location.longitudeLabel = 'Longitude';
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function(position) {
        location.set('latitude',position.coords.latitude);
        location.set('longitude',position.coords.longitude);
      });
    } else {
      location.set('latitude',"");
      location.set('longitude', "");
    }
// //Create User/Depart GeoLocation
    var destinationGeo = hyAuto('GeoCoordinates');
    destinationGeo.label = "Destination Location";
    destinationGeo.latitudeLabel = 'Latitude';
    destinationGeo.longitudeLabel = 'Longitude';
    // console.log('User/Departure Location',location);

// //Search Address
    var searchAddress = hyAuto('PostalAddress');
    searchAddress.label = 'Departure address';
    searchAddress.addressCountry = 'GR';
    searchAddress.addressCountryLabel = 'Country Code';
    searchAddress.addressLocality = 'Chios';
    searchAddress.addressLocalityLabel = 'City';

//search Airport
    var searchAirport = hyAuto('Airport');
    searchAirport.label = 'Search Airport';
    searchAirport.iataCode = '';
    searchAirport.iataCodeLabel = 'IATA code';
    searchAirport.address = searchAddress;
    searchAirport.geo = location;

//Departure Address
    var departureAddress = hyAuto('PostalAddress');
    departureAddress.label = 'Departure address';
    departureAddress.addressCountry = 'GR';
    departureAddress.addressCountryLabel = 'Country Code';
    departureAddress.addressLocality = 'ath';
    departureAddress.addressLocalityLabel = 'City';

// //Departure Airport
    var departureAirport = hyAuto('Airport');
    departureAirport.label = 'Departure Airport';
    departureAirport.iataCode = 'NYC';
    departureAirport.iataCodeLabel = 'IATA code';
    departureAirport.address = departureAddress;
    departureAirport.geo = location;
//    console.log('departureAirport:',departureAirport);

// //Arrival Address
    var arrivalAddress = hyAuto('PostalAddress');
    arrivalAddress.label = 'destination address';
    arrivalAddress.addressCountryLabel = 'Country Code';
    arrivalAddress.addressLocalityLabel = 'City';

// //Arrival Airport
    var arrivalAirport = hyAuto('Airport');
    arrivalAirport.label = 'Arrival Airport';
    arrivalAirport.iataCode = 'LAX';
    arrivalAirport.iataCodeLabel = 'IATA code';
    arrivalAirport.address = arrivalAddress;
    // arrivalAirport.set('geo',location);
//    console.log('arrivalAirport',arrivalAirport);

    var pois = hyAuto('TouristAttraction');
    pois.label = 'Points of Interest';
    pois.geo = destinationGeo;// = destinationGeo.value;

    var hotels = hyAuto('Hotel');
    hotels.label = 'Available hotels in the area';
    hotels.geo = destinationGeo;
//     console.log('hotels',hotels);

//     $scope.retrievedPois = {};
//     $scope.fetchPois = function() {
//       pois.actions.searchAction(graph,'').then(function(response){
//           $scope.retrievedPois = response;
//         });
//     };
// //Sun Apr 09 2017 02:03:00 GMT+0300 (EEST)

// //Helper function to return next week!
//     // function nextweek(){
//       // var today = new Date();
//       // var nextweek = new Date(today.getFullYear(), today.getMonth(), today.getDate()+7);
//     //   return nextweek;
//     // }
//       $scope.selectedDeparture = {};
//         // console.log(arrivalAirport.actions.searchAction(graph,''));

    var today =new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
    var oneWeek = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate()+7);
    // var oneWeek = new Date(date.getTime() + 24 * 60 * 60 * 1000);
// //Flight
    var searchFlight = hyAuto('Flight');
    searchFlight.label = 'Flight query';
    searchFlight.departureTime = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate());
    searchFlight.departureTimeLabel = 'Departure Date';
    searchFlight.arrivalTime = new Date(oneWeek.getFullYear(), oneWeek.getMonth(), oneWeek.getDate());;
    searchFlight.arrivalTimeLabel = 'Arrival Date ';
    // // console.log('date test',searchFlight.value.arrivalTime.get());
    searchFlight.departureAirport = departureAirport;
    searchFlight.departureAirportLabel = 'Departure Airport ';
    searchFlight.arrivalAirport = arrivalAirport;
    searchFlight.arrivalAirportLabel = 'Arrival Airport ';
//    console.log('searchFlight',searchFlight);

    var flightOffers = hyAuto('Offer');
    flightOffers.itemOffered = searchFlight;
    flightOffers.label = 'flight search query';

    $scope.searchAddress = searchAddress;
    $scope.searchFlight = searchFlight;
    $scope.departureAirport = departureAirport;
    $scope.arrivalAirport = arrivalAirport;
    $scope.input = arrivalAirport;
    $scope.offer = flightOffers;
    $scope.retrievedAirport = {};
    $scope.retrievedOffers = [];
    $scope.retrievedHotels = [];
    $scope.retrievedPois = [];

//Data Counters
    $scope.offersLength = 0;
    $scope.hotelsLength = 0;
    $scope.poisLength = 0;

    $scope.fetchOffers = function() {
        // Fetch Flight Offers
//        console.log('$scope.offer',$scope.offer);
        toggleBackdrop();
        var promise_flights = $scope.offer.action('schemaSearch').then(function(response){
//            console.log('response',response);
            $scope.offer.members.forEach(function(member,o){
              if (!(member.itemOffered.departureAirport.address)) {
                member.itemOffered.departureAirport = $scope.departureAirport;
                member.itemOffered.arrivalAirport = $scope.arrivalAirport;
              }
            });
            $scope.retrievedOffers = $scope.offer.members ;
            $scope.offersLength = $scope.retrievedOffers.length;
//            console.log('FORMATED DATA:',$scope.retrievedOffers);
        });
//        console.log(promise_flights);
        //Fetch POIs
        var promise_pois = pois.action('schemaSearch').then(function(response){
            $scope.retrievedPois = pois.members;
//            console.log('POIS RETR',$scope.retrievedPois);
            $scope.poisLength = $scope.retrievedPois.length;
            // $scope.retrievedPois = response;
        });
        //Fetch Hotels
        var promise_hotels = hotels.action('schemaSearch').then(function(response){
            $scope.retrievedHotels = hotels.members;
//            console.log('HOTELS RETR',$scope.retrievedHotels);
            $scope.hotelsLength = $scope.retrievedHotels.length;
        });
//        console.log('PLH8OS',$scope.retrievedHotels.length);

        // $scope.retrievedAirport = response['data']['hydra:member'];
//        console.log('w8 for Offers! w8!');
//        console.log($scope.departureAirport);

        Promise.all([promise_flights, promise_pois, promise_hotels]).then(values => {
            $scope.$apply();
            toggleBackdrop();
        });
    };
    
function toggleBackdrop() {
    if ($('.modal-backdrop').length > 0) {
        $('.modal-backdrop').remove();
    } else {
        $('body').append('<div class="modal-backdrop fade show"></div>');
    }
    $('i.my-spinner ').toggle();
}


//       function isContext(str) {
//         var reg = new RegExp('^@');
//         return reg.test(str);
//       }


      // var assign = function(obj,model,returnObject) {
      //   var classType = obj['@type'].split('/').pop();
      //   var returnObj;
      //   if((returnObject === null)||(typeof returnObject === "undefined")||(returnObject === '')) {
      //     if (model[classType]) {
      //       returnObj = new model[classType];
      //     } else {
      //       return null;
      //     }
      //   } else {
      //     returnObj = returnObject;
      //   }
      //   // console.log('EXW obj:',obj);
      //   returnObj['@id'] = obj['@id'];
      //   Object.keys(obj).forEach(function(prop){
      //     if (!(isContext(prop))) {
      //        // returnObj.value[prop] = {};
      //       if (( typeof obj[prop] === "object")&&( obj[prop])) {
      //         // console.log('Exw nested', prop);
      //         returnObj.set(prop,assign(obj[prop],model));
      //       } else {
      //         returnObj.set(prop,obj[prop]);
      //       }
      //     }
      //   });
      //   return returnObj;
      // };

      // //AutoComplete
      $scope.assignDepartureAirport = function(data) {
        // console.log('LOL',data);
        if(data.hasOwnProperty('originalObject')) {
          $scope.departureAirport = data.originalObject;
          $scope.searchFlight.departureAirport = $scope.departureAirport;
        }
      };

      $scope.assignArrivalAirport = function(data) {
        // console.log('LOL',data);
        if(data.hasOwnProperty('originalObject')) {
          $scope.arrivalAirport = data.originalObject;
          $scope.searchFlight.arrivalAirport = $scope.arrivalAirport;
          pois.geo = $scope.arrivalAirport.geo;
          hotels.geo = $scope.arrivalAirport.geo;
        }
      };

      //Search Airport
      $scope.localSearch = function(str) {
        var matches = [];
        var results = $q.defer();
        searchAddress.addressLocality = str;
//        console.log('search Address',searchAddress.addressLocality);
        searchAirport.address = searchAddress;
          searchAirport.action('schemaSearch').then(function(response){
//            console.log('autocomplete Response:',response);
            response.parsedObjects.forEach(function(obj,i){
//              console.log('IATA:',obj.iataCode);
              // var temp = JSON.parse(JSON.stringify(assign(response[i],hydraClass)));
              matches.push(obj);
            });
            // console.log(matches);
            results.resolve(matches);
          });
        return results.promise;
      };

      // console.log($scope.input);
      // console.log($log);
  });//end of init>?!?!

  // console.log('SCHYDRA Promise, Classes',schydra);

  }]);//END of anjular Controller
