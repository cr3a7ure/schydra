// helper doce
// 
function arraysEqual(arr1, arr2) {
  if(arr1.length !== arr2.length)
    return false;
  for(var i = arr1.length; i--;) {
    if(arr1[i] !== arr2[i])
        return false;
}

  return true;
}
  // http://stackoverflow.com/questions/4025893/how-to-check-identical-array-in-most-efficient-way
  // 
  // NOT USED 
  /*
Return contained classes from our data.
1. We may have already classes with properties,
  requested or given.
2. We may have only properties, that need to be matched
  against schema.org Classes so that we can query server
 */
/**
 * [returnContainedClasses description]
 * @param  {[type]} data   [description]
 * @param  {[type]} schema [description]
 * @return {[type]}        [description]
 */
function returnContainedClasses(data,schema) {
  //TEMP SCHEMA RELS
  console.log("data input ",data)
  var returnClasses = {}
  var containedClasses = []
  var orphanProps = [];
  var classList = [];
  // For every data Object, we do not know if it's a
  // property or class
  //Return doaminIncludes for properties
  Object.keys(data).forEach(function(dataObj){
    // if (data[dataObj].hasOwnProperty("value")) {
    if (isDatatype(data[dataObj],datatypeList)) {
      //we have property 0 level
      console.log("0-level property:", dataObj)
      orphanProps.push(dataObj)
        /*
        If we have empty class, add the domainIncludes classes
        should we check for the number of matches? per class?
         */
    } else {
      //We suppose we have Class
      console.log("Input class with properties",dataObj)
        containedClasses.push(dataObj) // save CLASS
        classList.push(dataObj)
    }
    Object.keys(data[dataObj]).forEach(function(prop){
        // data[dataClass][prop] rangeIncludes
        // add rangeIncludes structured data to classes
        // containedClasses.push(dataClass.uri) // save CLASS
    })
    //for every class go check props and find structured data CLASSES
    // gia ka8e CLASS pou einai adeia-den exei class, trava thn domain class
    //
  }) //endOfIteration
  // GO find matching classes for properties
  var guessedClasses = domainInc(orphanProps,schema)
  guessedClasses.forEach(function(cl,i){
    guessedClasses[i].uri = guessedClasses[i].uri.toClassName()
    classList.push(guessedClasses[i].uri.toClassName());
  })
  console.log("guessedClasses",guessedClasses)
  console.log("containedClasses",containedClasses)
  console.log("orphanProps",orphanProps)
  console.log('classList',classList)
  return classList
  // return returnClasses
}

/*
Create new array per Class rather than per server
for each class, we select its server and rank
 */
function arrangePerClass(serversArray,arrayOfClasses,dataEdited) {
  // console.log("serversArray",serversArray)
  // console.log('arrayOfClasses',arrayOfClasses)
  // console.log('dataEdited',dataEdited)
  var ObjPerClass = {};
  arrayOfClasses.forEach(function(cl){
    ObjPerClass[cl] = arrayOfClasses[cl];
    Object.keys(serversArray).forEach(function(svr){
      ObjPerClass[cl] = {};
      ObjPerClass[cl]['servers'] = {};
      Object.keys(serversArray[svr]).forEach(function(svrCl){
        // console.log("svrCl",svrCl + " " + timeTest)
        if(cl==svrCl) {
          ObjPerClass[cl]['servers'][svr] = {};
          ObjPerClass[cl]['servers'][svr]['rank'] = serversArray[svr]["rank"];
        }
      })
    })
  })
  console.log("ObjPerClass",ObjPerClass);
  return ObjPerClass;
};


/*
return Hydra Api classes as objects
 */
function hydraClasses(hydraObject) {
  var classObj = {};
  var classes = hydraObject['hydra:supportedClass'];
  var supportedClasses = [];
  // console.log(classes[0]['hydra:supportedProperty'][0]['hydra:property']);
  // For Each supported Class - Object, create an object for every entity and it's properties
  classes.forEach(function(clValue, clKey) {
    // console.log(key+" : "+value);
    if ((clValue['rdfs:label'])){
      var temp = clValue['hydra:title'];
      var obj = {};
      // obj['propsName'] = temp;
      // classObj[clKey]['propName'] = clValue['hydra:title'];
      // classObj.description = clValue['hydra:description'];
      var property = clValue['hydra:supportedProperty'];

      property.forEach(function(propValue, propKey) {
        var propRange = propValue['hydra:property']['range'];
        var propName = propValue['hydra:property']['rdfs:label'];
        // console.log(propRange);
        // console.log(propName);
        obj[propName] = {
            type: propRange
        }
      });
      classObj[temp] = obj;
      // console.log(clValue);
      // supportedClasses.push(value['hydra:title']);
    }
  });
  return classObj;
 }

