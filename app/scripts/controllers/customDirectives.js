// app/scripts/controllers/customDirectives.js
'use strict';

blogApp.directive('hyAddress', function() {
  return {
    // restrict: 'EA',
        scope: {
            postal: '='
            // add: '&',
        },
    template: '<address>' +
        '<p> <strong> {{ postal.streetAddressLabel ? postal.streetAddressLabel : "Street address"}} : </strong> {{postal.streetAddress}}<br>' +
        ' <strong> {{ postal.addressLocalityLabel ? postal.addressLocalityLabel : "City"}} : </strong> {{postal.addressLocality}}<br>' +
        ' <strong> {{ postal.addressRegionLabel ? postal.addressRegionLabel : "Region"}} : </strong> {{postal.addressRegion}}<br>' +
        ' <strong> {{ postal.postalCodeLabel ? postal.postalCodeLabel : "Postal code"}} : </strong> {{postal.postalCode}}<br>' +
        ' <strong> {{ postal.addressCountryLabel ? postal.addressCountryLabel : "Country"}} : </strong> {{postal.addressCountry}}</p><br>' +
    '</address>'
  };
});

blogApp.directive('hyPostaladdress', function() {
  return {
    // restrict: 'EA',
        scope: {
            postal: '='
            // add: '&',
        },
    template: '<div class="form-inline">' +
        '<label> {{postal.addressCountryLabel}} </label>' +
        '<input type="text" name={{propClass}} placeholder={{postal.addressCountry}} class="form-control" ng-model="postal.addressCountry">' +
       '<label> {{postal.addressLocalityLabel}} </label>' +
        '<input type="text" name={{propClass}} placeholder={{postal.addressLocality}} class="form-control" ng-model="postal.addressLocality">' +
    '</div>'
  };
});

blogApp.directive('hyGeoloc', function() {
  return {
    // restrict: 'EA',
        scope: {
            geo: '='
            // add: '&',
        },
    template: '<div class="jumbotron">' +
        '<p>{{geo.latitude}}</p>'+
        '<p>{{geo.longitude}}</p>'+
        '<p></p>'+
        '</div>'
  };
});

blogApp.directive('hyAirport', function() {
  return {
    // restrict: 'EA',
        scope: {
            airport: '='
            // add: '&',
        },
    template: '<div hy-Postaladdress postal="airport.address">test asddres </div>' +
        '<div class="form-inline">'+
        '<label> {{airport.iataCodelabel}} </label>' +
        '<input type="text" name={{airport.iataCode}} placeholder={{airport.iataCode}} class="form-control" ng-model="airport.iataCode">'+
        '</div>'
  };
});

blogApp.directive('hyAirportshow', function() {
  return {
    // restrict: 'EA',
        scope: {
            airport: '='
            // add: '&',
        },
    template: '<div hy-Postaladdress postal="airport.address">test asddres </div>' +
        '<div class="form-inline">'+
        '<label> {{airport.iataCodeLabel}} </label>' +
        '<input type="text" name={{airport.iataCode}} placeholder={{airport.iataCode}} class="form-control" ng-model="airport.iataCode">'+
        '</div>'
  };
});



blogApp.directive('hyAttraction', function() {
  return {
    // restrict: 'EA',
        scope: {
            attraction: '='
            // add: '&',
        },
    template:
    '<div class="col-sm-6 col-md-4 col-lg-3 " >'+
    '<div class="card" style="width: 20rem;">'+
        '<img class="card-img-top" src={{attraction.image}} alt=attraction.imageLabel>'+
        '<div class="card-block" >'+
        '<h4 class="card-title"> {{attraction.name}}</h4>'+
        '<p class="card-text">{{attraction.description}}</p>'+
        '<br> <i class="fa fa-map" aria-hidden="true"></i><a href={{attraction.hasMap}} class="text-primary"> map</a>'+
        '<br><i class="fa fa-link" aria-hidden="true"></i><a href={{attraction.url}} class="text-primary"> more</a>'+
        // ' <div hy-Geoloc geo="attraction.value.geo.value">testfl </div>'+
        '</div>'+
        '</div>'+
        '</div>'
  };
});

blogApp.directive('hyHotel', function() {
  return {
    // restrict: 'EA',
        scope: {
            hotel: '='
            // add: '&',
        },
    template: 
        '<div class="col-sm-6 col-md-4 col-lg-3 " >'+
    '<div class="card" style="width: 20rem;">'+
            '<img class="card-img-top" src="../../images/hotel-placeholder.jpg" alt=attraction.imageLabel>'+
        '<h4 class="card-title"> {{hotel.name}}</h4>'+
        '<p>{{hotel.description}}</p>'+
        '<div class="panel panel-info">'+
        '<div class="panel-heading">' +
           '<h4 class="panel-title">Contact Information</h4>' +
          '</div>' +
          '<div class="panel-body">'+
            '<i class="fa fa-phone" aria-hidden="true"></i> : {{hotel.telephone}}<br>'+
            '<i class="fa fa-fax" aria-hidden="true"></i> : {{hotel.faxNumber}}'+
          '</div>'+
        '</div>'+
          ' <div hy-Address postal=hotel.address>testfl </div>'+
        '</div>'+
        '</div>'
  };
});

// blogApp.directive('hyFlightResult', function() {
//   return {
//     // restrict: 'EA',
//         scope: true,
//         //{
//           //  flight: '='
//             // add: '&',
//         //},
//     template:
//         '<h4 class="card-title"><i class="fa fa-fighter-jet" aria-hidden="true"></i> Departure: {{offer.itemOffered.departureTime.toLocaleDateString("en-GB")}} </h4>'+
//         '<table class="table">'+
//         '<thead>'+
//             '<tr>'+
//                 '<th>From</th>'+
//                 '<th>To</th>'+
//                 '<th>Time</th>'+
//                 '<th>Airline</th>'+
//             '</tr>'+
//         '</thead>'+
//         '<tbody>'+
//             '<tr>'+
//                 '<td>{{offer.itemOffered.departureAirport.address.addressLocality}}, {{offer.itemOffered.departureAirport.address.addressCountry}} ({{offer.itemOffered.departureAirport.iataCode}})</td>'+
//                 '<td>{{offer.itemOffered.arrivalAirport.address.addressLocality}}, {{offer.itemOffered.arrivalAirport.address.addressCountry}} ({{offer.itemOffered.arrivalAirport.iataCode}})</td>'+
//                 //'<td class="text-justify">{{offer.itemOffered.departureTime.getHours()+ ":"+ offer.itemOffered.departureTime.getMinutes()}}</td>'+
//                 '<td class="text-justify">{{offer.itemOffered.departureTime.toLocaleTimeString("en-US")}}</td>'+
//                 '<td class="text-justify">{{offer.itemOffered.provider}}</td>'+
//             '</tr>'+
//         '</tbody>'+
//     '</table>'+
//     '<h4 class="card-title"><i class="fa fa-fighter-jet fa-flip-horizontal" aria-hidden="true"></i>  Return: {{offer.itemOffered.arrivalTime.toLocaleDateString("en-GB")}} </h4>'+
//         '<table class="table">'+
//         '<thead>'+
//             '<tr>'+
//                 '<th>From</th>'+
//                 '<th>To</th>'+
//                 '<th>Time</th>'+
//                 '<th>Airline</th>'+
//             '</tr>'+
//         '</thead>'+
//         '<tbody>'+
//             '<tr>'+
//                 '<td>{{offer.itemOffered.arrivalAirport.address.addressLocality}}, {{offer.itemOffered.arrivalAirport.address.addressCountry}} ({{offer.itemOffered.arrivalAirport.iataCode}})</td>'+
//                 '<td>{{offer.itemOffered.departureAirport.address.addressLocality}}, {{offer.itemOffered.departureAirport.address.addressCountry}} ({{offer.itemOffered.departureAirport.iataCode}})</td>'+
//                 '<td class="text-justify">{{offer.itemOffered.arrivalTime.toLocaleTimeString("en-US")}}</td>'+
//                 '<td class="text-justify">{{offer.itemOffered.provider}}</td>'+
//             '</tr>'+
//         '</tbody>'+
//     '</table>'
//     // '{{offer.itemOffered.value.arrivalAirport.value.value.address.value.value.addressLocality.value}}'+
//     // '<div hy-Datetime value=offer.itemOffered.value.departureTime.value> </div>'+
// //    '<h5 class="card-title bg-info text-white"><i class="fa fa-fighter-jet fa-flip-horizontal" aria-hidden="true"></i> Return: {{offer.itemOffered.arrivalTime.getDate() + "/" + (offer.itemOffered.arrivalTime.getMonth()+1) + "/" + offer.itemOffered.arrivalTime.getFullYear() }} </h5>'
//   };
// });

blogApp.directive('hyFlightResult', function() {
  return {
    // restrict: 'EA',
        scope:
        {
           flight: '='
            // add: '&',
        },
    template:
        '<h4 class="card-title"><i class="fa fa-fighter-jet" aria-hidden="true"></i> Departure: {{flight.departureTime.toLocaleDateString("en-GB")}} </h4>'+
        '<table class="table">'+
        '<thead>'+
            '<tr>'+
                '<th>From</th>'+
                '<th>To</th>'+
                '<th>Time</th>'+
                '<th>Airline</th>'+
            '</tr>'+
        '</thead>'+
        '<tbody>'+
            '<tr>'+
                '<td>{{flight.departureAirport.address.addressLocality}}, {{flight.departureAirport.address.addressCountry}} ({{flight.departureAirport.iataCode}})</td>'+
                '<td>{{flight.arrivalAirport.address.addressLocality}}, {{flight.arrivalAirport.address.addressCountry}} ({{flight.arrivalAirport.iataCode}})</td>'+
                //'<td class="text-justify">{{flight.departureTime.getHours()+ ":"+ flight.departureTime.getMinutes()}}</td>'+
                '<td class="text-justify">{{flight.departureTime.toLocaleTimeString("en-US")}}</td>'+
                '<td class="text-justify">{{flight.provider}}</td>'+
            '</tr>'+
        '</tbody>'+
    '</table>'+
    '<h4 class="card-title"><i class="fa fa-fighter-jet fa-flip-horizontal" aria-hidden="true"></i>  Return: {{flight.arrivalTime.toLocaleDateString("en-GB")}} </h4>'+
        '<table class="table">'+
        '<thead>'+
            '<tr>'+
                '<th>From</th>'+
                '<th>To</th>'+
                '<th>Time</th>'+
                '<th>Airline</th>'+
            '</tr>'+
        '</thead>'+
        '<tbody>'+
            '<tr>'+
                '<td>{{flight.arrivalAirport.address.addressLocality}}, {{flight.arrivalAirport.address.addressCountry}} ({{flight.arrivalAirport.iataCode}})</td>'+
                '<td>{{flight.departureAirport.address.addressLocality}}, {{flight.departureAirport.address.addressCountry}} ({{flight.departureAirport.iataCode}})</td>'+
                '<td class="text-justify">{{flight.arrivalTime.toLocaleTimeString("en-US")}}</td>'+
                '<td class="text-justify">{{flight.provider}}</td>'+
            '</tr>'+
        '</tbody>'+
    '</table>'
    // '{{flight.value.arrivalAirport.value.value.address.value.value.addressLocality.value}}'+
    // '<div hy-Datetime value=flight.value.departureTime.value> </div>'+
//    '<h5 class="card-title bg-info text-white"><i class="fa fa-fighter-jet fa-flip-horizontal" aria-hidden="true"></i> Return: {{flight.arrivalTime.getDate() + "/" + (flight.arrivalTime.getMonth()+1) + "/" + flight.arrivalTime.getFullYear() }} </h5>'
  };
});

// blogApp.directive('hyDatetime', function() {
//   return {
//     // restrict: 'EA',
//         scope: {
//             value: '='
//             // add: '&',
//         },
//         link: function (tElm,tAttrs){
//           var date = new Date();
//           date = date.parse(value);
//           date = date.toDateString();
//           tElm.html(date);
//         }
//       };
// });


blogApp.directive('hyOffer', function() {
  return {
    // restrict: 'EA',
        scope: {
            offer: '='
            // add: '&',
        },
    template: '' +
        '<div class="container" >'+
            '<div class="card" style="min-width: 20rem;">' +
                '<div class="card-header">' +
                    '<h2 class="flight-provider mb-3 text-muted"> {{ offer.seller }} </span>' +
                    '<h5 ng-if="offer.priceCurrency === \'EUR\' ">' +
                        'Price: {{offer.price}} <i class="fa fa-eur" aria-hidden="true"></i>' +
                    '</h5>' +
                    '<h5 ng-if="offer.priceCurrency === \'USD\' ">' +
                        'Price: {{offer.price}} <i class="fa fa-usd" aria-hidden="true"></i>' +
                    '</h5>' +
                '</div>'+
                '<div class="card-body">'+
                     '<div hy-Flight-Result flight=offer.itemOffered ></div>'+
                '</div>'+
                '<div class = "card-footer">'+
                    '<small>API source: {{offer.source}}</small>'+
                '</div>'+
            '</div>'+
        '</div>'
  };
});

/*
<div class="panel panel-info">...</div>
 */

blogApp.directive('hyFlight', function() {
  return {
    // restrict: 'EA',
        scope: {
            flight: '='
            // add: '&',
        },
    template: '<div class="row">'+
  '<div class="col-sm-6 col-md-4 col-lg-3">'+
    '<div class="form-inline">'+
        '<label class="col-2 col-form-label"> {{flight.arrivalAirportLabel}} </label>&nbsp;' +
        '<input type="text" name={{flight.props.arrivalAirport}} placeholder={{flight.itemOffered.arrivalAirport.iataCode}} class="form-control mb-2 mr-sm-2 mb-sm-0" ng-model="flight.itemOffered.arrivalAirport.iataCode">'+
        '<label> {{flight.departureAirportLabel}} </label>&nbsp;' +
        '<input type="text" name={{flight.props.departureAirport}} placeholder={{flight.itemOffered.departureAirport.iataCode}} class="form-control mb-2 mr-sm-2 mb-sm-0" ng-model="flight.itemOffered.departureAirport.iataCode">'+
        '</div>'+
        '</div>'+
'<div class="col-sm-6 col-md-4 col-lg-3">'+
    '<div class="form-inline">'+
        '<label> {{flight.departureTimeLabel}} </label>&nbsp;' +
        '<input type="date" name={{flight.itemOfferd.departureTime}} placeholder={{flight.itemOffered.departureTime}} class="form-control" ng-model="flight.itemOffered.departureTime">'+
        '<label> {{flight.arrivalTimeLabel}} </label>&nbsp;' +
        '<input type="date" name={{flight.itemOfferd.arrivalTime}} placeholder={{flight.itemOffered.arrivalTime}} class="form-control" ng-model="flight.itemOffered.arrivalTime">'+
        '</div>'+
        '</div>'+
        '</div>'
  };
});