blogApp.controller('PlaygroundCtrl', ['$log', '$scope', 'schydraAngular', '$http', '$q', 'prettyJsonFilter', function ($log, $scope, schydraAngular, $http, $q, prettyJsonFilter) {

    var RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
    var RDFS = $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
    var FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
    var XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
    var SCHEMA = $rdf.Namespace("http://schema.org/");
    var HYDRA = $rdf.Namespace("http://www.w3.org/ns/hydra/core#");


// //Init popovers
    angular.element(document).ready(function () {
        $('a[data-toggle="pill"]').on('show.bs.tab', function (e) {
            $('[data-toggle="popover"]').popover();
        });
    });


    var hydraParsedPromise2 = function(hydraDoc,hydraGraph,hydraServer,mimeType,cb) {
      mimeType = mimeType ? mimeType : 'application/ld+json';
      var previousGraph;
      console.log('hydraGraph to save',hydraGraph);
      if(Array.isArray(hydraGraph.statements)) {
        previousGraph = hydraGraph.statements.length;
      } else {
        previousGraph = 1;
      }
      return new Promise(function(resolve, reject) {
          $rdf.parse(hydraDoc, hydraGraph, hydraServer,mimeType,function(error,kb){
            if ((error===null)&&(kb.statements.length>previousGraph)) {
              console.log('kb-length',kb.statements.length);
              // log += 'Added ' + kb.statements.length + ' statements to default graph\n';
              resolve(kb);
            } else {
              // reject(error);
            }
          });
      });
    };

    var hydraFrame = JSON.parse('{'+
      '"@context":{ "hydra": "http://www.w3.org/ns/hydra/core#" },'+
      '"@type": "hydra:Class",'+
      '"hydra:supportedProperty": {'+
        '"@type": "hydra:SupportedProperty"'+
        // '"contains": {'+
        //   '"@type": "hydra:SupportedProperty"'+
        // '}'+
      '},'+
      '"hydra:supportedOperation": {'+
        '"@type":"hydra:opertaion"'+
      '}'+
    '}');
    // var frame = JSON.parse(hydraFrame);

    var apiResolver = 'http://vps454845.ovh.net:8091';
    $scope.vocabArray = [];//{'http://schema.org': true,'http://schema.org2':false};
    $scope.apiClassLiterals = {};
    $scope.apiCollectionLiterals = {};

    $scope.bindClassNames = [];
    $scope.bindCollectionNames = [];
    $scope.bindCustomNames = [];

    $scope.testProp = {};
    $scope.testPropOptions = {};
    $scope.serverUrl = '';
    $scope.bindPropertiesNames = [];
    $scope.bindActionNames = {};
    $scope.bindCollectionActionNames = {};
    $scope.bindMethodNames = {};
    $scope.bindCollectionMethodNames = {};
    $scope.bindCustomMethodNames = {};
    $scope.bindCustomActionNames = {};
    $scope.instanceMethodFunctions = {};
    $scope.instanceActionFunctions = {};

    $scope.responseCode = 0;
    $scope.responseData = {};
    $scope.instanceResult = {};
    $scope.collectionResult = {};
    $scope.customResult = {};
    // $scope.instanceActionResult = {};
//
    $scope.instanceId = '';

    var browseGraph = $rdf.graph();
    var consumeGraph = $rdf.graph();
    var vocGraph = $rdf.graph();
    var defaultGrapf = $rdf.graph();

    function loadBrowserGui(graphObj,graphRDF,frameOn,discover,rawJSON) {
      console.log('loadBrowserGui');
      console.log('rawJSON',rawJSON);
      var parse;
      if (discover) {
        parse = schydraAngular.test(graphObj,(apiResolver+'/api_ref/special'),graphRDF);
      } else {
        parse = schydraAngular.getProtos(graphRDF,true,rawJSON);
      }
      // var graphObj;
      // graph as Object
      //Parse as RDF GRAPH
      hydraParsedPromise2(JSON.stringify(graphObj),graphRDF,'')
      .then(function(parsedGraph){
        graphRDF = parsedGraph;
        console.log('parsedGraph',parsedGraph);
        $scope.apiClassLiterals.legnth = {};
        $scope.apiCollectionLiterals.legnth = {};
        console.log('$scope.apiCollectionLiterals',$scope.apiCollectionLiterals);
        var literalArray = schydraAngular.getLiterals(graphRDF);
        [$scope.apiClassLiterals,$scope.apiCollectionLiterals] = literalArray;
        $scope.$apply();
        //Load Autocreated Binds
        var hydraObj = schydraAngular.getProtos(graphRDF,true,rawJSON);
        console.log('hydraObj',hydraObj);
        // console.log('hydraObj',hydraObj);
        var classLiterals = hydraObj['classLiterals'];
        var collectionLiterals = hydraObj['collectionLiterals'];
        console.log('classLiterals',classLiterals);
        console.log('collectionLiterals',collectionLiterals);
        var hyClass = hydraObj['hyClass'];
        var hyCollection = hydraObj['hyCollection'];
        var hyBind = hydraObj['hyBind'];
        var hyAuto = hydraObj['hyAuto'];
        var hyParse = hydraObj['hyParse'];
        // $scope.bindClassNames = Object.keys(hyClass);
        $scope.bindCollectionNames = Object.keys(hyCollection);
        $scope.bindCustomNames = Object.keys(hyBind);
        var baseUrl = Object.keys(collectionLiterals[Object.keys(collectionLiterals)[0]])[0];
        console.log('baseUrl',baseUrl);
        $scope.serverUrl = baseUrl;
        console.log('hyClass',hyClass);
        // console.log('hyClass',new hyClass['PostalAddress'](baseUrl));
        // console.log('hyCollection',new hyCollection['TouristAttraction'](baseUrl));
        Object.keys(hyClass).forEach(function(className,i){
          Object.keys(classLiterals[className]).forEach(function(server,o){
            if (server!=='common') {
              var baseUrl = server;
              var temp = new hyClass[className](baseUrl);
              console.log('temp',temp);
              $scope.bindClassNames.push(({'clName':className ,'server':server}));
              $scope.testProp[className] = Object.keys(temp['props'])[0];
              $scope.testPropOptions[className] = {};
              var tempOption = $scope.testProp[className]+'Range';
              $scope.testPropOptions[className]['Range'] = temp[tempOption];
              tempOption = $scope.testProp[className]+'Domain';
              $scope.testPropOptions[className]['Domain'] = temp[tempOption];
              tempOption = $scope.testProp[className]+'Desc';
              $scope.testPropOptions[className]['Desc'] = temp[tempOption];
              tempOption = $scope.testProp[className]+'Label';
              $scope.testPropOptions[className]['Label'] = temp[tempOption];
              $scope.bindActionNames[className] = Object.keys(temp.classLits.actions);
              $scope.bindMethodNames[className] = Object.keys(temp.classLits.operations);

              // var tt = new hyCollection['GeoCoordinates'](baseUrl);
              // console.log('INSTANCE',tt.constructor.name);
              // console.log('INSTANCEColl',tt);
              // console.log('INSTANCEColl',tt.members);

              $scope.instanceMethodFunctions[className] = function(methodName) {
                if ( $scope.instanceResult[className] instanceof hyClass[className] ){
                  console.log('Already iniated');
                } else {
                  $scope.instanceResult[className] = new hyClass[className](baseUrl);
                }
                $scope.instanceResult[className]['@id'] = 'http://vps454845.ovh.net:8085/postal_addresses/1';
                var methodRes = $scope.instanceResult[className].method(methodName);
                methodRes.then(function(resolved){
                  $scope.responseCode = resolved.response[baseUrl].status;
                  console.log('$scope.responseCode',$scope.responseCode);
                  $scope.responseData = resolved.response[baseUrl].data;
                  console.log('lala',resolved);
                  // $scope.instanceResult[className] = resolved.parsedObjects[0];
                  $scope.$apply();
                },
                function(resolved){
                  $scope.responseCode = resolved.response[baseUrl].status;
                  $scope.responseData = resolved.response.data;
                  console.log('lala',resolved);
                  $scope.$apply();
                });
                console.log('method pressed '+methodName+ ' class: '+className);
              };// END of instanceMethodFunctions
              $scope.instanceActionFunctions[className] = function(actionName) {
                if ( $scope.instanceResult[className] instanceof hyClass[className] ){
                  console.log('Already iniated');
                } else {
                  $scope.instanceResult[className] = new hyClass[className](baseUrl);
                }
                $scope.instanceResult[className]['@id'] = 'http://vps454845.ovh.net:8085/postal_addresses/1';
                var methodRes = $scope.instanceResult[className].action(actionName);
                methodRes.then(function(resolved){
                  $scope.responseCode = resolved.response[baseUrl].status;
                  console.log('$scope.responseCode',$scope.responseCode);
                  $scope.responseData = resolved.response[baseUrl].data;
                  console.log('lala',resolved);
                  // $scope.instanceResult[className] = resolved.parsedObjects[0];
                  $scope.$apply();
                  console.log('method pressed '+actionName+ ' class: '+className);
                },
                function(resolved){
                  $scope.responseCode = resolved.response[baseUrl].status;
                  $scope.responseData = resolved.response.data;
                  console.log('lala',resolved);
                  $scope.$apply();
                });
              };// END of instanceActionFunctions
            }
          });


          Object.keys(hyCollection).forEach(function(className,i){
            Object.keys(classLiterals[className]).forEach(function(server,o){
              if (server!=='common') {
                $scope.bindCollectionNames.push({'clName':className ,'server':server});
                var baseUrl = server;
                var temp = new hyCollection[className](baseUrl);
                $scope.bindCollectionActionNames[className] = Object.keys(temp.collectionLits.actions);
                $scope.bindCollectionMethodNames[className] = Object.keys(temp.collectionLits.operations);
                $scope.$apply();
              }
            });
          });

          console.log('hyBind',hyBind);
          Object.keys(hyBind).forEach(function(className,i){
            var tempCustom = new hyBind[className]();
            console.log('tempCustom',tempCustom);
            $scope.bindCustomActionNames[className] = Object.keys(tempCustom.possibleActions);
            // $scope.bindCollectionMethodNames[className] = Object.keys(temp.collectionLits.operations);
            $scope.$apply();
          });
        });//END of EACH Class
          // deferred.resolve(browseGraph);
      },
      function (error) {
          reject(error);
      });
      //Frame the graph response and load Text editor and Vizualazation
      //Load Editor Graph
      if (frameOn) {
        jsonld.frame(graphObj,hydraFrame, function(err, compacted){
            console.log('compInside',err);
            console.log('compInside',compacted);
            // var formated = jsonld.normalize(compacted);
            $scope.browseEditor.setValue(formatter(JSON.stringify(compacted)).formatJson);
            vizGraph(compacted, '#graph');
        });
      } else {
        $scope.browseEditor.setValue(formatter(JSON.stringify(graphObj)).formatJson);
        vizGraph(graphObj, '#graph');
      }
    }





//Browse API Graphs

    $scope.namedGraph = '';
    $scope.selectedClassArray = '';
    $scope.selectedClasses = {};
    $scope.apiDescriptions = [];
    $scope.serverVocs = [];
    $scope.browseQuery = {};
    $scope.graphDesc = {};

    // vizGraph('http://vps362714.ovh.net:8085/docs.jsonld', '#graph');

// Init ACE editor
    $scope.publishEditor;
    $scope.consumeEditor;
    $scope.browseEditor = ace.edit("browseEditor");
    // var formated = formatter(JSON.stringify(exampleGraph));
    $scope.browseEditor.$blockScrolling = Infinity;
    $scope.browseEditor.setTheme("ace/theme/xcode");
    $scope.browseEditor.session.setMode("ace/mode/json");
    // $scope.browseEditor.setValue(formated.formatJson);

    loadBrowserGui(exampleGraph,browseGraph);

    $scope.editorSearch;
    $scope.editorLine = 1;

    console.log(exampleGraph);
    // hydraParsedPromise2(JSON.stringify(exampleGraph),browseGraph,'')
    // .then(function(browseGraph){
    //     var literalArray = schydraAngular.getLiterals(browseGraph);
    //     [$scope.apiClassLiterals,$scope.apiCollectionLiterals] = literalArray;
    //     console.log('schydraLits',literalArray);
    //     // deferred.resolve(browseGraph);
    // },
    // function (error) {
    //     reject(error);
    // })
    var testGraph = '';

/*
  Download graph from Ace editor as JSON-LD
 */
    $scope.downloadGraph = function(editor) {
        var data = editor.getValue();
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        var blob = new Blob([data], {type: "octet/stream"}),
            url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'graph.jsonld';
        a.click();
        window.URL.revokeObjectURL(url);
    }
    /*
    Ace editor Goto selected line
     */
    $scope.gotoLine = function(editor,line) {
      editor.gotoLine(line, 0, true);
    }
    /*
      Search Ace Editor, not returning after reaching end
     */
    $scope.searchGraph = function(editor,str) {
        var startPoint = {
            'end': {
              'column': 1,
              'row': 1
            },
            'start': {
              'column': 1,
              'row':1
            }
          };

        var test = editor.find(str,{
            backwards: false,
            wrap: false,
            caseSensitive: false,
            wholeWord: false,
            regExp: false
            // start: startPoint
        });
        // var options = {
        //   'backwards': true
        // };
        // var options2 = {
        //   'backwards': false
        // };
        // console.log('test',test);
        // editor.findNext(options2);
        // editor.findPrevious(options);
    }


/*
  Load data on ACE editor for publish
 */
    $scope.loadpublishEditor = function(editor) {
    }

    $scope.loadpublishEditor = function() {
        $scope.publishEditor = ace.edit("publishEditor");
        $scope.publishEditor.$blockScrolling = Infinity;
        $scope.publishEditor.setTheme("ace/theme/xcode");
        $scope.publishEditor.session.setMode("ace/mode/json");
        $scope.publishEditor.setValue(formated.formatJson);
    }

    var rawJSON = {
        "@context" : {
          "expects" : {
            "@id" : "http://www.w3.org/ns/hydra/core#expects",
            "@type" : "@id"
          },
          "returns" : {
            "@id" : "http://www.w3.org/ns/hydra/core#returns",
            "@type" : "@id"
          },
          "method" : {
            "@id" : "http://www.w3.org/ns/hydra/core#method"
          },
          "title" : {
            "@id" : "http://www.w3.org/ns/hydra/core#title"
          },
          "label" : {
            "@id" : "http://www.w3.org/2000/01/rdf-schema#label"
          },
          "writable" : {
            "@id" : "http://www.w3.org/ns/hydra/core#writable",
            "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
          },
          "required" : {
            "@id" : "http://www.w3.org/ns/hydra/core#required",
            "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
          },
          "readable" : {
            "@id" : "http://www.w3.org/ns/hydra/core#readable",
            "@type" : "http://www.w3.org/2001/XMLSchema#boolean"
          },
          "property" : {
            "@id" : "http://www.w3.org/ns/hydra/core#property",
            "@type" : "@id"
          },
          "object": {
            "@id": "schema:object",
            "@type": "@id"
          },
          "result": {
            "@id": "schema:result",
            "@type": "@id"
          },
          "target": {
            "@id": "schema:target",
            "@type": "@id"
          },
          "query": {
            "@id": "schema:query"
          },
          "description" : {
            "@id" : "http://www.w3.org/ns/hydra/core#description"
          },
          "supportedProperty" : {
            "@id" : "http://www.w3.org/ns/hydra/core#supportedProperty",
            "@type" : "@id"
          },
          "supportedOperation" : {
            "@id" : "http://www.w3.org/ns/hydra/core#supportedOperation",
            "@type" : "@id"
          },
          "@vocab" : "http://schema.org/",
          "schema" : "http://schema.org/",
          "hydra" : "http://www.w3.org/ns/hydra/core#",
          "xmls" : "http://www.w3.org/2001/XMLSchema#",
          "owl" : "http://www.w3.org/2002/07/owl#",
          "rdf" : "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
          "xsd" : "http://www.w3.org/2001/XMLSchema#",
          "rdfs" : "http://www.w3.org/2000/01/rdf-schema#",
        },
        "offer": {
          "@type": "Offer",
          "priceCurrency": '',
          "price": '',
          "seller": '',
          "itemOffered": { "@type": "Flight" },
          "potentialAction": {
            "@type": "searchAction",
            "query": {"@type": "schema:Flight"},
            "object": "schema:Offer",
            "result": "schema:Offer",
            "target": "schema:Offer"
          }
        },
        "flight": {
          "@type": "Flight",
          "arrivalAirport": {"@type": "Airport"},
          "departureAirport": {"@type": "Airport"},
          "departureTime": {"@type": "xmls:dateTime"},
          "arrivalTime": {"@type": "xmls:dateTime"},
          "provider": ""
        },
        "postalAdress": {
          "@type": "PostalAddress",
          "addressCountry": '',
          "addressLocality": ''
        },
        "airport": {
          "@type": "Airport",
          "iataCode": '',
          "address": { "@type": "PostalAddress" },
          "geo": {"@type": "GeoCoordinates"},
          "potentialAction": {
            "@type": "searchAction",
            "object": "schema:Airport",
            "result": "schema:Airport",
            "target": "schema:Airport",
            "query": {"@type": "schema:PostalAddress"}
          }
        },
        "poi": {
          "@type": "TouristAttraction",
          "name": "",
          "description": "",
          "url": "",
          "hasMap": "",
          "geo": {"@type": "GeoCoordinates"},
          "image": "",
          "potentialAction": {
            "@type": "searchAction",
            "object": "schema:TouristAttraction",
            "result": "schema:TouristAttraction",
            "target": "schema:TouristAttraction",
            "query": {"@type": "schema:GeoCoordinates"}
          }
        },
        "geoloc": {
          "@type": "GeoCoordinates",
          "latitude": "",
          "longitude": ""
        },
        "hotels": {
          "@type": "Hotel",
          "name": "",
          "description": "",
          "address": {"@type":"schema:PostalAddress"},
          "faxNumber": "",
          "telephone": "",
          "geo": {"@type": "schema:GeoCoordinates"},
          "potentialAction": {
            "@type": "searchAction",
            "object": "schema:Hotel",
            "result": "schema:Hotel",
            "target": "schema:Hotel",
            "query": {"@type": "schema:GeoCoordinates"}
          }
        }
    };
/*
Load data on ACE editor for consume formatter(JSON.stringify(rawJSON)).formatJson;
 */
    $scope.loadConsumeEditor = function() {
        $scope.consumeEditor = ace.edit("consumeEditor");
        $scope.consumeEditor.$blockScrolling = Infinity;
        $scope.consumeEditor.setTheme("ace/theme/xcode");
        $scope.consumeEditor.session.setMode("ace/mode/json");
        $scope.consumeEditor.setValue(formatter(JSON.stringify(rawJSON)).formatJson);
    }



    /*
    Load selected graph for display
     */



    $scope.fetchGraph = function (apiId) {
        var id = apiId ? apiId : 1;
        var link = apiResolver + '/software_applications/'+id ;
        if (isNaN(apiId)) {
          link = apiId;
        }
        console.log('apiId',apiId);
        var req = {
            method: 'GET',
            url: link,
            headers: {
                'Content-Type': 'application/ld+json'
            }
        };
        $http(req)
        .then(function (response) {
          console.log('response',response);
            var graphy = response.data.text ? JSON.parse(response.data.text) : response.data ;
            console.log('graphy',graphy);
            var browseGraph = $rdf.graph();
            loadBrowserGui(graphy,browseGraph,true);
            // var obj = graphy;
            // console.log('frame',hydraFrame);
            // // vizGraph(obj, '#graph');
            // // vizGraph(null, '#graph');
            // console.log('obj',obj);
            // var formated = formatter(graphy);
            // jsonld.frame = function(input, hydraFrame, options, callback)
            // loadVis(obj,'#graph');
            // var ctx = obj['@context'];
            // console.log('ctx',ctx);
            // var options = {
            //     'algorithm': 'URDNA2015',
            // 'skipExpansion' : true};

            // jsonld.frame(obj,hydraFrame, function(err, compacted){
            //     console.log('compInside',err);
            //     console.log('compInside',compacted);
            //     $scope.browseEditor.setValue(formatter(JSON.stringify(compacted)).formatJson);
            //     vizGraph(compacted, '#graph');
            // });
            // jsonld.normalize(obj, function(err, compacted){
            //     console.log('compInside',compacted);
            //     // var formated = jsonld.normalize(compacted);
            //     $scope.browseEditor.setValue(formatter(JSON.stringify(compacted)).formatJson);
            // });
            // $scope.browseEditor.setValue(formated.formatJson);
            console.log(response);
        },
        function (error) {
            reject(error);
        });
    }
/*
    Load API-Resolver vocabularies
 */
    $scope.fetchVocs = function () {
        var req = {
            method: 'GET',
            url: apiResolver + '/stats_vocs',
            headers: { 'Content-Type': 'application/ld+json' }
        };
        $http(req)
                .then(function (response) {
                    var vocs = response.data['hydra:member'];
                    console.log(response.data['hydra:member']);
                    vocs.forEach(function (voc, i) {
                        $scope.vocabArray[i] = {};
                        $scope.vocabArray[i] = vocs[i];
                        $scope.vocabArray[i]['selected'] = false;
                        $scope.browseQuery[$scope.vocabArray[i]['@id']] = {};
                    });
                    console.log($scope.serverVocs);
                    // response.data
                },
                function (error) {
                    reject(error);
                });
    }

    $scope.fetchVocs();
/*
    Load autocomplete classes from selected vocabularies
 */
    $scope.serverClassSearch = function (str) {
        var matches = [];
        var results = $q.defer();
        var selectedVocs = [];
        var searchString = '/stats_classes?url=' + str;
        if (!(selectedVocs.length === 0)) {
            $scope.vocabArray.forEach(function (obj, i) {
                if (obj['selected']) {
                    selectedVocs.push(obj['id']);
                }
            });
            searchString += '&vocabulary[]=' + selectedVocs
        }
        var req = {
            method: 'GET',
            url: apiResolver + searchString,
            headers: {'Content-Type': 'application/ld+json'}
        };
        $http(req)
            .then(function (response) {
                var possibleClasses = response.data['hydra:member'];
                results.resolve(possibleClasses);
                console.log(response);
            },
            function (error) {
                reject(error);
            });
        return results.promise;
    };
/*
    Load selected class in the list of possible ones.
 */
    $scope.loadClass = function (data) {
        if (data.originalObject) {
            var obj = data.originalObject;
            $scope.browseQuery[obj['vocabulary'][0]][obj.url] = true;
            console.log(data);
        }
    }
/*
    Based on the selected Vocs and Classes, search API-Resolver
    for descriptions
 */
    $scope.fetchAPIDescription = function(query) {
        console.log('query',query);
        // console.log('$scope.browseQuery',$scope.browseQuery);
        var searchString = '';
        var classes = null;
        if (query) {
            searchString = '?';
            Object.keys(query).forEach(function(voc,i){
                console.log('voc',voc);
                // if ( typeof query[i] !== 'undefined'){
                    Object.keys(query[voc]).forEach(function(cl,k){
                        console.log('cl',cl);
                        console.log('cl',query[voc][cl]);
                        if (query[voc][cl]) {
                            if (classes===null) {
                                classes = 'applicationSubCategory='+cl;
                            } else {
                                classes += ',' + cl;
                            }
                            console.log(classes);
                        }
                    });
                    if (classes!==null) {
                        classes = classes.slice(0, -1);
                    }
                // }
            });
        }
        console.log(classes);
        var req = {
            method: 'GET',
            url: apiResolver + '/software_applications'+searchString + classes,
            headers: {
                'Content-Type': 'application/ld+json'
            }
        };
        $http(req)
        .then(function (response) {
            $scope.graphDesc = response.data['hydra:member'];
            console.log(response);
        },
        function (error) {
            reject(error);
        });
    }


//https://raw.githubusercontent.com/schemaorg/schemaorg/sdo-gozer/data/schema.rdfa

// Consume API

/*
  Code snippets
 */
    $scope.testJson = {
         "flight": {
          "@type": "Flight",
          "arrivalAirport": {"@type": "Airport"},
          "departureAirport": {"@type": "Airport"},
          "departureTime": {"@type": "xmls:dateTime"},
          "arrivalTime": {"@type": "xmls:dateTime"},
          "provider": ""
        }
    }
    $scope.testCode = "\
   var destinationGeo = new hydraClass['GeoCoordinates']();\n\
    destinationGeo.label = 'Destination Location';\n\
    destinationGeo.latitudeLabel('Latitude');\n\
    destinationGeo.longitudeLabel('Longitude');\n\
    destinationGeo.latitude = '';\n\
    destinationGeo.longitude = '';\n\n\
    var pois = new hydraClass['TouristAttraction']();\n\
    pois.label = 'Points of Interest';\n\
    pois.geo = destinationGeo;\n\n\
    fetchPois = function() {\n\
      pois.actions.schemasearch().then(function(response){\n\
          retrievedPois = response;\n\
        });\n\
    };";

    $scope.testCodeInit =
    "var schydra = schydraAngular.init(rawJSON,resolverServer,graph);"+
    "\n schydra.then(function(hydraClass){ }";
    $scope.consumeEx1 = JSON.stringify($scope.testCode, undefined, 4);


// Call our search function and fetch results
    $scope.discover = function(editor) {
      var jsonDescription =  JSON.parse(editor.getValue());
      console.log('jsonDescription',jsonDescription);
      console.log('rawJSON',rawJSON);
      var consumeGraph = $rdf.graph();
      var req = {
        method: 'PUT',
        url: apiResolver+'/api_ref/special',
        headers: {
        'Content-Type': 'application/ld+json'
        },
        data: jsonDescription
      };
        $http(req)
        .then(function (response) {
          console.log('hydra response',response);
          loadBrowserGui(response.data,consumeGraph,false,false,jsonDescription);
        },
        function (error) {
            reject(error);
        });
      // var schydra = schydraAngular.test(jsonDescription,(apiResolver+'/api_ref/special'),consumeGraph);
      //     schydra.then(function(hydraClass){
      //       loadBrowserGui(graphObj,graphRDF,frameOn);
      //       var literalArray = schydraAngular.getLiterals(consumeGraph);
      //       var hydraObj = schydraAngular.getJsonld();
      //       jsonld.frame(hydraObj,hydraFrame, function(err, compacted){
      //           console.log('compInside',err);
      //           console.log('compInside',compacted);
      //           // var formated = jsonld.normalize(compacted);
      //           $scope.browseEditor.setValue(formatter(JSON.stringify(compacted)).formatJson);
      //           vizGraph(compacted, '#graph');
      //       });
      //       [$scope.apiClassLiterals,$scope.apiCollectionLiterals] = [{},{}];
      //       [$scope.apiClassLiterals,$scope.apiCollectionLiterals] = literalArray;
      //       console.log('discoveredClasses',literalArray);
      //       $scope.log = schydraAngular.log;
      //       $scope.error = schydraAngular.error;
      //     });
      return true;
    }

// PUBLISH Graph

    $scope.classAddName = '';
    $scope.baseURL = 'http://temp.graph.net';
    var schemaURL = 'https://schema.org/version/3.2/schema.jsonld';
    $scope.loadedVocs = {};
    $scope.loadedClasses = {};
    $scope.publishedClasses = [];

    $scope.publishVocabs = {
        "https://schema.org": {
            "selected": false,
            "description": "schema vocabulary",
            // "url": 'http://0.0.0.0:9000/scripts/schema.jsonld'
            "url": 'http://vps362714.ovh.net/schydra/scripts/schema.jsonld'
        },
        "hydra": {
            "selected": false,
            "description": "hydra vocabulary",
            "url": "http://0.0.0.0:9000/scripts/schema.jsonld"
        }
    };

/*
    Load selected vocabularies as graphs
 */
    $scope.loadVocs = function (selVoc,key) {
        console.log('selected',selVoc);
        console.log('ket',key);
        var vocsPromises = [];
        Object.keys(selVoc).forEach(function(voc,i){
            var obj = selVoc[voc];
            console.log('voc.selected',obj.selected);
            if (obj.selected) {
                console.log('Loaded',obj.selected);
                var deferred = $q.defer();
                vocsPromises.push(deferred.promise);
                var mimeType = obj.url.split('.').pop();
                var contentType = '';
                var req = {
                    method: 'GET',
                    url: obj.url,
                    // headers: {
                    //     'Content-Type': contentType,
                    //     'Accept': contentType
                    // }
                };//text/plain; charset=utf-8
                switch(mimeType) {
                    case 'jsonld':
                        contentType = 'application/ld+json';
                        break;
                    case 'rdfa':
                        contentType = 'text/plain';//'text/html'
                        break;
                    case 'ttl':
                        contentType = 'text/turtle';
                        break;
                    case 'xml':
                        contentType = 'application/xml';
                        break;
                };
                $http(req)
                .then(function (response) {
                    hydraParsedPromise2(JSON.stringify(response.data),vocGraph,i,contentType)
                        .then(function(){
                          console.log('vocGraph',vocGraph);
                            deferred.resolve(vocGraph);
                        },
                        function (error) {
                            reject(error);
                        })
                    // console.log(response);
                },
                function(update) {
                    deferred.resolve(null);
                    console.log('Got notification: ' ,update);
                });
            }
        });
        $q.all(vocsPromises)
        .then(function(resolved) {
            // console.log('promised vocs',resolved);
            if (resolved.length>0) {
                    resolved.forEach(function(voc,k){
                      if (voc) {
                          var classNodes = resolved[k].each(null,RDF('type'),RDFS('Class'));
                          classNodes.forEach(function(node,i){
                              $scope.loadedClasses[(classNodes[i].value)] = {};
                              $scope.loadedClasses[(classNodes[i].value)]['iri'] = (classNodes[i].value);
                              $scope.loadedClasses[(classNodes[i].value)]['comment'] = resolved[k].the(classNodes[i],RDFS('comment'),null).value;
                              $scope.loadedClasses[(classNodes[i].value)]['label'] = resolved[k].the(classNodes[i],RDFS('label'),null).value;
                          });
                      }
                });
            } else {
                console.log('resolved.length',resolved.length);
            }
        },
        function(reason) {
            console.log('Failed: ' + reason);
        },
        function(update) {
            console.log('Got notification: ' + update);
        });

        // .catch(function(resolved){
        //     // console.log(resolved);
        //     console.log('Loading one or more vocabularies failed');
        // });
    }

/*
    Autocomplete search loaded classes
 */
    $scope.publishClassSearch = function(str, cl) {
        var matches = [];
        Object.keys(cl).forEach(function(clName) {
          var label = cl[clName].label;
          var comment = cl[clName].comment;
          if ((label.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)||
              (comment.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) {
            matches.push(cl[clName]);
          }
        });
        return matches;
    };

    function retrievePropertyMeta() {
    }

    $scope.toClassNameSimple = function(str) {
        if (str) {
            str = str.split('/').pop();
            return str;
        }
        return null;
    }
/*
    Retrieve Class properties from the graph describing the vocabulary
 */
    function classProps(property,level,iri,arr,schema) {
        var properties = {};
        arr[level] = {};
        var propNodes = schema.each(null,SCHEMA('domainIncludes'),property);
        propNodes.forEach(function(node,i){
            var comment = schema.the(propNodes[i],RDFS('comment'),null);
            var label = schema.the(propNodes[i],RDFS('label'),null);
            var range = schema.the(propNodes[i],SCHEMA('rangeIncludes'),null);
            properties[propNodes[i].value] = {};
            // properties[propNodes[i].value]['@id'] = 'vocab:#'+label;
            properties[propNodes[i].value]['selected'] = false;
            properties[propNodes[i].value]['test'] = "false";
            properties[propNodes[i].value]['@type'] = HYDRA('SupportedProperty').value;
            properties[propNodes[i].value][HYDRA('property').value] = {};
            properties[propNodes[i].value][HYDRA('property').value]['@id'] = 'vocab:#'+label;
            properties[propNodes[i].value][HYDRA('property').value]['@type'] = ['rdf:Property',propNodes[i].value];
            properties[propNodes[i].value][HYDRA('property').value][RDFS('label').value] = label.value;
            properties[propNodes[i].value][HYDRA('property').value]['domain'] = iri;
            properties[propNodes[i].value][HYDRA('property').value]['range'] = range.value;
            properties[propNodes[i].value][HYDRA('title').value] = label.value;
            properties[propNodes[i].value][HYDRA('required').value] = false;
            properties[propNodes[i].value][HYDRA('readable').value] = true;
            properties[propNodes[i].value][HYDRA('writable').value] = true;
            properties[propNodes[i].value][HYDRA('description').value] = comment.value;
        });

        arr[level][iri] = properties;//schema.each(null,SCHEMA('domainIncludes'),property);
        var superClass = schema.each(property,RDFS('subClassOf'),null)
        if (superClass.length>0) {
            level++;
            superClass.forEach(function(value,i) {
                classProps(superClass[i],level,superClass[i].value,arr,schema);
            });
        } else {
            // properties['domainIncludes'] = (schema.each(null,SCHEMA('domainIncludes'),property))
        }
        return arr;
    }

var createGraph = function(classArray) {
  // $scope.publishedClasses
  if(classArray.legnth<0){
    return null;
  }
  var doc = {};
  doc['@context'] = {
        "vocab": "http://localhost/docs.jsonld",
        "@base": "http://localhost",
        "hydra": "http://www.w3.org/ns/hydra/core#",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "xmls": "http://www.w3.org/2001/XMLSchema#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "schema": "http://schema.org/",
        "domain": {
            "@id": "rdfs:domain",
            "@type": "@id"
        },
        "range": {
            "@id": "rdfs:range",
            "@type": "@id"
        },
        "subClassOf": {
            "@id": "rdfs:subClassOf",
            "@type": "@id"
        },
        "expects": {
            "@id": "hydra:expects",
            "@type": "@id"
        },
        "returns": {
            "@id": "hydra:returns",
            "@type": "@id"
        },
        "object": {
            "@id": "schema:object",
            "@type": "@id"
        },
        "result": {
            "@id": "schema:result",
            "@type": "@id"
        },
        "target": {
            "@id": "schema:target",
            "@type": "@id"
        },
        "query": {
            "@id": "schema:query"
        },
        "property": {
            "@id": "hydra:property"
        },
        "variable": {
            "@id": "hydra:variable"
        },
        "required": {
            "@id": "hydra:required"
        }
    };
  doc['@id'] = '/hydra.jsonld';
  doc['hydra:description'] = 'This is a test graph created using the API Resolver playground';
  doc['hydra:entrypoint'] = '/';
  doc['hydra:title'] = 'API Resolver Test graph';
  doc['hydra:supportedClass'] = [];
  doc['hydra:supportedClass'][0] = {
    '@id': 'vocab#Entrypoint',
    '@type': 'hydra:Class',
    'hydra:title': 'The API entrypoint',
    'hydra:supportedProperty': [],
    'hydra:supportedOperation': {
      '@type': 'hydra:Operation',
      'hydra:method': 'GET',
      'rdfs:label': 'The API entrypoint',
      'returns': '#Entrypoint'
    }
  };
  classArray.forEach(function(selectedClass,i){
    var localClass = {};
    localClass['@id'] = 'vocab:#' + selectedClass.iri.split('/').pop();
    localClass['@type'] = [];
    localClass['@type'][0] = 'hydra:Class';
    localClass['@type'][1] = selectedClass.iri;
    localClass['hydra:title'] = selectedClass.label
    localClass['rdfs:label'] = selectedClass.label;
    localClass['hydra:supportedProperty'] = [];
    localClass['hydra:supportedOperation'] = [];
    selectedClass.properties.forEach(function(parentClass,k){
      console.log('doc2',parentClass);
      // if (Object.keys(parentClass))
      Object.keys(parentClass).forEach(function(className,l){
        console.log('doc3',parentClass[className]);
        if ((typeof parentClass[className] === 'object')) {
          console.log('doc5',typeof parentClass[className]);//&&(Object.keys(parentClass[className])>0)
          Object.keys(parentClass[className]).forEach(function(propertyName,o){
            console.log('doc4',propertyName);
            console.log('doc1',parentClass[className][propertyName]);
            if (parentClass[className][propertyName].selected) {

              localClass['hydra:supportedProperty'].push(parentClass[className][propertyName]);
            }
          })
        } else {
          return;
        }
      })
    });//END of selectedClass.properties
    // selectedClass.operations.forEach(function(operation,k){
      var collections = {};
      collections['@type'] = [];
      collections['@type'][0] = 'hydra:SupportedProperty';
      // collections['@type'][1] = 'schema:AddAction';
      collections['hydra:title'] = 'Collection of  '+selectedClass.label+ ' resources.';
      collections['hydra:writable'] = false;
      collections['hydra:readable'] = true;
      collections['hydra:property'] = {
        '@id': '/'+selectedClass.label+'Collection',
        '@type': 'hydra:Link',
        'domain': 'vocab:#Entrypoint',
        'rdfs:label': 'Collection of '+selectedClass.label+ ' resources.',
        'range': {
          '@type': 'hydra:PartialCollectionView',
          'hydra:member': {
            '@type': 'vocab:#'+selectedClass.label,
            'hydra:search': null
          }
        },
        "hydra:supportedOperation": [
          {
              "@type": [
                  "hydra:operation",
                  "schema:FindAction"
              ],
              "hydra:method": "GET",
              "hydra:title": "Retrieves the collection of "+selectedClass.label+" resources.",
              "rdfs:label": "Retrieves the collection of "+selectedClass.label+" resources.",
              "returns": "hydra:PartialCollectionView"
          }]
        };
    if (selectedClass.operations.get.selected) {
      var op = {};
      op['@type'] = [];
      op['@type'][0] = 'hydra:operation';
      op['@type'][1] = 'schema:FindAction';
      op['returns'] = selectedClass.iri;
      op['rdfs:label'] = 'Retrieves a '+selectedClass.label+ ' instance.';
      op['hydra:title'] = 'Retrieves a '+selectedClass.label+ ' instance.';
      op['hydra:method'] = 'GET';
      localClass['hydra:supportedOperation'].push(op);
    }
    if (selectedClass.operations.post.selected) {
      var op =
        {
            "@type": [
                "hydra:operation",
                "schema:AddAction"
            ],
            "expects": selectedClass.iri,
            "hydra:method": "POST",
            "hydra:title": "Adds a "+selectedClass.label+" resource.",
            "rdfs:label": "Adds a "+selectedClass.label+" resource.",
            "returns": selectedClass.iri
        };
      collections['hydra:property']["hydra:supportedOperation"].push(op);
    }
    if (selectedClass.operations.put.selected) {
      var op = {};
      op['@type'] = [];
      op['@type'][0] = 'hydra:operation';
      op['@type'][1] = 'schema:UpdateAction';
      op['returns'] = selectedClass.iri;
      op['expects'] = selectedClass.iri;
      op['rdfs:label'] = 'Updates a '+selectedClass.label+ ' instance.';
      op['hydra:title'] = 'Updates a '+selectedClass.label+ ' instance.';
      op['hydra:method'] = 'PUT';
      localClass['hydra:supportedOperation'].push(op);
    }
    if (selectedClass.operations.delete.selected) {
      var op = {};
      op['@type'] = [];
      op['@type'][0] = 'hydra:operation';
      op['@type'][1] = 'schema:DeleteAction';
      op['returns'] = 'owl:Nothing';
      op['rdfs:label'] = 'Retrieves a '+selectedClass.label+ ' instance.';
      op['hydra:title'] = 'Retrieves a '+selectedClass.label+ ' instance.';
      op['hydra:method'] = 'DELETE';
      localClass['hydra:supportedOperation'].push(op);
    }
    doc['hydra:supportedClass'].push(localClass);
    doc['hydra:supportedClass'][0]['hydra:supportedProperty'].push(collections);
  });
  console.log('docObj',doc);
  var tempGraph = $rdf.graph();
  loadBrowserGui(doc,tempGraph,false,false,[]);
}

$scope.publishGraph = function(graphArray) {
  createGraph(graphArray);

}

$scope.publishedMethods = {};
/*
    Fetch selected Class metadata, properties etc
 */
    $scope.publishLoadClass = function (data) {
        var testArray = [];
        var loadedClass = data['originalObject'];
        loadedClass['properties'] = classProps($rdf.sym(data['originalObject']['iri']),0,data['originalObject']['iri'],testArray,vocGraph);
        loadedClass['operations'] = {};
        loadedClass['operations']['put'] = {};
        loadedClass['operations']['put']['selected'] = false;
        loadedClass['operations']['get'] = {};
        loadedClass['operations']['get']['selected'] = true;
        loadedClass['operations']['delete'] = {};
        loadedClass['operations']['delete']['selected'] = false;
        loadedClass['operations']['post'] = {};
        loadedClass['operations']['post']['selected'] = false;

        $scope.publishedClasses.push(loadedClass);
        console.log('Parsed Class Array',testArray);
        console.log('Selected Class to publish',data);
    }
    console.log('$scope.publishedClasses',$scope.publishedClasses);
    console.log('$scope.publishedMethods',$scope.publishedMethods);



    $scope.mySplit = function(string) {
        var array = string.split(',');
        return array;
    }

    }]);

blogApp.directive('hyApiDesc', function() {
    return {
        scope: true,
        template:
            '<div class="col-sm-6 col-md-4 col-lg-3 " >'+
                '<div class="card" style="width: 20rem;">'+
                    '<img class="card-img-top" src={{desc.image}} alt="server image placeholder">'+
                        '<div class="card-block" >'+
                            '<h4 class="card-title"> {{desc.name}}</h4>'+
                            '<p class="card-text">{{desc.description}}</p>'+
                            '<strong> Vocabularies in use:</strong>'+
                            '<div ng-repeat="voc in mySplit(desc.applicationCategory)">'+
                                '<a target="_blank" href="{{voc}}">{{voc}}</a> '+
                            '</div>'+
                            '<strong> Classes in use:</strong>'+
                            '<div ng-repeat="cl in mySplit(desc.applicationSubCategory)">'+
                                '<a target="_blank" href="{{cl}}">{{cl}} </a> '+
                            '</div>'+
                            '<br><a class="btn btn-primary type="button" btn-lg disabled" data-toggle="button" role="button" aria-disabled="true" href=#BrowseGraph ng-click="fetchGraph(desc.id)" class="text-primary"><i class="fa fa-map" aria-hidden="true"></i> Load Graph</a>'+
                            '<br><i class="fa fa-link" aria-hidden="true"></i><a target="_blank" href={{desc.provider}} class="text-primary"> Server URL</a>'+
                            '<br><strong>Release Notes:</strong>'+
                            '<p>{{desc.releaseNotes}}</p>'+
                            '<br><small>Updated at: {{desc.dateModified}}</small>'+
                            '<br><small>Version: {{desc.softwareVersion}}</small>'+
                        '</div>'+
                    '</div>'+
                '</div>'
  };
});

blogApp.directive('hyGraphProperty', function() {
   return {
        scope: {
            property: '='
        },
        template:''
    }
});

blogApp.directive('hyOperation', function() {
   return {
        scope: {
            property: '='
        },
        template:''
    }
});

blogApp.directive('hyClass', function() {

   return {
        scope: true,
        template:
            '<div ng-if="true">'+
    '<div ng-if="responseCode === 200">'+
        '<div class="card-block card-success border-0">'+
        '<h6>Response Field</h6>'+
        'Properties'+
        '<div class="card-footer">'+
            '<div ng-repeat="(propertyName, propertyObj) in instanceMethodResult[value].classLits.properties track by $index">'+
                '<div class="input-group">'+
                    '<span class="input-group-addon" id="{{propertyName}}-addon3">{{propertyName}}</span>'+
                    '<input type="text" id="basic-url" aria-describedby="{{propertyName}}-addon3" name="instanceMethod" placeholder="{{instanceMethodResult[value][propertyName]}}" class="form-control" ng-model=instanceMethodResult[value][propertyName]>'+
                '</div>'+
            '</div>'+
            '</div>'+
        '</div>'+
    '</div>'+
    '<div ng-if="responseCode === 0">'+
        '<div class="card-footer card-info border-0">'+
            '<h6>Response Field</h6>'+
        '</div>'+
    '</div>'+
    '<div ng-if="(responseCode !== 200)&&(responseCode !== 0)">'+
        '<div class="card-footer card-warning border-0">'+
        '<h6>Response Filed</h6>'+
        '<p>Code: {{responseCode}}</p>'+
        'Data'+
        '<ul class="">'+
            '<div ng-repeat="(propertyName, propertyObj) in responseData track by $index">'+
                '<div ng-if="propertyName !== '/trace/'">'+
                   '<li> {{propertyName}} : {{propertyObj}}</li>'+
                '</div>'+
            '</div>'+
        '</ul>'+
        '</div>'+
    '</div>'+
'</div>'
    };
});

blogApp.directive('hyCollection', function() {

   return {
        scope: {
            collection: '='
        },
        template:''
    }
});

                        // <div class="input-group">
                        //     <span class="input-group-addon" id="{{propertyName}}-addon3">{{propertyName}}</span>
                        //     <input type="text" id="basic-url" aria-describedby="{{propertyName}}-addon3" name="instanceMethod"  class="form-control" ng-model=instanceResult[value][propertyName]>
                        // </div>

blogApp.directive('hyProperty', function() {

    return {
        scope: {
            property: '='
        },
        template:
            '<form>'+
                '<div class="input-group">'+
                    '<span class="input-group-addon" id="{{propertyName}}-addon3"><strong>@type</strong></span>'+
                    '<input type="text" id="disabledTextInput" disabled class="form-control" placeholder={{property["@type"]}}>'+
                '</div>'+
                '<br>'+
                '<strong>HYDRA properties </strong>'+
                '<div class="container" >'+
                    '<div class="input-group">'+
                        '<span class="input-group-addon" id="{{propertyName}}-addon3"><strong>@id</strong></span>'+
                        '<input type="text" id="basic-url" aria-describedby="{{propertyName}}-addon3" name="instanceMethod" disabled  class="form-control" ng-model="property[\'http://www.w3.org/ns/hydra/core#property\'][\'@id\']">'+
                    '</div>'+
                    '<div class="input-group">'+
                        '<span class="input-group-addon" id="{{propertyName}}-addon3"><strong>@type</strong></span>'+
                        '<input type="text" id="basic-url" aria-describedby="{{propertyName}}-addon3" name="instanceMethod" disabled  class="form-control" ng-model="property[\'http://www.w3.org/ns/hydra/core#property\'][\'@type\']">'+
                        '</div>'+
                    '<div class="input-group">'+
                        '<span class="input-group-addon" id="{{propertyName}}-addon3"><strong>domain</strong></span>'+
                        '<input type="text" id="basic-url" aria-describedby="{{propertyName}}-addon3" name="instanceMethod" disabled  class="form-control" ng-model="property[\'http://www.w3.org/ns/hydra/core#property\'][\'domain\']">'+
                    '</div>'+
                    '<div class="input-group">'+
                        '<span class="input-group-addon" id="{{propertyName}}-addon3"><strong>range</strong></span>'+
                        '<input type="text" id="basic-url" aria-describedby="{{propertyName}}-addon3" name="instanceMethod" disabled  class="form-control" ng-model="property[\'http://www.w3.org/ns/hydra/core#property\'][\'range\']">'+
                    '</div>'+
                    '<div class="input-group">'+
                        '<span class="input-group-addon" id="{{propertyName}}-addon3"><strong>label</strong></span>'+
                        '<input type="text" id="basic-url" aria-describedby="{{propertyName}}-addon3" name="instanceMethod"  class="form-control" ng-model="property[\'http://www.w3.org/ns/hydra/core#property\'][\'http://www.w3.org/2000/01/rdf-schema#label\']">'+
                    '</div>'+
                    '<div class="input-group">'+
                        '<span class="input-group-addon" id="{{propertyName}}-addon3"><strong>title</strong></span>'+
                        '<input type="text" id="basic-url" aria-describedby="{{propertyName}}-addon3" name="instanceMethod"  class="form-control" ng-model="property[\'http://www.w3.org/ns/hydra/core#title\']">'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<span class="input-group-addon" id="{{propertyName}}-addon3"><strong>description</strong></span>'+
                        ' <textarea class="form-control form-rounded" rows="3" ng-model="property[\'http://www.w3.org/ns/hydra/core#description\']"></textarea>'+
                        // '<input type="text" id="basic-url" aria-describedby="{{propertyName}}-addon3" name="instanceMethod"  class="form-control" ng-model="property[\'http://www.w3.org/ns/hydra/core#title\']">'+
                    '</div>'+
                '<div class="form-check">'+
                    '<label class="form-check-label">'+
                        '<input type="checkbox" class="form-check-input" ng-model="property[\'http://www.w3.org/ns/hydra/core#readable\']">'+
                        '<span class="text-danger">hydra:readable </span>'+
                    '</label>'+
                '</div>'+
                '<div class="form-check">'+
                    '<label class="form-check-label">'+
                        '<input type="checkbox" class="form-check-input" ng-model="property[\'http://www.w3.org/ns/hydra/core#writable\']">'+
                        '<span class="text-danger">hydra:writable </span>'+
                    '</label>'+
                '</div>'+
                '<div class="form-check">'+
                    '<label class="form-check-label">'+
                        '<input type="checkbox" class="form-check-input" ng-model="property[\'http://www.w3.org/ns/hydra/core#required\']">'+
                        '<span class="text-danger">hydra:required </span>'+
                    '</label>'+
                '</div>'+
            '</form>'
  };
});

blogApp.filter('prettyJson', [function () {
    return function (obj, pretty) {
        if (!obj) {
            return '';
        }
        var json = angular.toJson(obj, pretty);
        if (!pretty) {
            return json;
        }
        return json
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                var cls;
                if (/^"/.test(match)) {
                    cls = /:$/.test(match) ? 'key' : 'string';
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                } else {
                    cls = 'number';
                }
                return '<span class="json-' + cls + '">' + match + '</span>';
            });
    };
}]);