// function expandClass(classNode,graph,level,properties) {
//     level = level ? level : 0;
//     properties = properties ? properties : new Array();
//     // var props = graph.each(null,SCHEMA('domainIncludes'),classNode);
//     var temp = {};
//     properties[level] = [];
//     temp[classNode.value] = graph.each(null,SCHEMA('domainIncludes'),classNode);
//     properties[level].push(temp);
//     var superClass = graph.each(classNode,RDFS('subClassOf'),null);
//     if (superClass.length>0) {
//         level++;
//         properties[level] = [];
//         superClass.forEach(function(node,i){
//             expandClass(superClass[i],graph,level,properties);
//         });
//     } else {
//         return properties
//     }
// }

// function expandClass(property,schema) {
//     var properties = {}
//     var superClass = schema.any(property,RDFS('subClassOf'),null)
//     if ((superClass)) {
//         properties['domainIncludes'] = (schema.each(null,SCHEMA('domainIncludes'),property))
//         var temp = expandClass(superClass,schema)
//         properties[superClass.value] = temp;
//     } else {
//         properties['domainIncludes'] = (schema.each(null,SCHEMA('domainIncludes'),property))
//     }
//     return properties
// }

// function flattenClass(obj,level,iri,arr) {
//     var props = [];
//     arr[level] = {};
//     if (obj.hasOwnProperty('domainIncludes')) {
//         arr[level][iri] = obj['domainIncludes'];
//     } else {
//         return arr;
//     }
//     level++;
//     Object.keys(obj).forEach(function(value,key){
//         if ((typeof obj[value] === 'object')&&(value !== 'domainIncludes')){
//             console.log('obj',obj);
//             console.log('level',level);
//             console.log('value',value);
//             flattenClass(obj[value],level,value,arr);
//         }
//     });
//     return arr;
// }
// 
// 
// 
    var destinationGeo = new hydraClass['GeoCoordinates']();
    destinationGeo.label = 'Destination Location';
    destinationGeo.set('latitude','Latitude','label');
    destinationGeo.set('longitude','Longitude','label');
    destinationGeo.set('latitude','');
    destinationGeo.set('longitude','');

    var pois = new hydraClass['TouristAttraction']();
    pois.label = 'Points of Interest';
    pois.set('geo',destinationGeo);

    fetchPois = function() {
      pois.actions.searchAction(graph,'').then(function(response){
          retrievedPois = response;
        });
    };

    $scope.syntaxHighlight = function(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}


var syntaxHighlight = function(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

blogApp.filter('prettify', function () {
    function syntaxHighlight(json) {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }
    return syntaxHighlight;
});